import time
from NRPy_functions import base_gf_name_and_type,superfast_uniq,nrpy_ccode_parser
from sympy import zeros, factorial,Rational,sympify,ccode, srepr
from parameters_compiletime import params_varname, params_value, FDUPWINDINPLACE
from NRPy_functions import get_parameter_value
from NRPy_SIMD import expr_convert_to_SIMD_funcs

FDSTENCILSIZE = int(get_parameter_value("FDCENTERDERIVS_FDORDER", params_varname, params_value)) + 1
PRECISION = get_parameter_value("PRECISION", params_varname, params_value)

#  Define the to-be-inverted matrix, A.
#  We define A row-by-row, according to the prescription
#  derived in notes/notes.pdf, via the following pattern
#  that applies for arbitrary order.
#
#  As an example, consider a 5-point finite difference
#  stencil (4th-order accurate), where we wish to compute
#  some derivative at the center point.
#
#  Then A is given by:
#
#  -2^0  -1^0  1  1^0   2^0
#  -2^1  -1^1  0  1^1   2^1
#  -2^2  -1^2  0  1^2   2^2
#  -2^3  -1^3  0  1^3   2^3
#  -2^4  -1^4  0  1^4   2^4
#
#  Then multiplying A^{-1}
#  by (1 0 0 0 0)^T will yield 0th deriv. stencil
#  by (0 1 0 0 0)^T will yield 1st deriv. stencil
#  by (0 0 1 0 0)^T will yield 2nd deriv. stencil
#  etc.
#
#  Next suppose we want an upwinded, 4th-order accurate
#  stencil. For this case, A is given by:
#
#  -1^0  1  1^0   2^0   3^0
#  -1^1  0  1^1   2^1   3^1
#  -1^2  0  1^2   2^2   3^2
#  -1^3  0  1^3   2^3   3^3
#  -1^4  0  1^4   2^4   3^4
#
#  ... and similarly for the downwinded derivative.
#
#  Finally, let's consider a 3rd-order accurate
#  stencil. This would correspond to an in-place
#  upwind stencil with stencil radius of 2 gridpoints,
#  where other, centered derivatives are 4th-order
#  accurate. For this case, A is given by:
#
#  -1^0  1  1^0   2^0
#  -1^1  0  1^1   2^1
#  -1^2  0  1^2   2^2
#  -1^3  0  1^3   2^3
#  -1^4  0  1^4   2^4
#
#  ... and similarly for the downwinded derivative.
#
#  The general pattern is as follows:
#
#  1) The top row is all 1's,
#  2) If the second row has N elements (N must be odd),
#  .... then the radius of the stencil is rs = (N-1)/2
#  .... and the j'th row e_j = j-rs-1. For example,
#  .... for 4th order, we have rs = 2
#  .... j  | element
#  .... 1  | -2
#  .... 2  | -1
#  .... 3  |  0
#  .... 4  |  1
#  .... 5  |  2
#  3) The L'th row, L>2 will be the same as the second
#  .... row, but with each element e_j -> e_j^(L-1)
#  A1 is used later to validate the inverted
#  matrix.

def fdstencil_coeffs(STENCILSIZE,derivstr,updownwindstr,upwindinplacestr,coeffs,stencl):
    if updownwindstr == "NoUpwind":
        UPDOWNWIND = 0
    elif updownwindstr == "Upwind":
        UPDOWNWIND  = 1
    elif updownwindstr == "Downwind":
        UPDOWNWIND  =-1
    elif upwindinplacestr == "UpwindInPlace" and updownwindstr == "Upwind":
        UPDOWNWIND -= 1
    else:
        print("upwindinplacestr == "+updownwindstr+" not recognized!")
        exit()

    M = zeros(STENCILSIZE,STENCILSIZE)
    for i in range(STENCILSIZE):
        for j in range(STENCILSIZE):
            if i == 0:
                M[(i,j)] = 1 # Setting n^0 = 1 for all n, including n=0, because this matches the pattern
            else:
                dist_from_xeq0_col = j - Rational((STENCILSIZE - 1),2) + UPDOWNWIND
                if dist_from_xeq0_col==0:
                    M[(i,j)] = 0
                else:
                    M[(i, j)] = dist_from_xeq0_col**(i)
    Minv = zeros(STENCILSIZE,STENCILSIZE)
    Minv = M**(-1)

    if   derivstr == "FirstDeriv":
        DERIV=1
    elif derivstr == "SecondDeriv":
        DERIV=2
    elif derivstr == "KreissOligerDeriv":
        DERIV=STENCILSIZE-1
    else:
        print("Error: Didn't recognize derivative string")
    for i in range(STENCILSIZE):
        coeffs[i] = factorial(DERIV)*Minv[(i,DERIV)]
        if derivstr == "KreissOligerDeriv":
            coeffs[i] *= (-1)**(Rational((STENCILSIZE+1),2))/2**DERIV
        stencl[i] = i - int((STENCILSIZE-1)/2) + UPDOWNWIND
    return

def unique_idx(ijk,gfidx):
    #  ASSUMES 3D INDEXING IN C CODE IS FORTRAN-LIKE: return i + Npts[0] * (j + Npts[1] * k);
    offset = 50
    return str(int(ijk[0])+offset + 500*( (int(ijk[1])+offset) + 500*( (int(ijk[2])+offset) + 500*( gfidx+offset ) ) ))

def invdx_string(type,derivdirn1,derivdirn2,SIMD="None"):
    returnstring = "inv"
    if SIMD == "256bit" or SIMD == "512bit":
        returnstring = "MulSIMD(inv"
    # In which direction is derivative taken?
    if type == "Upwind" or type == "First":
        return returnstring+"dx"+str(derivdirn1)
    elif type == "Second":
        return returnstring+"dx"+str(derivdirn1)+"invdx"+str(derivdirn2)
    elif type == "KreissOliger":
        return returnstring+"dy"+str(derivdirn1)
    else:
        print("Error: Unknown derivative type:",type)
        exit(1)

def readfrommemprefix(inputstring):
    if inputstring[:3] == "AUX":  # If the gridfunction name starts with AUX, then it's an auxilliary gridfunct.
        return "gfs_aux[IDX4("+inputstring+","
    return "in_gfs[IDX4("+inputstring+","

def process_unmixed_fd_derivative(list_of_gfs,gfindex, derivdirn, stencl,coeffs,
                                  list_of_fdidxs_pulled_from_memory,fd_derivexpr_rhs_string):
    fd_derivexpr_rhs_string.extend([""])
    for i in range(len(stencl)):
        # FD coeffs for mixed derivs are zero if i offset is 0
        if sympify(coeffs[i]) != 0:
            coeffstring = " +" + str(sympify(coeffs[i]))
            coeffstring = coeffstring.replace('+-', '-').replace('+','+ ').replace('-','- ')
            #fd_derivexpr_rhs_string.extend([coeffstring])
            if derivdirn == 0:
                ijk = [stencl[i], 0, 0]
                idxstring = readfrommemprefix(list_of_gfs[gfindex]) + "ii+" + str(stencl[i]) + ",jj,kk)]"
            if derivdirn == 1:
                ijk = [0, stencl[i], 0]
                idxstring = readfrommemprefix(list_of_gfs[gfindex]) + "ii,jj+" + str(stencl[i]) + ",kk)]"
            if derivdirn == 2:
                ijk = [0, 0, stencl[i]]
                idxstring = readfrommemprefix(list_of_gfs[gfindex]) + "ii,jj,kk+" + str(stencl[i]) + ")]"
            # Replace, e.g., j+-1->j-1, j+0->j, j-0->j
            idxstring = idxstring.replace('+-', "-").replace('+0', '').replace('-0','') + ".START"+unique_idx(ijk,gfindex)+"END"
            list_of_fdidxs_pulled_from_memory.extend([idxstring])
            fd_derivexpr_rhs_string[len(fd_derivexpr_rhs_string) - 1] += coeffstring + " * " + idxstring

def process_mixed_second_fd_derivative(list_of_gfs,gfindex,derivdirn1,derivdirn2,stencl_1stderiv,coeffs_1stderiv,
                                       list_of_fdidxs_pulled_from_memory,fd_derivexpr_rhs_string):
    fd_derivexpr_rhs_string.extend([""])
    for i in range(len(stencl_1stderiv)):
        for j in range(len(stencl_1stderiv)):
            # FD coeffs for mixed derivs are zero if i or j offset is 0
            if sympify(coeffs_1stderiv[i]) != 0 and sympify(coeffs_1stderiv[j]) != 0:
                coeffstring = " +" + str(sympify(coeffs_1stderiv[i]) * sympify(coeffs_1stderiv[j]))
                coeffstring = coeffstring.replace('+-', '-').replace('+', '+ ').replace('-', '- ')
                #fd_derivexpr_rhs_string.extend([coeffstring])
                if derivdirn1 == 0 and derivdirn2 == 1:
                    ijk = [stencl_1stderiv[i], stencl_1stderiv[j], 0]
                    idxstring = readfrommemprefix(list_of_gfs[gfindex])+"ii+"+str(stencl_1stderiv[i])+",jj+"+str(stencl_1stderiv[j])+",kk)]"
                if derivdirn1 == 0 and derivdirn2 == 2:
                    ijk = [stencl_1stderiv[i], 0, stencl_1stderiv[j]]
                    idxstring = readfrommemprefix(list_of_gfs[gfindex])+"ii+"+str(stencl_1stderiv[i])+",jj,kk+"+str(stencl_1stderiv[j])+")]"
                if derivdirn1 == 1 and derivdirn2 == 2:
                    ijk = [0, stencl_1stderiv[i], stencl_1stderiv[j]]
                    idxstring = readfrommemprefix(list_of_gfs[gfindex])+"ii,jj+"+str(stencl_1stderiv[i])+",kk+"+str(stencl_1stderiv[j])+")]"
                # Replace, e.g., j+-1->j-1, j+0->j, j-0->j :
                idxstring = idxstring.replace('+-', "-").replace('+0', '').replace('-0','') + ".START"+unique_idx(ijk,gfindex)+"END"
                list_of_fdidxs_pulled_from_memory.extend([idxstring])
                fd_derivexpr_rhs_string[len(fd_derivexpr_rhs_string)-1] += coeffstring + "*" + idxstring

# This function processes a list of variables called "list_of_gridfunction_derivs", including gridfunctions and their
#  derivatives. Processing is performed in two steps, described below in Step 1 and Step 2 comment blocks.
def fd_codegen__process_list_of_gridfunction_derivs(filename,list_of_gridfunction_derivs,
                                                    func_gf_define_alias,list_of_gridfunctions,
                                                    stencl_1stderiv  , coeffs_1stderiv,
                                                    stencl_1stderivup, coeffs_1stderivup,
                                                    stencl_1stderivdn, coeffs_1stderivdn,
                                                    stencl_2ndderiv , coeffs_2ndderiv, SIMD="None", # SIMD is an optional input parameter.
                                                    SIMD_const_varnme_array = [],SIMD_const_values_array = []):
    DATATYPE = PRECISION
    if SIMD == "256bit":
        DATATYPE = "__m256d"
    elif SIMD == "512bit":
        DATATYPE = "__m512d"

    #########
    # Step 1
    # When we read variables from main memory, we generate code like
    #  dummygf = in_gfs[DUMMYGF][index];
    #  We passed in a function called func_gf_define_alias that will read in dummygf and output DUMMYGF, or
    #   whatever other alias you'd like to specify, generally through a simple string search/replace.
    list_of_gf_defs = []
    for j in range(len(list_of_gridfunctions)):
        list_of_gf_defs.extend([func_gf_define_alias(list_of_gridfunctions[j])])

    #########
    # Step 2
    # This is where the magic happens.
    #  First read in the base gridfunction name (basegfname) and type using pattern described in Step 1.
    #  Then generate the appropriate finite difference line of code based on the type of derivative.
    #  Each line of code has a left-hand-side (lhs) and a right-hand-side (rhs):
    #  lhs = the variable name (stored in raw_varname). E.g., hDDdD110
    #  rhs = the finite difference expression. E.g., something like "(hDD11ip1jk - hDD11im1jk)/(2 dx);"
    #  For each term in the rhs needed from memory, we store the precise index in list_of_gfidxs_pulled_from_memory[]
    #  There is logic to ensure that all temporary variables are human-readable: hDD11ip1jk = hDD11[IDX(i+1,j,k)]
    #  To maximize efficiency, we sort list_of_gfidxs_pulled_from_memory[]
    list_of_gfidxs_pulled_from_memory = []
    fd_derivexpr_lhs_string = []
    fd_derivexpr_rhs_string = []
    fd_derivexpr_rhs_divide_by_dxtothen_string = []
    derivdirn2 = "Null"
    for i in range(len(list_of_gridfunction_derivs)):
        raw_varname =  str(list_of_gridfunction_derivs[i])
        basegfname_list = [""]
        type = base_gf_name_and_type(raw_varname, basegfname_list)
        basegfname = basegfname_list[0]
        gfindex = list_of_gridfunctions.index(basegfname)

        fd_derivexpr_lhs_string.extend([raw_varname])
        if type == "Second":
            derivdirn1 = int(raw_varname[:-1][-1:])  # The second-to-last character contains derivdirn1. E.g., 1 in hDDdDD0012
            derivdirn2 = int(raw_varname[-1:])  # The last character contains derivdirn2. E.g., 2 in hDDdDD0012
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([ invdx_string("Second", derivdirn1, derivdirn2,SIMD) ])

            # First, process mixed 2nd-order derivatives
            if derivdirn1 != derivdirn2:
                process_mixed_second_fd_derivative(list_of_gf_defs,gfindex,derivdirn1,derivdirn2,
                                                   stencl_1stderiv,coeffs_1stderiv,
                                                   list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)
            # Next, process unidirectional 2nd-order derivatives
            if derivdirn1 == derivdirn2:
                process_unmixed_fd_derivative(
                    list_of_gf_defs,gfindex,derivdirn1, stencl_2ndderiv, coeffs_2ndderiv,
                    list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)

        elif type == "First":
            derivdirn1 = int(raw_varname[-1:])  # The last character contains derivdirn1. E.g., 2 in vetUdD02
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([invdx_string("First", derivdirn1, derivdirn2,SIMD)])
            process_unmixed_fd_derivative(list_of_gf_defs,gfindex,derivdirn1, stencl_1stderiv, coeffs_1stderiv,
                                          list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)

        elif type == "KreissOliger":
            derivdirn1 = int(raw_varname[-1:])  # The last character contains derivdirn1. E.g., 2 in vetUdD02
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([invdx_string("KreissOliger", derivdirn1, derivdirn2,SIMD)])
            process_unmixed_fd_derivative(list_of_gf_defs,gfindex,derivdirn1, stenclKOderiv, coeffsKOderiv,
                                          list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)

        elif type == "Upwind":
            derivdirn1 = int(raw_varname[-1:])  # The last character contains derivdirn1. E.g., 2 in vetUdD02
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([invdx_string("Upwind", derivdirn1, derivdirn2,SIMD)])
            process_unmixed_fd_derivative(list_of_gf_defs,gfindex,derivdirn1, stencl_1stderivup, coeffs_1stderivup,
                                          list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)
            # Downwinded derivative: In the code, we declare, e.g.,
            # [upwinded derivative] = upwind[0] * NAMEdupD[0][0]  + dnwind[0] * NAMEddnD[0][0]
            # I.e., upwinded derivatives are defined as a sum of up and downwinded, with upwind[] and dnwind[]
            #  set to 1 or 0 depending on the sign of vetU in a given direction.
            fd_derivexpr_lhs_string.extend([raw_varname.replace("dupD", "ddnD")])
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([invdx_string("Upwind", derivdirn1, derivdirn2,SIMD)])
            process_unmixed_fd_derivative(list_of_gf_defs,gfindex,derivdirn1, stencl_1stderivdn, coeffs_1stderivdn,
                                          list_of_gfidxs_pulled_from_memory,fd_derivexpr_rhs_string)

        elif type == "Zeroth":
            fd_derivexpr_rhs_divide_by_dxtothen_string.extend([""])
            idxstring = readfrommemprefix(list_of_gf_defs[gfindex]) + "ii,jj,kk)]"
            # Replace, e.g., j+-1->j-1, j+0->j, j-0->j :
            idxstring = idxstring.replace('+-',"-").replace('+0','').replace('-0','')+".START"+unique_idx([0,0,0],gfindex)+"END"
            list_of_gfidxs_pulled_from_memory.extend([idxstring])
            fd_derivexpr_rhs_string.extend([idxstring])

    list_of_gfidxs_pulled_from_memory_uniq = superfast_uniq(list_of_gfidxs_pulled_from_memory)
    keys = ["" for i in range(len(list_of_gfidxs_pulled_from_memory_uniq))]
    for i in range(len(keys)):
        keys[i] = list_of_gfidxs_pulled_from_memory_uniq[i].replace('END','').replace('.START','\n').splitlines()[1]
    keys.sort()
    final_list_of_gfidxs = []
    for i in range(len(keys)):
        for j in range(len(list_of_gfidxs_pulled_from_memory_uniq)):
            if list_of_gfidxs_pulled_from_memory_uniq[j].replace('.START','\n.START').splitlines()[1] == ".START"+keys[i]+"END":
                rhs = list_of_gfidxs_pulled_from_memory_uniq[j].replace('.START','\n').splitlines()[0]
                tmpvarname = rhs.replace("in_gfs[IDX4(","").replace("gfs_aux[IDX4(","").replace(")]","").replace(",","")
                tmpvarname = tmpvarname.replace("ii","_i").replace("jj","_j").replace("kk","_k")
                tmpvarname = tmpvarname.replace("-","m").replace("+","p")

                if SIMD == "256bit":
                    final_list_of_gfidxs.extend([ "const __m256d " + tmpvarname + " = _mm256_loadu_pd(&" + rhs + ");" ])
                elif SIMD == "512bit":
                    final_list_of_gfidxs.extend([ "const __m512d " + tmpvarname + " = _mm512_loadu_pd(&" + rhs + ");" ])
                else: # if SIMD == "None"; only SIMD == "256bit", "512bit", and "None" are currently supported.
                    final_list_of_gfidxs.extend(["const " + DATATYPE + " " + tmpvarname + " = " + rhs + ";"])
                for k in range(len(fd_derivexpr_rhs_string)):
                    fd_derivexpr_rhs_string[k] = fd_derivexpr_rhs_string[k].replace(list_of_gfidxs_pulled_from_memory_uniq[j],tmpvarname)
                j = len(list_of_gfidxs_pulled_from_memory_uniq)+1
    with open(filename, "a") as output:
        for i in range(len(final_list_of_gfidxs)):
            output.write(final_list_of_gfidxs[i] + "\n")
        for i in range(len(fd_derivexpr_lhs_string)):
            dummy = [""]
            vartype = "const "+DATATYPE+" "
            if base_gf_name_and_type(str(fd_derivexpr_lhs_string[i]),dummy) == "Upwind":
                vartype = DATATYPE+" "
            fullexp = vartype
            if SIMD == "None":
                fullexp += nrpy_ccode_parser(ccode(sympify(fd_derivexpr_rhs_string[i]),
                                                   sympify(fd_derivexpr_lhs_string[i])))
                if fd_derivexpr_rhs_divide_by_dxtothen_string[i] != "":
                    fullexp = fullexp.replace(" = ",
                                              " = " + fd_derivexpr_rhs_divide_by_dxtothen_string[i] + " * ( ").replace(
                        ";", " );")
            elif SIMD == "256bit" or SIMD == "512bit":
                fullexp += fd_derivexpr_lhs_string[i]+" = "
                fullexp += str(expr_convert_to_SIMD_funcs(sympify(fd_derivexpr_rhs_string[i]),SIMD_const_varnme_array,SIMD_const_values_array, debug=True))+";"
                if fd_derivexpr_rhs_divide_by_dxtothen_string[i] != "":
                    fullexp = fullexp.replace(" = ",
                                              " = " + fd_derivexpr_rhs_divide_by_dxtothen_string[i] + " , ( ").replace(
                        ";", " ) );")

            output.write(fullexp + "\n")
    list_of_gfidxs_pulled_from_memory.sort()

# DRIVER FUNCTION:
starttimer_FDgen = time.time()
print("Generating finite difference stencils...")

# Finite Difference stencil / coefficient generation
# First derivative, with finite-difference stencil size of FDSTENCILSIZE and highest-order accuracy
coeffs_1stderiv = [0 for i in range(FDSTENCILSIZE)]
stencl_1stderiv = [0 for i in range(FDSTENCILSIZE)]
fdstencil_coeffs(FDSTENCILSIZE,"FirstDeriv","NoUpwind","NoUpwindInPlace",coeffs_1stderiv,stencl_1stderiv)

if FDUPWINDINPLACE == "True":
    # First derivative upwinded, with finite-difference stencil size of FDSTENCILSIZE-1 and highest-order accuracy
    coeffs_1stderivup = [0 for i in range(FDSTENCILSIZE-1)]
    stencl_1stderivup = [0 for i in range(FDSTENCILSIZE-1)]
    fdstencil_coeffs(FDSTENCILSIZE-1,"FirstDeriv","Upwind","UpwindInPlace",coeffs_1stderivup,stencl_1stderivup)
    # First derivative downwinded, with finite-difference stencil size of FDSTENCILSIZE-1 and highest-order accuracy
    coeffs_1stderivdn = [0 for i in range(FDSTENCILSIZE-1)]
    stencl_1stderivdn = [0 for i in range(FDSTENCILSIZE-1)]
    fdstencil_coeffs(FDSTENCILSIZE-1,"FirstDeriv","Downwind","UpwindInPlace",coeffs_1stderivdn,stencl_1stderivdn)
else:
    # First derivative upwinded, with finite-difference stencil size of FDSTENCILSIZE and highest-order accuracy
    coeffs_1stderivup = [0 for i in range(FDSTENCILSIZE)]
    stencl_1stderivup = [0 for i in range(FDSTENCILSIZE)]
    fdstencil_coeffs(FDSTENCILSIZE,"FirstDeriv","Upwind","NoUpwindInPlace",coeffs_1stderivup,stencl_1stderivup)
    # First derivative downwinded, with finite-difference stencil size of FDSTENCILSIZE and highest-order accuracy
    coeffs_1stderivdn = [0 for i in range(FDSTENCILSIZE)]
    stencl_1stderivdn = [0 for i in range(FDSTENCILSIZE)]
    fdstencil_coeffs(FDSTENCILSIZE,"FirstDeriv","Downwind","NoUpwindInPlace",coeffs_1stderivdn,stencl_1stderivdn)
# Second derivative, with finite-difference stencil size of FDSTENCILSIZE, with highest-order accuracy
coeffs_2ndderiv = [0 for i in range(FDSTENCILSIZE)]
stencl_2ndderiv = [0 for i in range(FDSTENCILSIZE)]
fdstencil_coeffs(FDSTENCILSIZE,"SecondDeriv","NoUpwind","NoUpwindInPlace",coeffs_2ndderiv,stencl_2ndderiv)
if FDUPWINDINPLACE == "False":
    # Highest-order derivative possible with stencil size of FDSTENCILSIZE+2,
    #    multiplied by (-1)**(Rational((STENCILSIZE+1),2))/2**(STENCILCIZE-1)
    coeffsKOderiv = [0 for i in range(FDSTENCILSIZE+2)]
    stenclKOderiv = [0 for i in range(FDSTENCILSIZE+2)]
    fdstencil_coeffs(FDSTENCILSIZE+2,"KreissOligerDeriv","NoUpwind","NoUpwindInPlace",coeffsKOderiv,stenclKOderiv)
else:
    # Highest-order derivative possible with stencil size of FDSTENCILSIZE,
    #    multiplied by (-1)**(Rational((STENCILSIZE+1),2))/2**(STENCILCIZE-1)
    coeffsKOderiv = [0 for i in range(FDSTENCILSIZE)]
    stenclKOderiv = [0 for i in range(FDSTENCILSIZE)]
    fdstencil_coeffs(FDSTENCILSIZE,"KreissOligerDeriv","NoUpwind","NoUpwindInPlace",coeffsKOderiv,stenclKOderiv)
stoptimer_FDgen = time.time()
print("Finished finite difference stencil generation in \t" + str(round(stoptimer_FDgen-starttimer_FDgen,2)) + " seconds")
