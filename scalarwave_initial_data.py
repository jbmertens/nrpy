import time
from sympy import sqrt, Matrix, sympify, symbols, sin, cos, Rational, diff, log
from parameters_compiletime import params_type, params_varname, params_value
from NRPy_functions import get_parameter_value, set_parameter
from reference_metric import ReU, ReDD, ghatDD, xx, r as r_refmetric, th as th_refmetric, ph as ph_refmetric, protected_varnames
from NRPy_functions import declarerank2, print_diff
from NRPy_file_output import NRPy_file_output

def sympify_integers__replace_rthph(obj,r,th,ph,r_refmetric,th_refmetric,ph_refmetric):
    if isinstance(obj, int):
        return sympify(obj)
    else:
        # Do not enable precomputation of transcendental functions in initial data routine. Unlikely to positively impact performance.
        return obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric)
        # However, precomputation can very well be useful
        # return clean_transcendental(obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric),
        #                             list_of_precomp_names, counter)

OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

##########################
# INITIAL DATA PARAMETERS
##########################

# Initial data parameters: choose between PlaneWave
# Validated against Mathematica: (none)
ID_scheme = get_parameter_value("ID_scheme", params_varname, params_value)

DebugIDagainstMathematica = "False"

#************************
#************************
# --={ INITIAL DATA }=--
# --={  TRANSLATION }=--
#************************
#************************
# Apply appropriate coordinate transformations to convert
# initial data in a given coord system to our grid

# TIP: If your solution blows up early, you may need to be
#      a bit more careful about coordinate singularities in
#      the Jacobian!

print("Generating C code for scalarwave-based -={ "+ID_scheme+" }=- initial data. This may take a few minutes...")
starttimer_scalarwaveID = time.time()

# Variables needed for initial data given in spherical basis
r       = symbols('r', positive=True)
th, ph = symbols('th ph', real=True)

ID_protected_variables = protected_varnames

# Quantities that must be set by initial data routines below:
# Physical (as opposed to conformal or reference) metric:
IDuu = symbols('IDuu', real=True)
IDvv = symbols('IDvv', real=True)

# Represents a plane wave propagating in the z-direction
if ID_scheme == "PlaneWave":
    # Input parameters:
    # Auxiliary variables:
    zz = symbols('zz', real=True) # z-coordinate
    zz = r*cos(th)

    IDuu = cos(zz) + 2
    IDvv = sin(zz)
else:
    print("ERROR: You chose an unsupported ID_scheme. Check for spelling issues or special characters and try again.")
    exit(1)

# Now that all derivatives of ghat and gbar have been computed,
# we may now substitute the definitions r = r_refmetric, th=th_refmetric,...
# WARNING: Substitution only works when the variable is not an integer. Hence the if not isinstance(...,...) stuff.
# If the variable isn't an integer, we revert transcendental functions inside to normal variables. E.g., sin(x2) -> sinx2
#  Reverting to normal variables in this way makes expressions simpler in NRPy, and enables transcendental functions
#  to be pre-computed in SENR.
IDuu = sympify_integers__replace_rthph(IDuu,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
IDvv = sympify_integers__replace_rthph(IDvv,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

r  = r_refmetric
th = th_refmetric
ph = ph_refmetric

# -={ C code generation: output to codegen_output/NRPy_Initial_Data_*.h }=-
NRPy_file_output(OUTDIR+"/initial_data/NRPy_codegen/Initial_Data.h", [],[],[], ID_protected_variables,
                 [],[IDuu,"in_gfs[IDX4pt(UU,idx)]",IDvv,"in_gfs[IDX4pt(VV,idx)]"])
# For validation against Mathematica-based code:
if DebugIDagainstMathematica == "True":
    print("Print[\"ReDDs:\"]")
    for i in range(3):
        for j in range(i, 3):
            print_diff(ReDD[i][j], "ReDD[" + str(i) + "][" + str(j) + "]")

    print("Print[\"uu,vv:\"]")
    print_diff(IDuu, "IDuu")
    print_diff(IDvv, "IDvv")


stoptimer_scalarwaveID = time.time()
print("Completed scalarwave initial data code generation in \t"+str(round(stoptimer_scalarwaveID-starttimer_scalarwaveID,2))+" seconds")
