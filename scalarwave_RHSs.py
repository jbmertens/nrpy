import time
from sympy import sympify, symbols
# Generate finite difference stencils
#from finite_difference_stencil_generator import *
# Define the reference metric ghatDD & rescaling quantities ReU and ReDD. xyzCart,dxdr,drdx used for ADM integrands.
from reference_metric import protected_varnames
# Define reference metric derived ("hatted") quantities:
from ref_metric__hatted_quantities import ghatUU,GammahatUDD
from NRPy_file_output import NRPy_file_output
from NRPy_functions import declarerank1, declarerank2, get_parameter_value
from parameters_compiletime import params_varname, params_value

def func_scalarwave_gf_define_alias(gfname):
    # FIXME: compatibility mode:
    return gfname.replace("D", "").replace("U", "").replace("2", "3").replace("1", "2").replace("0", "1").upper()

# SCALAR WAVE

# Set precision to, e.g., double or long double
PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

#################################
#   SCALARWAVE RHS PARAMETERS   #
#################################

scalarwave_evolved_variable_list = [ "uu", "vv"]
scalarwave_evolved_variable_list.sort()

scalarwave_aux_gridfunctions = []  # Put other gridfunctions here

# Write the file gridfunction_defines.h, which stores #define aliases to gridfunctions.
#  This makes NRPy-generated C codes easier to read and understand.
with open(OUTDIR+"/gridfunction_defines-NRPyGEN.h", "w") as output:
    output.write("#define NUM_EVOL_GFS "+str(len(scalarwave_evolved_variable_list))+"\n")
    for i in range(len(scalarwave_evolved_variable_list)):
        output.write("#define "+func_scalarwave_gf_define_alias(scalarwave_evolved_variable_list[i])+" "+str(i)+"\n")
    output.write("// AUXILIARY GRIDFUNCTIONS: (These could in principle re-use space stored for other gridfunction RHSs, though they do not.)\n")
    for i in range(len(scalarwave_aux_gridfunctions)):
        output.write("#define "+func_scalarwave_gf_define_alias(scalarwave_aux_gridfunctions[i])+" "+str(i)+"\n")

scalarwave_runtime_parameters = ["wavespeed"]  # Wave speed
                                               # We set these as runtime parameters so they are not considered
                                               #   a gridfunction or evolved variable.

scalarwave_protected_variables = protected_varnames + scalarwave_runtime_parameters

##############################
#   DEFINE SCALARWAVE RHSs   #
##############################
# Start timer, for benchmarking
starttimer_scalarwaveEqs = time.time()
print("Generating C code for scalarwave equations...")

'''
SEE tutorial/scalarwave for a full description of the scalar wave equation, NRPy+, and how this code works
All equation references below are used in the tutorial
'''

# First compute the contracted Christoffel symbol (Eq. 1.8 in scalar wave tutorial)
GammahatU = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            GammahatU[i] += ghatUU[j][k]*GammahatUDD[i][j][k]

# Set up 2nd order derivatives for uu gridfunction \partial_i \partial_j u.
# Declaring it in this way (with declarerank2) ensures that NRPy_file_output.py
#    knows to define the expressions in the C code as finite differences:
uudDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(uudDD,"uudDD","sym12","DeclTrue")
# Do the same for \partial_i u:
uudD = [ sympify(0) for i in range(3)]; declarerank1(uudD,"uudD")

# Enable Kreiss Oliger Dissipation? Let's not get into this, unnecessary complication.
# if EnableKreissOligerDissipation == "True":
#     uudDKO = [sympify(0) for i in range(3)]; declarerank1(uudDKO, "uudDKO")
#     vvdDKO = [sympify(0) for i in range(3)]; declarerank1(vvdDKO, "vvdDKO")
#     diss_ko_strength = symbols('diss_ko_strength', positive=True)
#
#     for dirn in range(3):
#         if EnableKO_direction[dirn] == "True":
#             uudDKO += diss_ko_strength*uudDKO[dirn]
#             vvdDKO += diss_ko_strength*vvdDKO[dirn]

# Declare vv as a proper SymPy symbol, so that expressions containing it are properly processed by SymPy
vv = symbols('vv', real=True)

# Eq. 2.2 in tutorial.
# We define the right-hand-side of a time evolution equation \partial_t [something] as [something]_rhs:
uu_rhs = vv

# Eq. 2.3 in tutorial.
vv_rhs = sympify(0) # sympify(expression) converts the expression into an expression that SymPy recognizes. In this case, the number zero.
for i in range(3):
    vv_rhs += - GammahatU[i] * uudD[i]
    for j in range(3):
        vv_rhs += ghatUU[i][j]*uudDD[i][j]

# Next we specify the RHS variables NRPy+ needs to output
rhs_vars = [uu_rhs,"gfs_rhs[IDX4pt(UU,idx)]",vv_rhs,"gfs_rhs[IDX4pt(VV,idx)]"]

# Next generate the C code for declaring and allocating memory for gridfunctions
#   Gridfunctions come in two classes:
#   (1) evolved quantities, and (2) not evolved quantities. The latter are defined
#   inside scalarwave_aux_gridfunctions[] and the former can be teased out of the
#   expressions that have been generated, using get_full_list_evol_gfs()

# Append auxiliary (not-evolved) gridfunctions to end of list of scalarwave_gfs. Currently scalarwave_aux_gridfunctions is empty.
list_of_scalarwave_gfs = scalarwave_evolved_variable_list + scalarwave_aux_gridfunctions

# Enable upwinding? Let's not get into this, unnecessary complication.
# scalarwave_upwind_vector = [sympify(0) for i in range(3)]
# for i in range(3):
#     scalarwave_upwind_vector[i] = vetU[i]*ReU[i]
SIMD = get_parameter_value("SIMD",params_varname,params_value) # Output 256 or 512bit-SIMD version if desired.
NRPy_file_output(OUTDIR+"/evolution_equations/NRPy_codegen/NRPy_scalarwave_RHS.h",func_scalarwave_gf_define_alias,list_of_scalarwave_gfs,
                 [], scalarwave_protected_variables, [], rhs_vars, SIMD)

stoptimer_scalarwaveEqs = time.time()
print("Completed scalar wave code generation in \t\t"+str(round(stoptimer_scalarwaveEqs-starttimer_scalarwaveEqs,2))+" seconds")
