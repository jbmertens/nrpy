import time
from sympy import sqrt, Matrix, sympify, symbols, sin, cos, Rational, diff, log, exp, simplify, pi
from parameters_compiletime import params_type, params_varname, params_value
from NRPy_functions import get_parameter_value, set_parameter
from reference_metric import ReU, ReDD, ghatDD, xx, r as r_refmetric, th as th_refmetric, ph as ph_refmetric, protected_varnames
from NRPy_functions import declarerank2, symm_matrix_inverter3x3, print_diff
from NRPy_file_output import NRPy_file_output

def sympify_integers__replace_rthph(obj,r,th,ph,r_refmetric,th_refmetric,ph_refmetric):
    if isinstance(obj, int):
        return sympify(obj)
    else:
        # Do not enable precomputation of transcendental functions in initial data routine. Unlikely to positively impact performance.
        return obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric)
        # However, precomputation can very well be useful
        # return clean_transcendental(obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric),
        #                             list_of_precomp_names, counter)

OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

##########################
# INITIAL DATA PARAMETERS
##########################

Coordinate_system = get_parameter_value("CoordSystem", params_varname, params_value)
if Coordinate_system != "Cartesian":
    print("\033[91m\033[1m Error! Evol_scheme = IllinoisGRMHD_ID_ETK_thorns requires Cartesian coordinates.\033[0m")
    exit(1)


# Initial data parameters: choose between BoostedSchwarzshild, BrillLindquist, Minkowski, StaticTrumpet, UIUC
# Validated against Mathematica: StaticTrumpet (with stationary gauge), UIUC (with stationary & PunctureR2/ShiftZero gauge), BoostedSchwarzschild (with PunctureR2 &StationaryBoostedSchwarzschild gauge), BrillLindquist & Minkowski (PunctureR2+Zero gauge)
ID_scheme = get_parameter_value("ID_scheme", params_varname, params_value)

# Initial lapse: choose between Unit, Trumpet, IsoSchw, PunctureR2, StationaryStaticTrumpet, StationaryUIUC
InitialLapseType = "PunctureR2"
set_parameter("char", "InitialLapseType", "PunctureR2", params_type, params_varname, params_value)

# Initial shift: choose between Zero, StationaryBoostedSchwarzschild, StationaryStaticTrumpet, and StationaryUIUC
InitialShiftType = "Zero"
set_parameter("char", "InitialShiftType", "Zero", params_type, params_varname, params_value)

DebugIDagainstMathematica = "False"

#************************
#************************
# --={ INITIAL DATA }=--
# --={  TRANSLATION }=--
#************************
#************************
# Apply appropriate coordinate transformations to convert
# initial data in a given coord system to our grid

# TIP: If your solution blows up early, you may need to be
#      a bit more careful about coordinate singularities in
#      the Jacobian!

print("Generating C code for IllinoisGRMHD-based -={ "+ID_scheme+" }=- initial data. This may take a few minutes...")
starttimer_IllinoisGRMHDID = time.time()

# Variables needed for initial data given in spherical basis
r       = symbols('r', positive=True)
th, ph = symbols('th ph', real=True)

ID_protected_variables = protected_varnames

# gPhys4UU = inverse physical (as opposed to conformal or reference) metric:
gPhys4UU = [[sympify(0) for i in range(4)] for j in range(4)]
KDD = [[sympify(0) for i in range(3)] for j in range(3)]

# The BoostedSchwarzschild represent a Schwarzschild black hole with mass M,
# Lorentz boosted in the z - direction. SEE https://arxiv.org/abs/1410.8607
if ID_scheme == "FishboneMoncriefID":
    # Input parameters:
    r_in,r_at_max_density,a,M,l = symbols('r_in r_at_max_density a M l', real=True)
    ID_protected_variables += ["r_in","r_at_max_density","a","M","l"]

    kappa,gamma = symbols('kappa gamma', real=True)
    ID_protected_variables += ["kappa","gamma"]

    # Auxiliary variables:
    zOffset, xB, yB, zB, LorentzFactor, rB, psiB = symbols('zOffset xB yB zB LorentzFactor rB psiB', real=True)

    # Fishbone-Moncrief paper
    # http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1976ApJ...207..962F&amp;data_type=PDF_HIGH&amp;whole_paper=YES&amp;type=PRINTER&amp;filetype=.pdf
    # First compute angular momentum at r_at_max_density, TAKING POSITIVE ROOT. This way disk is co-rotating with black hole
    # Eq 3.8:
    l_r  = sqrt(M/r_at_max_density**3) * (r_at_max_density**4 + r_at_max_density**2*a**2 - 2*M*r_at_max_density*a**2 - a*sqrt(M*r_at_max_density)*(r_at_max_density**2-a**2))
    l_r /= r_at_max_density**2 - 3*M*r_at_max_density + 2*a*sqrt(M*r_at_max_density)

    l = l_r

    # Eq 3.6:
    # First compute the radially-independent part of the log of the enthalpy, ln_h_const
    Delta = r**2 - 2*M*r + a**2
    Sigma = r**2 + a**2*cos(th)**2
    A = (r**2 + a**2)**2 - Delta*a**2*sin(th)**2

    # Next compute the radially-dependent part of log(enthalpy), ln_h
    tmp3 = sqrt(1 + 4*l**2*Sigma**2*Delta/(A*sin(th))**2)
    # Term 1 of Eq 3.6
    ln_h  = Rational(1,2)*log( ( 1 + tmp3) / (Sigma*Delta/A))
    # Term 2 of Eq 3.6
    ln_h -= Rational(1,2)*tmp3
    # Term 3 of Eq 3.6
    ln_h -= 2*a*M*r*l/A


    # Next compute the radially-INdependent part of log(enthalpy), ln_h
    # Note that there is some typo in the expression for these terms given in Eq 3.6, so we opt to just evaluate
    #   negative of the first three terms at r=r_in and th=pi/2 (the integration constant), as described in
    #   the text below Eq. 3.6, basically just copying the above lines of code.
    # Delin = Delta_in ; Sigin = Sigma_in ; Ain = A_in .
    Delin = r_in**2 - 2*M*r_in + a**2
    Sigin = r_in**2 + a**2*cos(pi/2)**2
    Ain   = (r_in**2 + a**2)**2 - Delin*a**2*sin(pi/2)**2

    tmp3in = sqrt(1 + 4*l**2*Sigin**2*Delin/(Ain*sin(pi/2))**2)
    # Term 4 of Eq 3.6
    mln_h_in  = -Rational(1,2)*log( ( 1 + tmp3in) / (Sigin*Delin/Ain))
    # Term 5 of Eq 3.6
    mln_h_in += Rational(1,2)*tmp3in
    # Term 6 of Eq 3.6
    mln_h_in += 2*a*M*r_in*l/Ain

    hm1 = exp(ln_h + mln_h_in) - 1

    # Python 3.4 + sympy 1.0.0 has a serious problem taking the power here, hangs forever.
    # so instead we use the identity x^{1/y} = exp( [1/y] * log(x) )
    # Original expression (works with Python 2.7 + sympy 0.7.4.1):
    # rho0 = ( hm1*(gamma-1)/(kappa*gamma) )**(1/(gamma - 1))
    # New expression (workaround):
    rho0 = exp( (1/(gamma-1)) * log( hm1*(gamma-1)/(kappa*gamma) ))
    Pressure0 = kappa * rho0**gamma

    # Eq 3.3: First compute exp(-2 chi), assuming Boyer-Lindquist coordinates
    #    Eq 2.16: chi = psi - nu, so
    #    Eq 3.5 -> exp(-2 chi) = exp(-2 (psi - nu)) = exp(2 nu)/exp(2 psi)
    exp2nu  = Sigma*Delta / A
    exp2psi = A*sin(th)**2 / Sigma
    expm2chi = exp2nu / exp2psi

    # Eq 3.3: Next compute u_(phi).
    u_pphip = sqrt((-1 + sqrt(1 + 4*l**2*expm2chi))/2)
    # Eq 2.13: Compute u_(t)
    u_ptp   = -sqrt(1 + u_pphip**2)

    # Next compute spatial components of 4-velocity in Boyer-Lindquist coordinates:
    uBL4D = [sympify(0) for i in range(4)]
    uBL4D[1] = 0 # u_r
    uBL4D[2] = 0 # u_theta
    # Eq 2.12 (typo): u_(phi) = e^(-psi) u_phi -> u_phi = e^(psi) u_(phi)
    uBL4D[3] = sqrt(exp2psi)*u_pphip

    # Assumes Boyer-Lindquist coordinates:
    omega = 2*a*M*r/A
    # Eq 2.13: u_(t) = 1/sqrt(exp2nu) * ( u_t + omega*u_phi )
    #     -->  u_t = u_(t) * sqrt(exp2nu) - omega*u_phi
    #     -->  u_t = u_ptp * sqrt(exp2nu) - omega*uBL4D[3]
    uBL4D[0] = u_ptp*sqrt(exp2nu) - omega*uBL4D[3]
    # Eq. 3.5:
    # w = 2*a*M*r/A;
    # Eqs. 3.5 & 2.1:
    # gtt = -Sig*Del/A + w^2*Sin[th]^2*A/Sig;
    # gtp = w*Sin[th]^2*A/Sig;
    # gpp = Sin[th]^2*A/Sig;
    # FullSimplify[Inverse[{{gtt,gtp},{gtp,gpp}}]]
    gPhys4BLUU = [[sympify(0) for i in range(4)] for j in range(4)]
    gPhys4BLUU[0][0] = -A/(Delta*Sigma)
    # DO NOT NEED TO SET gPhys4BLUU[1][1] or gPhys4BLUU[2][2]!
    gPhys4BLUU[0][3] = gPhys4BLUU[3][0] = -2*a*M*r/(Delta*Sigma)
    gPhys4BLUU[3][3] = -4*a**2*M**2*r**2/(Delta*A*Sigma) + Sigma**2/(A*Sigma*sin(th)**2)

    uBL4U = [sympify(0) for i in range(4)]
    for i in range(4):
        for j in range(4):
            uBL4U[i] += gPhys4BLUU[i][j]*uBL4D[j]

    # https://github.com/atchekho/harmpi/blob/master/init.c
    # Next transform Boyer-Lindquist velocity to Kerr-Schild basis:
    transformBLtoKS = [[sympify(0) for i in range(4)] for j in range(4)]
    for i in range(4):
        transformBLtoKS[i][i] = 1
    transformBLtoKS[0][1] = 2*r/(r**2 - 2*r + a*a)
    transformBLtoKS[3][1] =   a/(r**2 - 2*r + a*a)
    uKS4U = [sympify(0) for i in range(4)]
    for i in range(4):
        for j in range(4):
            uKS4U[i] += transformBLtoKS[i][j]*uBL4U[j]


    # Adopt the Kerr-Schild metric for Fishbone-Moncrief disks
    # http://gravity.psu.edu/numrel/jclub/jc/Cook___LivRev_2000-5.pdf
    # Alternatively, Appendix of https://arxiv.org/pdf/1704.00599.pdf
    rhoKS2  = r**2 + a**2*cos(th)**2 # Eq 79 of Cook's Living Review article
    DeltaKS = r**2 - 2*M*r + a**2    # Eq 79 of Cook's Living Review article
    alphaKS = 1/sqrt(1 + 2*M*r/rhoKS2)
    betaKSU = [sympify(0) for i in range(3)]
    betaKSU[0] = alphaKS**2*2*M*r/rhoKS2
    gammaKSDD = [[sympify(0) for i in range(3)] for j in range(3)]
    gammaKSDD[0][0] = 1 + 2*M*r/rhoKS2
    gammaKSDD[0][2] = gammaKSDD[2][0] = -(1 + 2*M*r/rhoKS2)*a*sin(th)**2
    gammaKSDD[1][1] = rhoKS2
    gammaKSDD[2][2] = (r**2 + a**2 + 2*M*r/rhoKS2 * a**2*sin(th)**2) * sin(th)**2

    AA = a**2 * cos(2*th) + a**2 + 2*r**2
    BB = AA + 4*M*r
    DD = sqrt(2*M*r / (a**2 * cos(th)**2 + r**2) + 1)
    KDD[0][0] = DD*(AA + 2*M*r)/(AA**2*BB) * (4*M*(a**2 * cos(2*th) + a**2 - 2*r**2))
    KDD[0][1] = KDD[1][0] = DD/(AA*BB) * 8*a**2*M*r*sin(th)*cos(th)
    KDD[0][2] = KDD[2][0] = DD/AA**2 * (-2*a*M*sin(th)**2 * (a**2 * cos(2*th) + a**2 - 2*r**2))
    KDD[1][1] = DD/BB * 4*M*r**2
    KDD[1][2] = KDD[2][1] = DD/(AA*BB) * (-8*a**3*M*r*sin(th)**3*cos(th))
    KDD[2][2] = DD/(AA**2*BB) * \
                (2*M*r*sin(th)**2 * (a**4*(r-M)*cos(4*th) + a**4*(M+3*r) +
                 4*a**2*r**2*(2*r-M) + 4*a**2*r*cos(2*th)*(a**2 + r*(M+2*r)) + 8*r**5))

    # For compatibility, we must compute gPhys4UU
    gammaKSUU = [[sympify(0) for i in range(3)] for j in range(3)]
    gammaKSDET = symbols('gammaKSDET', positive="True")
    gammaKSDET = symm_matrix_inverter3x3(gammaKSDD, gammaKSUU)

    # See, e.g., Eq. 4.49 of https://arxiv.org/pdf/gr-qc/0703035.pdf , where N = alpha
    gPhys4UU[0][0] = -1 / alphaKS**2
    for i in range(1,4):
        if i>0:
            # if the quantity does not have a "4", then it is assumed to be a 3D quantity.
            #  E.g., betaKSU[] is a spatial vector, with indices ranging from 0 to 2:
            gPhys4UU[0][i] = gPhys4UU[i][0] = betaKSU[i-1]/alphaKS**2
    for i in range(1,4):
        for j in range(1,4):
            # if the quantity does not have a "4", then it is assumed to be a 3D quantity.
            #  E.g., betaKSU[] is a spatial vector, with indices ranging from 0 to 2,
            #    and gammaKSUU[][] is a spatial tensor, with indices again ranging from 0 to 2.
            gPhys4UU[i][j] = gPhys4UU[j][i] = gammaKSUU[i-1][j-1] - betaKSU[i-1]*betaKSU[j-1]/alphaKS**2

    A_b = symbols('A_b', real=True)
    ID_protected_variables += ["A_b"]

    A_3vecpotentialD = [sympify(0) for i in range(3)]
    # Set A_phi = A_b*rho0 FIXME: why is there a sign error?
    A_3vecpotentialD[2] = -A_b * rho0

    BtildeU = [sympify(0) for i in range(3)]
    # Eq 15 of https://arxiv.org/pdf/1501.07276.pdf:
    # B = curl A -> B^r = d_th A_ph - d_ph A_th
    BtildeU[0] = diff(A_3vecpotentialD[2],th) - diff(A_3vecpotentialD[1],ph)
    # B = curl A -> B^th = d_ph A_r - d_r A_ph
    BtildeU[1] = diff(A_3vecpotentialD[0],ph) - diff(A_3vecpotentialD[2],r)
    # B = curl A -> B^ph = d_r A_th - d_th A_r
    BtildeU[2] = diff(A_3vecpotentialD[1],r)  - diff(A_3vecpotentialD[0],th)

else:
    print("ERROR: You chose an unsupported ID_scheme. Check for spelling issues or special characters and try again.")
    exit(1)

# Apply conformal transformations:
#  Define the IllinoisGRMHD conformal factor in the spherical basis.
ghat0DD = [[sympify(0) for i in range(3)] for j in range(3)]
ghat0DD[0][0] = 1
ghat0DD[1][1] = r**2
ghat0DD[2][2] = r**2 * sin(th)**2

# Construct spacetime metric in 3+1 form:
# See, e.g., Eq. 4.49 of https://arxiv.org/pdf/gr-qc/0703035.pdf , where N = alpha
alpha = sqrt(1/(-gPhys4UU[0][0]))
betaU = [sympify(0) for i in range(3)]
for i in range(3):
    betaU[i] = alpha**2 * gPhys4UU[0][i+1]
gammaUU = [[sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        gammaUU[i][j] = gPhys4UU[i+1][j+1] + betaU[i]*betaU[j]/alpha**2
# For compatibility, we must compute gPhys4UU
gammaDD = [[sympify(0) for i in range(3)] for j in range(3)]
gammaDET = symbols('gammatildeDET', positive="True")
# symm_matrix_inverter3x3 will output determinant if first argument is gammaDD. 1/determinant otherwise:
gammaDET = 1/symm_matrix_inverter3x3(gammaUU, gammaDD)

###############
# Next compute g_{\alpha \beta} from lower 3-metric, using
# Eq 4.47 of https://arxiv.org/pdf/gr-qc/0703035.pdf
betaD = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        betaD[i] += gammaDD[i][j]*betaU[j]

beta2 = sympify(0)
for i in range(3):
    beta2 += betaU[i]*betaD[i]

gPhys4DD = [[sympify(0) for i in range(4)] for j in range(4)]
gPhys4DD[0][0] = -alpha**2 + beta2
for i in range(3):
    gPhys4DD[0][i+1] = gPhys4DD[i+1][0] = betaD[i]
    for j in range(3):
        gPhys4DD[i+1][j+1] = gammaDD[i][j]
###############
# Next compute b^{\mu} using Eqs 23 and 31 of https://arxiv.org/pdf/astro-ph/0503420.pdf
uKS4D = [sympify(0) for i in range(4)]
for i in range(4):
    for j in range(4):
        uKS4D[i] += gPhys4DD[i][j] * uKS4U[j]

# Eq 27 of https://arxiv.org/pdf/astro-ph/0503420.pdf
BU = [sympify(0) for i in range(3)]
for i in range(3):
    BU[i] = BtildeU[i]/sqrt(gammaDET)

# Eq 23 of https://arxiv.org/pdf/astro-ph/0503420.pdf
BU0_u = sympify(0)
for i in range(3):
    BU0_u += uKS4D[i+1]*BU[i]/alpha

smallbU = [sympify(0) for i in range(4)]
smallbU[0] = BU0_u   / sqrt(4 * pi)
# Eqs 24 and 31 of https://arxiv.org/pdf/astro-ph/0503420.pdf
for i in range(3):
    smallbU[i+1] = (BU[i]/alpha + BU0_u*uKS4U[i+1])/(sqrt(4*pi)*uKS4U[0])

smallbD = [sympify(0) for i in range(4)]
for i in range(4):
    for j in range(4):
        smallbD[i] += gPhys4DD[i][j]*smallbU[j]

smallb2 = sympify(0)
for i in range(4):
    smallb2 += smallbU[i]*smallbD[i]
###############
LorentzFactor = alpha * uKS4U[0]
# Define Valencia 3-velocity v^i_(n), which sets the 3-velocity as measured by normal observers to the spatial slice:
#  v^i_(n) = u^i/(u^0*alpha) + beta^i/alpha. See eq 11 of https://arxiv.org/pdf/1501.07276.pdf
Valencia3velocityU = [sympify(0) for i in range(3)]
for i in range(3):
    Valencia3velocityU[i] = uKS4U[i + 1] / (alpha * uKS4U[0]) + betaU[i]

NRPy_file_output(OUTDIR+"/standalone-spherical_coords/NRPy_codegen/KerrSchild.h", [],[],[],
                 ID_protected_variables + ["r","th","ph"],
                 [],[alpha, "alp", betaU[0], "betaUr", betaU[1], "betaUth", betaU[2], "betaUph",
                     gammaDD[0][0], "grr", gammaDD[0][1], "grth", gammaDD[0][2], "grph",
                     gammaDD[1][1], "gthth", gammaDD[1][2], "gthph", gammaDD[2][2], "gphph",
                     KDD[0][0], "krr", KDD[0][1], "krth", KDD[0][2], "krph",
                     KDD[1][1], "kthth", KDD[1][2], "kthph", KDD[2][2], "kphph",sqrt(gammaDET)*alpha ,"sqrtgamma4DET"])
NRPy_file_output(OUTDIR+"/standalone-spherical_coords/NRPy_codegen/FMdisk_Lorentz_uUs.h", [],[],[],
                 ID_protected_variables + ["r","th","ph"],
                 [],[LorentzFactor, "LorentzFactor", uKS4U[1], "uUr",uKS4U[2], "uUth",uKS4U[3], "uUph"])
NRPy_file_output(OUTDIR+"/standalone-spherical_coords/NRPy_codegen/FMdisk_hm1_rho_P.h", [],[],[],
                 ID_protected_variables + ["r","th","ph"],
                 [],[hm1, "hm1", rho0, "rho0",Pressure0, "Pressure0"])
udotu = sympify(0)
for i in range(4):
    udotu += uKS4U[i]*uKS4D[i]
NRPy_file_output(OUTDIR+"/standalone-spherical_coords/NRPy_codegen/FMdisk_Btildes.h", [],[],[],
                 ID_protected_variables + ["r","th","ph"],
                 [],[uKS4U[0], "uKS4Ut", uKS4U[1],"uKS4Ur", uKS4U[2],"uKS4Uth", uKS4U[3],"uKS4Uph",
                     uKS4D[0], "uKS4Dt", uKS4D[1],"uKS4Dr", uKS4D[2],"uKS4Dth", uKS4D[3],"uKS4Dph",
                     uKS4D[1] * BU[0] / alpha, "Bur", uKS4D[2] * BU[1] / alpha, "Buth", uKS4D[3] * BU[2] / alpha, "Buph",
                     gPhys4DD[0][0], "g4DD00", gPhys4DD[0][1], "g4DD01",gPhys4DD[0][2], "g4DD02",gPhys4DD[0][3], "g4DD03",
                     BtildeU[0], "BtildeUr", BtildeU[1], "BtildeUth",BtildeU[2], "BtildeUph",
                     smallbU[0], "smallbUt", smallbU[1], "smallbUr", smallbU[2], "smallbUth",smallbU[3], "smallbUph",
                     smallb2,"smallb2",udotu,"udotu"])


# Now that all derivatives of ghat and gbar have been computed,
# we may now substitute the definitions r = r_refmetric, th=th_refmetric,...
# WARNING: Substitution only works when the variable is not an integer. Hence the if not isinstance(...,...) stuff.
# If the variable isn't an integer, we revert transcendental functions inside to normal variables. E.g., sin(x2) -> sinx2
#  Reverting to normal variables in this way makes expressions simpler in NRPy, and enables transcendental functions
#  to be pre-computed in SENR.

alpha = sympify_integers__replace_rthph(alpha,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
for i in range(3):
    betaU[i] = sympify_integers__replace_rthph(betaU[i], r, th, ph, r_refmetric, th_refmetric, ph_refmetric)
    for j in range(3):
        gammaDD[i][j] = sympify_integers__replace_rthph(gammaDD[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        KDD[i][j] = sympify_integers__replace_rthph(KDD[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

# GRMHD variables:
# Density and pressure:
hm1           = sympify_integers__replace_rthph(hm1,           r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
rho0          = sympify_integers__replace_rthph(rho0,          r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
Pressure0     = sympify_integers__replace_rthph(Pressure0,     r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
LorentzFactor = sympify_integers__replace_rthph(LorentzFactor, r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
# "Valencia" three-velocity
for i in range(3):
    BtildeU[i] = sympify_integers__replace_rthph(BtildeU[i], r, th, ph, r_refmetric, th_refmetric, ph_refmetric)
    uKS4U[i+1] = sympify_integers__replace_rthph(uKS4U[i+1], r, th, ph, r_refmetric, th_refmetric, ph_refmetric)
    Valencia3velocityU[i] = sympify_integers__replace_rthph(Valencia3velocityU[i], r, th, ph, r_refmetric, th_refmetric, ph_refmetric)



# uUphi = uKS4U[3]
# uUphi = sympify_integers__replace_rthph(uUphi,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
# uUt = uKS4U[0]
# uUt = sympify_integers__replace_rthph(uUt,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)


r = r_refmetric
th = th_refmetric
ph = ph_refmetric

# Transform initial data to our coordinate system:
# First compute Jacobian and its inverse
drrefmetric__dx_0UDmatrix = Matrix([[diff( r_refmetric,xx[0]), diff( r_refmetric,xx[1]), diff( r_refmetric,xx[2])],
                                    [diff(th_refmetric,xx[0]), diff(th_refmetric,xx[1]), diff(th_refmetric,xx[2])],
                                    [diff(ph_refmetric,xx[0]), diff(ph_refmetric,xx[1]), diff(ph_refmetric,xx[2])]])
dx__drrefmetric_0UDmatrix = drrefmetric__dx_0UDmatrix.inv()


IDalpha = alpha
IDgammaDD = [[sympify(0) for i in range(3)] for j in range(3)]
IDKDD = [[sympify(0) for i in range(3)] for j in range(3)]
IDbetaU   = [sympify(0) for i in range(3)]

IDValencia3velocityU = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        # Matrices are stored in row, column format, so (i,j) <-> (row,column)
        IDbetaU[i]   += dx__drrefmetric_0UDmatrix[(i,j)]*betaU[j]
        IDValencia3velocityU[i]   += dx__drrefmetric_0UDmatrix[(i,j)]*Valencia3velocityU[j]
        for k in range(3):
            for l in range(3):
                IDgammaDD[i][j] += drrefmetric__dx_0UDmatrix[(k,i)]*drrefmetric__dx_0UDmatrix[(l,j)]*gammaDD[k][l]
                IDKDD[i][j]     += drrefmetric__dx_0UDmatrix[(k,i)]*drrefmetric__dx_0UDmatrix[(l,j)]*    KDD[k][l]


# -={ Spacetime quantities: Generate C code from expressions and output to file }=-
NRPy_file_output(OUTDIR+"/FishboneMoncrief_ET_thorn/src/NRPy_codegen/KerrSchild.h", [],[],[], ID_protected_variables,
                 [],[IDalpha,"alp[idx]",IDbetaU[0],"betax[idx]",IDbetaU[1],"betay[idx]",IDbetaU[2],"betaz[idx]",
                     IDgammaDD[0][0], "gxx[idx]",IDgammaDD[0][1],"gxy[idx]",IDgammaDD[0][2],"gxz[idx]",
                     IDgammaDD[1][1], "gyy[idx]",IDgammaDD[1][2],"gyz[idx]",IDgammaDD[2][2],"gzz[idx]",
                     IDKDD[0][0], "kxx[idx]",IDKDD[0][1],"kxy[idx]",IDKDD[0][2],"kxz[idx]",
                     IDKDD[1][1], "kyy[idx]",IDKDD[1][2],"kyz[idx]",IDKDD[2][2],"kzz[idx]"])

# -={ GRMHD quantities: Generate C code from expressions and output to file }=-
NRPy_file_output(OUTDIR+"/FishboneMoncrief_ET_thorn/src/NRPy_codegen/FMdisk_GRHD_hm1.h", [],[],[], ID_protected_variables,
                 [],[hm1,"hm1"])
NRPy_file_output(OUTDIR+"/FishboneMoncrief_ET_thorn/src/NRPy_codegen/FMdisk_GRHD_velocities.h", [],[],[], ID_protected_variables,
                 [],[LorentzFactor,"w_lorentz[idx]", 
                     IDValencia3velocityU[0], "velx[idx]", IDValencia3velocityU[1], "vely[idx]",IDValencia3velocityU[2], "velz[idx]"])

stoptimer_IllinoisGRMHDID = time.time()
print("Completed IllinoisGRMHD initial data code generation in \t\t"+str(round(stoptimer_IllinoisGRMHDID-starttimer_IllinoisGRMHDID,2))+" seconds")
