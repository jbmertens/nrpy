// Set scalarwave initial data
void Set_Initial_Data(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" //Located in ../../common_functions/

  //#define sign(x) (((x) > 0) - ((x) < 0))

  LOOP_NOGZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL t = 0.0;
    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];

#include "NRPy_codegen/Initial_Data.h"
  } // END for(ii, jj, kk)

  return;
}
