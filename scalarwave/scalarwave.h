#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <time.h>

#include "loop_setup.h"
#include "defines_for_scalarwave_variables.h"
#include "input_parameters.h"
// Declare the struct datatype for precomputed hatted (reference-metric-related) quantities.
#include "reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h" // Located in ../common_functions/

// Boundary condition function declarations
#include "boundary_conditions/boundary_condition_include.h" // Located in ../common_functions/

// Find the minimum grid spacing so we can set timestep
#include "Find_Time_Step.c" // Located in ../common_functions/

// Set initial data: TODO
#include "initial_data/Set_Initial_Data.c" 
void scalarwave_RHS(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);

// Right-hand-side (RHS) evaluation driver:
#include "evolution_equations/Evaluate_RHS.c"

// Diagnostics
#include "diagnostics/Grid_Function_Reduction.c" // Located in ../common_functions/
#include "diagnostics/Data_File_Output.c" // Depends on diagnostics/Grid_Function_Reduction.c. Located in ../common_functions/
#include "Checkpoint.c"  // Located in ../common_functions/
#include "NaN_Checker.c" // Located in ../common_functions/
