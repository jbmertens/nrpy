#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "loop_setup.h" // Located in ../../common_functions/
#include "../defines_for_scalarwave_variables.h"
// Declare the struct datatype for precomputed hatted (reference-metric-related) quantities.
#include "reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h" // Located in ../../common_functions/

#include "SIMD_header.h" // Located in ../../common_functions/

// scalarwave RHS computation:
void scalarwave_RHS(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params)
{

#if defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)
  SIMD_WARNING_MESSAGE;

#include "parameters_readin_SIMD-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs_SIMD.h" // Located in ../../common_functions/

#include "NRPy_codegen/NRPy_scalarwave_RHS.h-SIMDconstants.h"

  START_LOOP_NOGZFILL_SIMD(ii,jj,kk) {
#include "NRPy_codegen/NRPy_scalarwave_RHS.h"
  } END_LOOP_NOGZFILL_SIMD;
  
#else
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/
    // pragma omp parallel for collapse(2)
#pragma omp parallel for
    for(int kk=NGHOSTS;kk<Npts3-NGHOSTS;kk++) {
      const REAL x3 = x3G[kk];
      for(int jj=NGHOSTS;jj<Npts2-NGHOSTS;jj++) {
        const REAL x2 = x2G[jj];
#pragma ivdep
#pragma vector always
        for(int ii=NGHOSTS;ii<Npts1-NGHOSTS;ii++) {
          const int idx = IDX3(ii, jj, kk);
          const REAL x1 = x1G[ii];
#include "NRPy_codegen/NRPy_scalarwave_RHS.h"
        }
      }
    }
#endif
  return;
}
