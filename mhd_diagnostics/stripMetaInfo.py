from math import sin, cos, sqrt
import numpy as np
import struct
import sys

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

infile=sys.argv[1]
outfile=sys.argv[2]

print "reading from ",infile
print "writing to ",outfile

g = open(outfile,"wb")

with open(infile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    Nr = len(rs)
    Nth = len(ths)
    Nph = len(phs)

# from here on, everything we read in gets written to outfile

    bytechunk=f.read(8)

    g.write(bytechunk)

    Nvar = 8

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        bytechunk=f.read(full_timeslice_size)
        buffer_res = np.frombuffer(bytechunk)
        print 'size of buffer = ',buffer_res.size
        g.write(bytechunk)

        bytechunk=f.read(8)
        g.write(bytechunk)


f.close()
g.close()
