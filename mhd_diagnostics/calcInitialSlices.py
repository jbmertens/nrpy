from math import sin, cos, sqrt
import numpy as np
import struct
import matplotlib.pyplot as plt
import sys
import numpy.linalg as linalg
from pylab import *
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph

def input_from_file(datafile):
    
    with open(datafile,"rb") as f:

        f.seek(0, 2)
        eof = f.tell()
        f.seek(0, 0)

        Nr=struct.unpack('i',f.read(4))[0]
        Nth=struct.unpack('i',f.read(4))[0]
        Nph=struct.unpack('i',f.read(4))[0]
        print "Nr,Nth,Nph:",Nr,Nth,Nph
        r_min=struct.unpack('d',f.read(8))[0]
        r_max=struct.unpack('d',f.read(8))[0]
        print r_min,"<= r <=",r_max
        th_min=struct.unpack('d',f.read(8))[0]
        th_max=struct.unpack('d',f.read(8))[0]
        print th_min,"<= th <=",th_max
        ph_min=struct.unpack('d',f.read(8))[0]
        ph_max=struct.unpack('d',f.read(8))[0]
        print ph_min,"<= ph <=",ph_max

        dr=(r_max-r_min)/(Nr-1.0);
        rs=np.arange(r_min,r_max+dr,dr)
        dth=(th_max-th_min)/(Nth-1.0);
        ths=np.arange(th_min,th_max+dth,dth)
        dph=(ph_max-ph_min)/(Nph-1.0);
        phs=np.arange(ph_min,ph_max+dph,dph)
        magicnum=0
        data=[]
        iters=[]
        times=[]
        
        bytechunk=f.read(8)

        Nvar = 8

        full_timeslice_size = Nvar*Nph*Nth*Nr*8
        full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

        print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

        print "len bytechunk=",len(bytechunk)
        while (bytechunk ):

            num=struct.unpack('d',bytechunk)[0]
            if(magicnum==0): 
                magicnum=num  
                print "magicnum=",magicnum
            elif(not magicnum==num):
                print"magicnum mismatch"
                print "got num=",num
                return data

            this_iter = struct.unpack('i',f.read(4))[0]
            this_time = struct.unpack('d',f.read(8))[0]
            print "reading slab it = ", this_iter," time = ", this_time

            file_place = f.tell()            
            data_left = eof - file_place
            print "only ", data_left, " bytes left in file"

            if(data_left < full_timeslice_size_inc_meta):
                print "not enough data for another time slice --- sorry!"
                break

            bytechunk=f.read(full_timeslice_size)
            buffer_res = np.frombuffer(bytechunk)
            print 'size of buffer = ',buffer_res.size

            iters.append(this_iter)
            times.append(this_time)
            data.append(buffer_res.reshape(Nvar,Nph,Nth,Nr))
        
            bytechunk=f.read(8)

    return rs,ths,phs,times,data

def compute_KS_metric(r,th,ph,M=1,a=0.9375):
    x = r*sin(th)*cos(ph)
    y = r*sin(th)*sin(ph)
    z = r*cos(th)

    #rBL = np.sqrt( (r**2 - a**2 + np.sqrt((r**2 - a**2)**2 + 4.0*a**2*z**2) )/2.0)
    rBL = r

    if (rBL < 1.0e-4):
        print "z = ", z
        print "r = ", r
        print "r^2 - a^2 = ", r**2 - a**2
        print "next expression = ", r**2 - a**2 + np.sqrt((r**2 - a**2)**2)
        print "rBL = ", rBL

    HKS = M*rBL**3/(rBL**4 + a**2*z**2)
    lKS = np.zeros(4)
    lKS[0] = 1.0
    lKS[1] = (rBL*x+a*y)/(rBL**2+a**2)
    lKS[2] = (rBL*y-a*x)/(rBL**2+a**2)
    lKS[3] = z/rBL

    g4KS = np.zeros((4,4))
    g4KS[0][0] = -1.0
    g4KS[1][1] =  1.0
    g4KS[2][2] =  1.0
    g4KS[3][3] =  1.0
    for i in range(4):
        for j in range(4):
            g4KS[i][j] = g4KS[i][j] + 2.0*HKS*lKS[i]*lKS[j]

    g4KSUU = linalg.inv(g4KS)

    return g4KS, g4KSUU

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

# hard-coded mass & spin
M_BH=1
chi_BH=0.9375
#M_BH=0.97
#chi_BH=0.69


datafile=sys.argv[1]

#rs,ths,phs,times,data=input_from_file("interp_sph_grids_test_data_dec15.dat")
print "reading from",datafile
rs,ths,phs,times,data=input_from_file(datafile)

print 'times are ',times

do_plots = 1

Nt = len(times)
print 'Nt = ', Nt

# taking from thorn interpolator code
# data col 0 = rho
# data col 1 = P
# data col 2 = vx
# data col 3 = vy
# data col 4 = vz
# data col 5 = Bx
# data col 6 = By
# data col 7 = Bz

#print 'theta = ', ths
#print 'phi = ',phs
#print 'r = ',rs

Nr = len(rs)
print 'Nr = ', Nr

Nth = len(ths)
print 'Nth = ', Nth

Nph = len(phs)

jj_eq = Nth/2

Bsqs = np.zeros(Nr)
vsqs = np.zeros(Nr)
Pmags = np.zeros(Nr)
betainv = np.zeros(Nr)
w_lorentz = np.zeros(Nr)
smallb2s = np.zeros(Nr)

inner_edge = 6.0

GAMMA_SPEED_LIMIT = 100.0

chi_temp = 1.0+np.sqrt(1.0-chi_BH**2)
#rh_sq = 2.0*M**2*( chi**2*chi_temp + (4.0*chi_temp - chi**2*(2.0+chi_temp))*costh**2 )/( chi**2 + 2.0*(2.0-chi**2)*costh**2 + chi**2*costh**4 )
#rh = np.sqrt(rh_sq)
rh = M_BH*chi_temp

#TEMP ...
rh = 5.0

print 'rh = ', rh

ii_high = 0
while (rs[ii_high] < rh):
    ii_high = ii_high+1

ii_low = ii_high-1

print 'horizon radius = ', rh,', sandwiched between grid values ',rs[ii_low],' and ',rs[ii_high] 
w_low = (rh-rs[ii_low])/(rs[ii_high]-rs[ii_low]) # weighting factor for interpolation

print 'weighting factor = ', w_low

#exit()

## note format of read-in files:
## data[time_idx][field_idx,phi_idx,theta_idx,r_idx]

for ii in range(Nr):
    if(rs[ii]>inner_edge):
        g4KS, g4KSUU = compute_KS_metric(rs[ii],ths[jj_eq],phs[0],M=M_BH,a=chi_BH)

        # extract upper-index v-fields
        vx = data[0][2,0,jj_eq,ii]
        vy = data[0][3,0,jj_eq,ii]
        vz = data[0][4,0,jj_eq,ii]
        # extract upper-index B-fields
        Bx = data[0][5,0,jj_eq,ii]
        By = data[0][6,0,jj_eq,ii]
        Bz = data[0][7,0,jj_eq,ii]

        BDx = g4KS[1][1]*Bx + g4KS[1][2]*By + g4KS[1][3]*Bz
        BDy = g4KS[2][1]*Bx + g4KS[2][2]*By + g4KS[2][3]*Bz
        BDz = g4KS[3][1]*Bx + g4KS[3][2]*By + g4KS[3][3]*Bz
   
        Bsqs[ii] = Bx*BDx + By*BDy + Bz*BDz 

        vDx = g4KS[1][1]*vx + g4KS[1][2]*vy + g4KS[1][3]*vz
        vDy = g4KS[2][1]*vx + g4KS[2][2]*vy + g4KS[2][3]*vz
        vDz = g4KS[3][1]*vx + g4KS[3][2]*vy + g4KS[3][3]*vz
   
        vsqs[ii] = vx*vDx + vy*vDy + vz*vDz 

        rho = data[0][0,0,jj_eq,ii]
        P = data[0][1,0,jj_eq,ii]

        lapse = np.sqrt(-1.0/g4KSUU[0][0])
        shiftx = g4KSUU[0][1]
        shifty = g4KSUU[0][2]
        shiftz = g4KSUU[0][3]
        gxx = g4KS[1][1]
        gxy = g4KS[1][2]
        gxz = g4KS[1][3]
        gyy = g4KS[2][2]
        gyz = g4KS[2][3]
        gzz = g4KS[3][3]

        ONE_OVER_LAPSE_SQRT_4PI = 1.0/lapse/np.sqrt(4.0*np.pi)

        alp_veulerx = shiftx+vx
        alp_veulery = shifty+vy
        alp_veulerz = shiftz+vz
        #alp_veulerx = vx
        #alp_veuler_y = vy
        #alp_veuler_z = vz

        # Eq. 56 in http:#arxiv.org/pdf/astro-ph/0503420.pdf:
        #  u_i = gamma_{ij} u^0 (v^j + beta^j), gamma_{ij} is the physical metric
        alp_veuler_x = gxx*alp_veulerx + gxy*alp_veulery + gxz*alp_veulerz
        alp_veuler_y = gxy*alp_veulerx + gyy*alp_veulery + gyz*alp_veulerz
        alp_veuler_z = gxz*alp_veulerx + gyz*alp_veulery + gzz*alp_veulerz

        veuler_squared = (alp_veuler_x*alp_veulerx + alp_veuler_y*alp_veulery + alp_veuler_z*alp_veulerz)/lapse**2

        if(veuler_squared > 1.0) :
            ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0-1.0/GAMMA_SPEED_LIMIT**2
            veuler_squared=ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED

        wlor = 1.0/np.sqrt(1.0-veuler_squared) # == w_lor
        u0 = wlor/lapse
 
        # NOW COMPUTE b^{\mu} and b^2 = b^{\mu} b^{\nu} g_{\mu \nu}

        # Eqs. 23 and 31 in http://arxiv.org/pdf/astro-ph/0503420.pdf:
        #   Compute alpha sqrt(4 pi) b^t = u_i B^i
        alpha_sqrt_4pi_bt = ( alp_veuler_x*Bx + alp_veuler_y*By + alp_veuler_z*Bz ) * u0

        # Eq. 24 in http://arxiv.org/pdf/astro-ph/0503420.pdf:
        # b^i = B^i_u / sqrt(4 pi)
        # b^i = ( B^i/u^0 +  alpha sqrt(4 pi) b^t v^i ) / ( alpha sqrt(4 pi) )
        smallbx = (Bx/u0 + vx*alpha_sqrt_4pi_bt)*ONE_OVER_LAPSE_SQRT_4PI
        smallby = (By/u0 + vy*alpha_sqrt_4pi_bt)*ONE_OVER_LAPSE_SQRT_4PI
        smallbz = (Bz/u0 + vz*alpha_sqrt_4pi_bt)*ONE_OVER_LAPSE_SQRT_4PI
        # Eq. 23 in http://arxiv.org/pdf/astro-ph/0503420.pdf, with alpha sqrt (4 pi) b^2 = u_i B^i already computed above
        smallbt = alpha_sqrt_4pi_bt * ONE_OVER_LAPSE_SQRT_4PI

        # POYNTING FLUX: */
        # S^i = -alp*Tem^i_0 = -alp*(b^2 u^i u_0 + b^2/2 g^i_0 - b^i b_0) 

        # First compute u_0.
        #uup[4] = {u0,u0*vx,u0*vy,u0*vz};
        #u_0=0.0; for(int ii=0;ii<4;ii++) u_0 += g4dn[0][ii]*uup[ii];
        u_0 = g4KS[0][0]*u0 + g4KS[0][1]*u0*vx + g4KS[0][2]*u0*vy + g4KS[0][3]*u0*vz


        # Next compute b_0.
        sbU = np.array([smallbt,smallbx,smallby,smallbz])

        sbD = np.zeros(4)
        for mu in range(4):
            for nu in range(4):
                sbD[mu] = sbD[mu] + g4KS[mu][nu]*sbU[nu]

        sb2 = 0.0
        for mu in range(4):
            sb2 = sb2 + sbD[mu]*sbU[mu]

        smallb2s[ii] = sb2
        w_lorentz[ii] = wlor

        Pmags[ii] = smallb2s[ii]/2.0
        betainv[ii] = Pmags[ii]/P


    else: #inside ring
        w_lorentz[ii] = 1.0

#print "rho[th=ph=0]=",data[0][0,0,0,:]
#print "rho[th=0,ph=N]=",data[0][0,0,-1,:]

######################################################
### Reproduce Fig. 1 from codeComparisonInfo document
######################################################

### read in reference data ###

refdata=loadtxt('torus_cuts.csv')
r_col=0
rho_col=1
p_col=2
lfac_col=4
b2_col=3

def calc_betainv(refdata):
    return refdata[:,b2_col]/2./refdata[:,p_col]

#Pfix = 4.255
#rshift = -1.0
Pfix = 1.0
rshift = 0.0

if(do_plots):

    f, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, sharex='col')
    f.subplots_adjust(wspace=.5)
    f.set_size_inches(10,4)

    # plot rho
    ax1.grid(True)
    ax1.plot(rs-rshift,data[0][0,0,jj_eq,:],'r-')
    ax1.plot(refdata[:,r_col],refdata[:,rho_col],'b--')
    ax1.set_ylabel(r'$\rho$')
    ax1.set_ylim(1e-8,1)
    ax1.set_yscale('log')

    # plot Pgas
    ax2.grid(True)
    ax2.plot(rs-rshift,Pfix*data[0][1,0,jj_eq,:],'r-')
    ax2.plot(refdata[:,r_col],refdata[:,p_col],'b--')
    ax2.set_ylabel(r'$P_{\rm gas}$')
    #ax2.set_ylim(1e-12,0.002)
    ax2.set_ylim(1e-12,0.2)
    ax2.set_yscale('log')

    ax3.grid(True)
    ax3.plot(rs-rshift,np.sqrt(smallb2s),'r-')
    ax3.plot(refdata[:,r_col],np.sqrt(refdata[:,b2_col]),'b--')
    ax3.set_ylabel(r'$\sqrt{b_\mu b^\mu}$')
    ax3.set_ylim(1.e-4,1.e-2)
    ax3.set_yscale('log')

    ax4.grid(True)
    ax4.plot(rs-rshift,betainv/Pfix,'r-')
    #ax4.plot(refdata[:,r_col],calc_betainv(refdata),'b--')
    ax4.plot(refdata[:,r_col],calc_betainv(refdata),'b--')
    ax4.set_ylabel(r'$\beta^{-1}$')
    ax4.set_xlabel(r'$r_{\rm KS} [GM/c^2]$')
    ax4.set_ylim(1.e-7,1.e-1)
    ax4.set_yscale('log')

    ax5.grid(True)
    ax5.plot(rs-rshift,w_lorentz,'r-')
    ax5.plot(refdata[:,r_col],refdata[:,lfac_col],'b--')
    ax5.set_ylabel(r'$\Gamma$')
    ax5.set_xlabel(r'$r_{\rm KS} [GM/c^2]$')
    ax5.set_ylim(0.98,1.25)

    ax6.grid(True)
    ax6.plot(rs-rshift,Pfix*data[0][1,0,jj_eq,:] + Pmags,'r-')
    #ax6.plot(refdata[:,r_col],refdata[:,p_col]+refdata[:,b2_col]/2.,'b--')
    ax6.plot(refdata[:,r_col],refdata[:,p_col]+refdata[:,b2_col]/2.,'b--')
    ax6.set_ylabel(r'$P_{\rm gas}+P_{\rm mag}$')
    ax6.set_xlabel(r'$r_{\rm KS} [GM/c^2]$')
    ax6.set_ylim(1e-12,0.01)
    ax6.set_yscale('log')


    plt.show()



