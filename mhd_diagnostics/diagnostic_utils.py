from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg

def compute_KS_metric_OLD(r,th,ph,M=1,a=0.9375):
    x = r*sin(th)*cos(ph)
    y = r*sin(th)*sin(ph)
    z = r*cos(th)

    #rBL = np.sqrt( (r**2 - a**2 + np.sqrt((r**2 - a**2)**2 + 4.0*a**2*z**2) )/2.0)
    rBL = r

    if (rBL < 1.0e-4):
        print "z = ", z
        print "r = ", r
        print "r^2 - a^2 = ", r**2 - a**2
        print "next expression = ", r**2 - a**2 + np.sqrt((r**2 - a**2)**2)
        print "rBL = ", rBL

    HKS = M*rBL**3/(rBL**4 + a**2*z**2)
    lKS = np.zeros(4)
    lKS[0] = 1.0
    lKS[1] = (rBL*x+a*y)/(rBL**2+a**2)
    lKS[2] = (rBL*y-a*x)/(rBL**2+a**2)
    lKS[3] = z/rBL

    g4KS = np.zeros((4,4))
    g4KS[0][0] = -1.0
    g4KS[1][1] =  1.0
    g4KS[2][2] =  1.0
    g4KS[3][3] =  1.0
    for i in range(4):
        for j in range(4):
            g4KS[i][j] = g4KS[i][j] + 2.0*HKS*lKS[i]*lKS[j]

    g4KSUU = linalg.inv(g4KS)

    return g4KS, g4KSUU


def compute_KS_metric_sph_OLD(r,th,ph,M,a):

    z = r*cos(th)

    #rBL = np.sqrt( (r**2 - a**2 + np.sqrt((r**2 - a**2)**2 + 4.0*a**2*z**2) )/2.0)
    rBL = r

    HKS = M*rBL**3/(rBL**4 + a**2*z**2)
    lKS = np.zeros(4)
    lKS[0] = 1.0
    lKS[1] = r*(rBL**2 + a**2*cos(th)**2)/rBL/(rBL**2+a**2)
    lKS[2] = -a**2*r**2*cos(th)*sin(th)/rBL/(rBL**2+a**2)
    lKS[3] = -a*r**2*sin(th)**2/(rBL**2+a**2)

    g4KS = eta4 = np.zeros((4,4))
    eta4[0][0] = -1.0
    eta4[1][1] =  1.0
    eta4[2][2] =  r**2
    eta4[3][3] =  r**2*sin(th)**2
    for i in range(4):
        for j in range(4):
            g4KS[i][j] = eta4[i][j] + 2.0*HKS*lKS[i]*lKS[j]

    EPS = 1.0e-6
    if r < EPS:
       r = EPS
    if abs(sin(th)) < EPS:
       th = EPS

    g4KSUU = eta4UU = np.zeros((4,4))
    eta4UU[0][0] = -1.0
    eta4UU[1][1] =  1.0
    eta4UU[2][2] =  1.0/r**2
    eta4UU[3][3] =  1.0/(r**2*sin(th)**2)

    lKSU = np.zeros(4)
    for i in range(4):
        for j in range(4):
            lKSU[i] = lKSU[i] + eta4UU[i][j]*lKS[j] 

    for i in range(4):
        for j in range(4):
            g4KSUU[i][j] = eta4UU[i][j] - 2.0*HKS*lKSU[i]*lKSU[j]

    return g4KS, g4KSUU

def compute_KS_metric_sph(rr,th,ph,M,a):

    cth = cos(th)
    sth = sin(th)
    s2=sth*sth

    a2 = a*a
    r2 = rr*rr
    Delta =r2-(2.*rr)+a2
    rho2 = r2 + a2*cth*cth
    A = ((r2+a2)**2)-a2*Delta*s2 

    gcov = np.zeros((4,4))

# covariant metric  
    gcov[0][0] = -1.0 + 2.0*rr/rho2          #- g_tt 
    gcov[0][1] =  2.0*rr/rho2              #- g_tr 
    gcov[0][3] = -2.0*a*rr*s2*(1.0/rho2) #- g_t phi 

    gcov[1][0] = gcov[0][1]        #- g_rt 
    gcov[1][1] = 1.0 + 2.0*rr/rho2      #- g_rr 
    gcov[1][3] = -a*s2*(1.0+2.0*rr/rho2) #- g_r phi 

    gcov[2][2] = rho2                #- g_th th 
 
    gcov[3][0] = gcov[0][3]        #- g_phi t 
    gcov[3][1] = gcov[1][3]        #- g_phi r 
    gcov[3][3] = A*s2*(1.0/rho2)         #- g_phi phi 

    EPS = 1.0e-6
    if rr < EPS:
        rr = EPS
    if abs(sin(th)) < EPS:
        th = EPS

    cth = cos(th)
    sth = sin(th)
    s2=sth*sth
    r2 = rr*rr
    Delta =r2-(2.*rr)+a2
    rho2 = r2 + a2*cth*cth
    A = ((r2+a2)**2)-a2*Delta*s2 

# contravariant metric 

    gcon = np.zeros((4,4))

    gcon[0][0] = -1.0 - 2.0*rr/rho2      #- g^tt 
    gcon[0][1] = 2.0*rr/rho2          #- g^tr 

    gcon[1][0] = gcon[0][1]        #- g^rt 
    gcon[1][1] = Delta/rho2           #- g^rr 
    gcon[1][3] = a*(1.0/rho2)      #- g^r phi 

    gcon[2][2] = 1.0/rho2            #- g_th th 
 
    gcon[3][1] = gcon[1][3]       #- g^phi r 
    gcon[3][3] = 1.0/(rho2*s2)       #- g^phi phi 

    return gcov, gcon

def convertVUCart2Sph(r,th,ph,Vx,Vy,Vz):

    rcyl = r*sin(th)
    if (abs(rcyl) < 1e-8):
        rcyl = 1e-8

    # Vr = (x/r)*Vx + (y/r)*Vy + (z/r)*Vz
    # Vth = (x*z/r**2/rcyl)*Vx + (y*z/r**2/rcyl)*Vy - (rcyl/r**2)*Vz
    # Vph = -(y/rcyl**2)*Vx + (x/rcyl**2)*Vy

    # Vr = sin(th)*cos(ph)*Vx + sin(th)*sin(ph)*Vy + cos(th)*Vz
    # Vth = sin(th)*cos(ph)*cos(th)/rcyl*Vx + sin(th)*sin(ph)*cos(th)/rcyl*Vy - rcyl/r**2*Vz
    # Vph = -r*sin(th)*sin(ph)/rcyl**2*Vx + r*sin(th)*cos(ph)/rcyl**2*Vy

    Vr = sin(th)*cos(ph)*Vx + sin(th)*sin(ph)*Vy + cos(th)*Vz
    Vth = cos(ph)*cos(th)/r*Vx + sin(ph)*cos(th)/r*Vy - sin(th)/r*Vz
    Vph = -sin(ph)/rcyl*Vx + cos(ph)/rcyl*Vy

    return Vr,Vth,Vph

def convertTDDCart2Sph(r,th,ph,Txx,Txy,Txz,Tyy,Tyz,Tzz):

    g3cart = np.array([ [Txx,Txy,Txz],[Txy,Tyy,Tyz],[Txz,Tyz,Tzz]])
    Mmat = np.array( [ [np.cos(ph)*np.sin(th), r*np.cos(th)*np.cos(ph), -r*np.sin(th)*np.sin(ph)], [np.sin(th)*np.sin(ph), r*np.cos(th)*np.sin(ph), r*np.cos(ph)*np.sin(th)], [np.cos(th), -r*np.sin(th), 0] ])
    MmatT = Mmat.transpose()
    g3sph = np.matmul(MmatT,np.matmul(g3cart,Mmat))

    return g3sph[0][0],g3sph[0][1],g3sph[0][2],g3sph[1][1],g3sph[1][2],g3sph[2][2]

