from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph

def calcEmissivity(r_here,th_here,ph_here,ii,jj,kk,data_clump,g4KSsph, g4KSsphUU):

    alphasq = -1.0/g4KSsphUU[0][0]
    alpha = np.sqrt(alphasq)
    betar   = alphasq*g4KSsphUU[0][1]
    betath  = alphasq*g4KSsphUU[0][2]
    betaph  = alphasq*g4KSsphUU[0][3]

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_r  = g4KSsph[1][1]*Br + g4KSsph[1][2]*Bth + g4KSsph[1][3]*Bph
    B_th = g4KSsph[2][1]*Br + g4KSsph[2][2]*Bth + g4KSsph[2][3]*Bph
    B_ph = g4KSsph[3][1]*Br + g4KSsph[3][2]*Bth + g4KSsph[3][3]*Bph

    vHBr  = (vr  + betar)/alpha
    vHBth = (vth + betath)/alpha
    vHBph = (vph + betaph)/alpha

    gij_vHBi_vHBj = g4KSsph[1][1]*vHBr*vHBr + 2.0*g4KSsph[1][2]*vHBr*vHBth + 2.0*g4KSsph[1][3]*vHBr*vHBph + g4KSsph[2][2]*vHBth*vHBth + 2.0*g4KSsph[2][3]*vHBth*vHBph + g4KSsph[3][3]*vHBph*vHBph

    Bsq = B_r*Br + B_th*Bth + B_ph*Bph
    B_dot_v = B_r*vHBr + B_th*vHBth + B_ph*vHBph

    detg4KSsph = linalg.det(g4KSsph)
#    if (abs(detg4KSsph) < 1.0e-8):
#        print 'at theta = ', th_here,', and phi = ', ph_here, ', determinant is too small for inversion'

    measure = np.sqrt(-detg4KSsph)*dr*dth*dph
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', Lorentz limit exceeded; ignoring'
        jval = 0.0

    else:

        wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

        u0 = wlor/alpha

        ur  = u0*vr
        uth = u0*vth
        uph = u0*vph

        #smallb2 = Bsq/wlor**2 + B_dot_v**2

        b0  = wlor*B_dot_v/alpha
        br  = Br/wlor  + B_dot_v*ur
        bth = Bth/wlor + B_dot_v*uth
        bph = Bph/wlor + B_dot_v*uph

        b_0  = g4KSsph[0][0]*b0 + g4KSsph[0][1]*br + g4KSsph[0][2]*bth + g4KSsph[0][3]*bph
        b_r  = g4KSsph[1][0]*b0 + g4KSsph[1][1]*br + g4KSsph[1][2]*bth + g4KSsph[1][3]*bph
        b_th = g4KSsph[2][0]*b0 + g4KSsph[2][1]*br + g4KSsph[2][2]*bth + g4KSsph[2][3]*bph
        b_ph = g4KSsph[3][0]*b0 + g4KSsph[3][1]*br + g4KSsph[3][2]*bth + g4KSsph[3][3]*bph

        smallb2 = b_0*b0 + b_r*br + b_th*bth + b_ph*bph

        bmag = np.sqrt(smallb2)
        C = 0.001 #0.2

        jval = (rho**3/P**2)*np.exp(-C*(rho**2/bmag/P**2)**(0.333333333333333))

    return jval, measure

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

# hard-coded mass & spin
M_BH=1
chi_BH=0.9375

chi_temp = 1.0+np.sqrt(1.0-chi_BH**2)
#rh_sq = 2.0*M**2*( chi**2*chi_temp + (4.0*chi_temp - chi**2*(2.0+chi_temp))*costh**2 )/( chi**2 + 2.0*(2.0-chi**2)*costh**2 + chi**2*costh**4 )
#rh = np.sqrt(rh_sq)
rh = M_BH*chi_temp

datafile=sys.argv[1]

outfileroot=sys.argv[2]


start_time = float(sys.argv[3])
print "starting at time ", start_time

final_time = float(sys.argv[4])
print "finishing at time ", final_time

with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz
    ### data col 8 = u0IGM

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    Nr = len(rs)
    Nth = len(ths)
    Nph = len(phs)

    dth = (ths[Nth-1]-ths[0])/(Nth-1) # could be wrong --- assumes even spacing
    dph = (phs[Nph-1]-phs[0])/(Nph-1)

    ii_r1 = 0
    while (rs[ii_r1] < rh):
        ii_r1 = ii_r1+1

    r1 = rs[ii_r1]

    print 'horizon radius = ', rh,'; next grid radius = ',r1 

    th_low = np.pi/3.0
    th_high = 2.0*np.pi/3.0

    jj_low = 0
    while( ths[jj_low] < th_low):
        jj_low = jj_low+1

    jj_high = Nth-1
    while( ths[jj_high] > th_high):
        jj_high = jj_high-1

    g4KSsph = [[[[] for kk in range(Nph)] for jj in range(Nth)] for ii in range(Nr)]

    g4KSsphUU = [[[[] for kk in range(Nph)] for jj in range(Nth)] for ii in range(Nr)]

    for ii in range(Nr):
        r_here = rs[ii]

        for jj in range(Nth):
            th_here = ths[jj]

            for kk in range(Nph):
                ph_here = phs[kk]

                g4KSsph[ii][jj][kk], g4KSsphUU[ii][jj][kk] = compute_KS_metric_sph(r_here,th_here,ph_here,M_BH,chi_BH)

    bytechunk=f.read(8)

    Nvar = 22 #was 8, before we added u0IGM

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    f1out = open(outfileroot+".txt",'w',0)

    header_line = "#r = "+str(r1)+"\n"
    f1out.write(header_line)
    header_line = "#time\tEll\n"
    f1out.write(header_line)

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        if( (this_time < start_time) or (this_time > final_time)):
            print "too early --- skipping to next time step"
            f.seek(file_place+full_timeslice_size)
            #now want to skip processing this time step ...
        else:
            print "NOT too early --- using this time step"

            bytechunk=f.read(full_timeslice_size)
            buffer_res = np.frombuffer(bytechunk)
            print 'size of buffer = ',buffer_res.size

            # rather than push this into a huge data array, do analysis on the data here
            this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

            Ell = 0.0

            for jj in range(jj_low,jj_high+1):
                th_here = ths[jj]

                for kk in range(Nph):
                    ph_here = phs[kk]

                    for ii in range(ii_r1,Nr):
                        r_here = rs[ii]

                        jval, measure = calcEmissivity(r_here,th_here,ph_here,ii,jj,kk,this_data,g4KSsph[ii][jj][kk], g4KSsphUU[ii][jj][kk])

                        Ell = Ell + jval*measure

            data_vec = [str(this_time),str(Ell)]
            data_line = "\t".join(data_vec)
            f1out.write(data_line)
            f1out.write('\n')

        # END OF IF (this_time < recover_time)
        bytechunk=f.read(8)

f1out.close()

#exit()


