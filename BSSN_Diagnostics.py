import time

# xyzCart,dxdr used for ADM integrands.
from reference_metric import xCart,yCart,zCart, dxdr

# Depends on BSSN_RHS quantities being computed
from BSSN_RHSs import *

# def func_BSSN_gf_define_alias(gfname):
#     # FIXME: compatibility mode:
#     return gfname.replace("D", "").replace("U", "").replace("2", "3").replace("1", "2").replace("0", "1").upper().replace("LAMBDA","LAMB").replace("RBAR","AUXR").replace("DETG","AUXDETG")

# Set precision to, e.g., double or long double
PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

startdiagtimer = time.time()
print("Outputting BSSN Diagnostic quantities...")

#*******************************
# Output diagnostic quantities!
#*******************************

# -={ Shift vector \beta^i: output to NRPy_codegen/shift.h }=-
NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/shift.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 [],[betaU[0],"const REAL beta1",betaU[1],"const REAL beta2",betaU[2],"const REAL beta3"])


# -={ Hamiltonian constraint: output to codegen_output/NRPy_Hamiltonian*.h }=-
traceRbar                 = sympify(0)
Dbar2phicontraction       = sympify(0)
DbarphiDbarphicontraction = sympify(0)
AbarAbar                  = sympify(0)
for i in range(3):
    for j in range(3):
        traceRbar += gammabarUU[i][j] * RbarDD[i][j]
        Dbar2phicontraction += gammabarUU[i][j] * Dbar2phiDD[i][j]
        DbarphiDbarphicontraction += gammabarUU[i][j] * DbarphiD[i] * DbarphiD[j]
        AbarAbar += AbarDD[i][j] * AbarUU[i][j]

# Eq. 13 in https://arxiv.org/pdf/1211.6632.pdf
# -={ Hamiltonian constraint: output to codegen_output/NRPy_Hamiltonian*.h }=-
traceRbar                 = sympify(0)
Dbar2phicontraction       = sympify(0)
DbarphiDbarphicontraction = sympify(0)
AbarAbar                  = sympify(0)
for i in range(3):
    for j in range(3):
        traceRbar += gammabarUU[i][j] * RbarDD[i][j]
        Dbar2phicontraction += gammabarUU[i][j] * Dbar2phiDD[i][j]
        DbarphiDbarphicontraction += gammabarUU[i][j] * DbarphiD[i] * DbarphiD[j]
        AbarAbar += AbarDD[i][j] * AbarUU[i][j]

Hamiltonian = Rational(2,3)*trK**2 + psim4*(traceRbar - 8*DbarphiDbarphicontraction - 8*Dbar2phicontraction) - AbarAbar
NormalizedHamiltonian = Hamiltonian / sqrt((Rational(2,3)*trK**2)**2 + psim4**2*(traceRbar - 8*DbarphiDbarphicontraction - 8*Dbar2phicontraction)**2 + AbarAbar**2)

NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/Hamiltonian_Constraint.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 def_tmpgammabarsall,[Hamiltonian,"gfs_aux[IDX4pt(HAM,idx)]",NormalizedHamiltonian, "gfs_aux[IDX4pt(NORMHAM,idx)]"])

# -={ Momentum constraint }=-
# Hatted covariant derivative of gammabarUU
DhatgammabarUUdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]

# Use identity 0 = \hat{D}_{k} \delta_{i}^{j} = \hat{D}_{k} (g_{i l} g^{l j})
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for m in range(3):
                    DhatgammabarUUdD[i][j][k] += -gammabarUU[i][l] * gammabarUU[j][m] * DhatgammabarDDdD[l][m][k]

# Hatted covariant derivative of AbarDD
DhatAbarDDdD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]

for i in range(3):
    for j in range(3):
        for k in range(3):
            DhatAbarDDdD[i][j][k] = AbarDDdD[i][j][k]
            for l in range(3):
                DhatAbarDDdD[i][j][k] += -(GammahatUDD[l][i][k] * AbarDD[l][j] + GammahatUDD[l][j][k] * AbarDD[i][l])

# Covariant divergence
DhatDivAbarU = [sympify(0) for i in range(3)]

for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                DhatDivAbarU[i] += (DhatgammabarUUdD[i][k][j] * gammabarUU[j][l] + gammabarUU[i][k] * DhatgammabarUUdD[j][l][j]) * AbarDD[k][l] + gammabarUU[i][k] * gammabarUU[j][l] * DhatAbarDDdD[k][l][j]

# Momentum constraint vector
MomentumConstraintU = [sympify(0) for i in range(3)]
# Eq. 14 in https://arxiv.org/pdf/1211.6632.pdf
for i in range(3):
    MomentumConstraintU[i] = DhatDivAbarU[i] / ReU[i] # term 1
    for j in range(3):
        MomentumConstraintU[i] += 6 * AbarUU[i][j] * phidD[j] * ReDD[i][j] * ReU[j] # term 2
        MomentumConstraintU[i] += -Rational(2,3) * gammabarUU[i][j] * trKdD[j] * ReDD[i][j] * ReU[j] # term 3
        for k in range(3):
            # The second term here is missing from the expression in the reference
            MomentumConstraintU[i] += (AbarUU[j][k] * DGammaUDD[i][j][k] + AbarUU[i][k] * DGammaUDD[j][j][k]) / ReU[i] # term 4
    MomentumConstraintU[i] *= psim4 # multiplying all terms

NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/Momentum_Constraint.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,def_tmpgammabarsall,
                 [MomentumConstraintU[0],"gfs_aux[IDX4pt(MOM1,idx)]",
                  MomentumConstraintU[1],"gfs_aux[IDX4pt(MOM2,idx)]",
                  MomentumConstraintU[2],"gfs_aux[IDX4pt(MOM3,idx)]"])

# -={ Connection Coefficient constraint: output to codegen_output/Connection_Coefficient*.h }=-
ConnectionConstraint = [sympify(0) for i in range(3)]
for i in range(3):
    ConnectionConstraint[i] = LambarU[i] - DGammaU[i]

NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/Connection_Constraint.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 def_tmpgammabarsall,[ConnectionConstraint[0],"gfs_aux[IDX4pt(CC1,idx)]",
                                      ConnectionConstraint[1],"gfs_aux[IDX4pt(CC2,idx)]",
                                      ConnectionConstraint[2],"gfs_aux[IDX4pt(CC3,idx)]"])

# -={ tr(Abar_{ij}), used for removing the trace of Abar: output to codegen_output/NRPy_traceA*.h }=-
Trace_Ao3 = sympify(0)
for i in range(3):
    for j in range(3):
        Trace_Ao3 += (gammabarUU[i][j] * AbarDD[i][j])/3
NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/TraceAo3.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,def_tmpgammabarsall,
                 [Trace_Ao3,"const "+PRECISION+" Trace_Ao3"])

# -={ Spherical symmetry null expansion: output to codegen_output/NRPy_NullExpansion*.h }=-
NullExpansion = (4*gammabarDD[1][1]*phidD[0] + gammabarDDdD[1][1][0])/(gammabarDD[1][1]*sqrt(gammabarDD[0][0]/psim4)) - 2*(AbarDD[1][1] + Rational(1,3)*gammabarDD[1][1]*trK)/gammabarDD[1][1]
NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/NullExpansion.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 [tmpgammabarDD[0][0],"tmpgammabarDD00",tmpgammabarDD[1][1],"tmpgammabarDD11"],
                 [NullExpansion,"gfs_aux[IDX4pt(NULLEXP,idx)]"])

# -={ detg = detgammabar/detgammahat: output to NRPy_codegen/detg.h }=-
detg_out = gammabarDET/detgammahat
NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/detg.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 def_tmpgammabarsDD,[detg_out,"gfs_aux[IDX4pt(AUXDETG,idx)]"])

##########################################
# ADM integrands
##########################################

# Conformal factor
psi = psim4**(-Rational(1, 4))

# Cartesian outward normal on a sphere
r = (sqrt(xCart**2 + yCart**2 + zCart**2))
sinth = (sqrt(1 - (zCart / r)**2))
dSU = [xCart / r, yCart / r, zCart / r]

# ADM mass; Eq. A.10 in Alcubierre (2008)
DhathADMDDdD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            DhathADMDDdD[i][j][k] = psi**4 * (4 * (ghatDD[i][j] + epsDD[i][j]) * phidD[k] + DhatgammabarDDdD[i][j][k])

integrand_M = sympify(0)
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                integrand_M += (DhathADMDDdD[i][k][j] - DhathADMDDdD[i][j][k]) * ghatUU[i][j] * dxdr[k][l] * dSU[l] / (16 * pi) * r**2 * sinth

# ADM linear momentum; Eq. 3.195 in Baumgarte & Shapiro (2010)
integrand_P = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                integrand_P[i] += dxdr[k][i] * dxdr[j][l] * psi**4 * (AbarDD[k][j] - Rational(2, 3) * gammabarDD[k][j] * trK) * dSU[l] / (8 * pi) * r**2 * sinth

# Cartesian position vector
yy = [xCart, yCart, zCart]

# ADM angular momentum; Eq. 3.191 in Baumgarte & Shapiro (2010)
integrand_J = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
                integrand_J[i] += LeviCivita(i, j, k) * yy[j] * integrand_P[k]

NRPy_file_output(OUTDIR+"/diagnostics/NRPy_codegen/ADM_integrand.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                 BSSN_upwind_vector, BSSN_protected_variables,
                 def_tmpgammabarsall,[integrand_M, "const REAL integrand_M",
                                      integrand_P[0], "const REAL integrand_P1", integrand_P[1], "const REAL integrand_P2", integrand_P[2], "const REAL integrand_P3",
                                      integrand_J[0], "const REAL integrand_J1", integrand_J[1], "const REAL integrand_J2", integrand_J[2], "const REAL integrand_J3"])
stopdiagtimer = time.time()
print("Output BSSN diagnostics to file in \t\t\t" + str(round(stopdiagtimer-startdiagtimer,2)) + " seconds")

