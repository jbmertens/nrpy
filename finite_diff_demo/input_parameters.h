#ifndef INPUT_PARAMETERS_H__
#define INPUT_PARAMETERS_H__

/* Total number of iterations N_iters = FRAME_JUMP * N_FRAMES */
#define FRAME_JUMP 500 /* Output one frame to data files when iteration# is a multiple of FRAME_JUMP */
#define N_FRAMES   32 /* Output this many frames (plus the initial condition) */

/* Checkpointing */
const int CHECKPOINT_ID = 0;    // Set 1 to save checkpoint after setting up initial data. Useful if initial data take a long time to generate.
const int READ_CHECKPOINT = 0;    // Set 1 to read checkpoint
const int CHECKPOINT_EVERY = -1; // Set negative to turn off checkpoint writing

/* WARNING: DO NOT CHANGE THIS ORDERING! */
const char *gf_name[NUM_EVOL_GFS] = {"f"};

/* Outer boundary conditions */
//#define OUTER_BOUNDARY_EXACT
//#define OUTER_BOUNDARY_LIN_EXTRAP
#define OUTER_BOUNDARY_QUAD_EXTRAP
//#define OUTER_BOUNDARY_SOMMERFELD

/* CFL factor for RK4 timestep */
const REAL CFL = 0.5;

/* Time step (in physical units) */
REAL dt;

#endif // INPUT_PARAMETERS_H__
