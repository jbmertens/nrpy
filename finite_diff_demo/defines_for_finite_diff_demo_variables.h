#ifndef DEFINES_FOR_SCALARWAVE_VARIABLES_H__
#define DEFINES_FOR_SCALARWAVE_VARIABLES_H__

/* DON'T TOUCH THIS! Only DIM=3 supported at the moment. TODO: Support DIM!=3 as well. */
#define DIM 3

#include "parameters_struct_def-NRPyGEN.h"

#include "gridfunction_defines-NRPyGEN.h"
// Re-use SCALARWAVE gridfunctions to store temporary gridfunctions needed for analysis.
#define UUEXACTDIFF 0 // Difference between UU and exact.
#define VVEXACTDIFF 1 // Difference between VV and exact.
#define VOLUME      2 // Volume, for debugging integration interface.
#define NUM_AUX_GFS 3

#endif /* DEFINES_FOR_SCALARWAVE_VARIABLES_H__ */
