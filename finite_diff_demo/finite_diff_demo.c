#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include "loop_setup.h"
#include "defines_for_finite_diff_demo_variables.h"
#include "input_parameters.h"

int main(int argc, const char *argv[]) {
  paramstruct params;

  // Error out if command-line arguments are not set. Otherwise set these "runtime parameters"
#include "runtime_params_set-NRPyGEN.h"

  // Set basic simulation parameters, including setting floating point precision (double or long double), coord system, ID type, etc.
#include "Input_CompileTime_params-NRPyGEN.h"

  // Display simulation info
  printf("\x1B[32mFD order: %d\x1B[0m\n", params.FDCENTERDERIVS_FDORDER);

#ifdef USE_LONG_DOUBLE
  if(strncmp(params.PRECISION,"long double",100)!=0) {
    printf("ERROR: You set USE_LONG_DOUBLE in loop_setup.h, but forgot to tell NRPy!\n");
    exit(1);
  }
#else
  printf("ERROR: finite_diff_demo uses long double precision. It looks like you defined USE_LONG_DOUBLE in loop_setup.h, but forgot to set long double in NRPy_main.py!\n");
  exit(1);
#endif

  const int FDCENTERDERIVS_FDORDER = params.FDCENTERDERIVS_FDORDER;

  params.Nx1 = atoi(argv[1]); params.Nx2 = atoi(argv[2]); params.Nx3 = atoi(argv[3]);
  params.NGHOSTS = ((params.FDCENTERDERIVS_FDORDER) / 2 + 1);

  if(params.UPWIND == 0 || params.UPWIND == -1) {
    params.NGHOSTS = ((params.FDCENTERDERIVS_FDORDER) / 2);
  }

  const int Nx1 = params.Nx1;
  const int Nx2 = params.Nx2;
  const int Nx3 = params.Nx3;
  params.del[0] = (params.x1max - params.x1min)/((REAL)Nx1);
  params.del[1] = (params.x2max - params.x2min)/((REAL)Nx2);
  params.del[2] = (params.x3max - params.x3min)/((REAL)Nx3);

  const int NGHOSTS = params.NGHOSTS;
  params.Npts1 = (Nx1 + 2 * NGHOSTS);
  params.Npts2 = (Nx2 + 2 * NGHOSTS);
  params.Npts3 = (Nx3 + 2 * NGHOSTS);

  const int Npts1 = params.Npts1;
  const int Npts2 = params.Npts2;
  const int Npts3 = params.Npts3;

  // Total number of grid points, including ghost zones
  const int Ntot = Npts1 * Npts2 * Npts3;
  
  const double del[3] = {params.del[0],params.del[1],params.del[2]};
  const double x123min[3] = {params.x1min,params.x2min,params.x3min};

  // Uniform coordinates
  REAL *x1G = (REAL *)malloc(sizeof(REAL)*Npts1);
  REAL *x2G = (REAL *)malloc(sizeof(REAL)*Npts2);
  REAL *x3G = (REAL *)malloc(sizeof(REAL)*Npts3);

  // Initialize static (uniform) cell-centered coordinates on the chosen coordinate domain
  for(int ii = 0; ii < Npts1; ii++) x1G[ii] = x123min[0] + del[0] * ((REAL)(ii - NGHOSTS) + 0.5);
  for(int jj = 0; jj < Npts2; jj++) x2G[jj] = x123min[1] + del[1] * ((REAL)(jj - NGHOSTS) + 0.5);
  for(int kk = 0; kk < Npts3; kk++) x3G[kk] = x123min[2] + del[2] * ((REAL)(kk - NGHOSTS) + 0.5);

  // Static coordinate domains set by the grid resolution
  const REAL x_min[3] = {x1G[NGHOSTS], x2G[NGHOSTS], x3G[NGHOSTS]};
  const REAL x_max[3] = {x1G[Npts1 - NGHOSTS - 1], x2G[Npts2 - NGHOSTS - 1], x3G[Npts3 - NGHOSTS - 1]};

  // Allocate grid function storage

  // Test field
  REAL *in_gfs = (REAL *)malloc(sizeof(REAL)*NUM_EVOL_GFS*Ntot);
  REAL *yy     = (REAL *)malloc(sizeof(REAL)*DIM*Ntot);

  // Store yy coordinates:
  LOOP_GZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];

    {
#include "reference_metric/NRPy_codegen/yy.h" // Located in ../common_functions/
    }
  }

  // Display grid info
  //for(int dirn = 0; dirn < DIM; dirn++) printf("x%dmin = %f, x%dmax = %f, Dx%d = %.2e\n", dirn, (double)x_min[dirn], dirn, (double)x_max[dirn], dirn, (double)del[dirn]);

  #define ANALYTIC_FUNCTION sinl((long double)yy[IDX4pt(0,idx)]+3.1415926535L/6.0L)
  #define ANALYTIC_FUNCTIOND1 cosl((long double)yy[IDX4pt(0,idx)]+3.1415926535L/6.0L)

  //#define ANALYTIC_FUNCTION sinl((long double)yy[IDX4pt(0,idx)]+3.1415926535L/4.0L)
  //#define ANALYTIC_FUNCTIOND1 cosl((long double)yy[IDX4pt(0,idx)]+3.1415926535L/4.0L)

  //#define ANALYTIC_FUNCTION logl((long double)yy[IDX4pt(0,idx)]+1.0L)
  //#define ANALYTIC_FUNCTIOND1 1.0L/((long double)yy[IDX4pt(0,idx)]+1.0L)

  // The dreaded RUNGE FUNCTION
  //#define ANALYTIC_FUNCTION 1.0L/((long double)yy[IDX4pt(0,idx)]*yy[IDX4pt(0,idx)]+25.0L)
  //#define ANALYTIC_FUNCTIOND1 -2.0L*yy[IDX4pt(0,idx)]*ANALYTIC_FUNCTION*ANALYTIC_FUNCTION

  //#define ANALYTIC_FUNCTION 1.0L/((long double)yy[IDX4pt(0,idx)]+4.0L)
  //#define ANALYTIC_FUNCTIOND1 -ANALYTIC_FUNCTION*ANALYTIC_FUNCTION

  //#define ANALYTIC_FUNCTION expl(-yy[IDX4pt(0,idx)])
  //#define ANALYTIC_FUNCTIOND1 -expl(-yy[IDX4pt(0,idx)])

  //#define ANALYTIC_FUNCTION expl(yy[IDX4pt(0,idx)])
  //#define ANALYTIC_FUNCTIOND1 ANALYTIC_FUNCTION

  //#define ANALYTIC_FUNCTION powl((long double)yy[IDX4pt(0,idx)]-10.1L,-2.0L)
  //#define ANALYTIC_FUNCTIOND1 -2.0*powl((long double)yy[IDX4pt(0,idx)]-10.1L,-3.0L)

  //#define ANALYTIC_FUNCTION powl((long double)yy[IDX4pt(0,idx)]-10.1L,-12.0L)
  //#define ANALYTIC_FUNCTIOND1 -12.0L*powl((long double)yy[IDX4pt(0,idx)]-10.1L,-13.0L)

  // Set the gridfunction f:
  LOOP_GZFILL(ii, jj, kk) {
    const int idx = IDX3(ii,jj,kk);
    in_gfs[IDX4pt(F,idx)] = (REAL)ANALYTIC_FUNCTION;
  }

  {
    const int ii=NGHOSTS;
    const int jj=NGHOSTS;
    const int kk=NGHOSTS;
    const int idx = IDX3(ii,jj,kk);

    REAL first_deriv[3];
    REAL second_deriv[3][3];
#include "read_FD_dxs.h" // Located in ../../common_functions/
#include "NRPy_codegen/NRPy_fdiff_demo.h"

    REAL NUM = first_deriv[0];
    REAL ANA = (REAL)ANALYTIC_FUNCTIOND1;
    REAL rel_error = (REAL)(fabs((double)(NUM - ANA)) / ( 0.5 * (fabs((double)NUM) + fabs((double)ANA)) ));
    printf("%.15e %.15e\n",log10((double)(x1G[1] - x1G[0])),log10((double)rel_error));
    //printf("%.15e %.15e %e %e\n",log10((double)(x1G[1] - x1G[0])),log10((double)rel_error),NUM,ANA);
  }

  // Free allocated memory
  free(in_gfs);
  
  free(yy);
  free(x1G);
  free(x2G);
  free(x3G);

  return 0;
}
