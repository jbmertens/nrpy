#include <stdio.h>

/* Written by Ian Ruchlin */

int main()
{
  // Fill with zeros
  int g[3][3][3][3] = {{{{0}}}};

  // 36 independent components
  g[0][0][0][0] = 1;
  g[0][0][0][1] = 2;
  g[0][0][0][2] = 3;
  g[0][0][1][1] = 4;
  g[0][0][1][2] = 5;
  g[0][0][2][2] = 6;
  g[0][1][0][0] = 7;
  g[0][1][0][1] = 8;
  g[0][1][0][2] = 9;
  g[0][1][1][1] = 10;
  g[0][1][1][2] = 11;
  g[0][1][2][2] = 12;
  g[0][2][0][0] = 13;
  g[0][2][0][1] = 14;
  g[0][2][0][2] = 15;
  g[0][2][1][1] = 16;
  g[0][2][1][2] = 17;
  g[0][2][2][2] = 18;
  g[1][1][0][0] = 19;
  g[1][1][0][1] = 20;
  g[1][1][0][2] = 21;
  g[1][1][1][1] = 22;
  g[1][1][1][2] = 23;
  g[1][1][2][2] = 24;
  g[1][2][0][0] = 25;
  g[1][2][0][1] = 26;
  g[1][2][0][2] = 27;
  g[1][2][1][1] = 28;
  g[1][2][1][2] = 29;
  g[1][2][2][2] = 30;
  g[2][2][0][0] = 31;
  g[2][2][0][1] = 32;
  g[2][2][0][2] = 33;
  g[2][2][1][1] = 34;
  g[2][2][1][2] = 35;
  g[2][2][2][2] = 36;

  // Print un-symmetrized matrix
  for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
	{
	  for(int k = 0; k < 3; k++)
	    {
	      for(int l = 0; l < 3; l++)
		{
		  printf("g[%d][%d][%d][%d] = %d\n", i, j, k, l, g[i][j][k][l]);
		}
	    }
	}
    }

  printf("\n");

  // Count the number of symmetry operations
  int count = 0;

  for(int i = 0; i < 3; i++)
    {
      for(int j = i; j < 3; j++)
	{
	  for(int k = 0; k < 3; k++)
	    {
	      for(int l = k + 1; l < 3; l++)
		{
		  g[i][j][l][k] = g[i][j][k][l];
		  count++;
		}
	    }
	}
    }

  for(int i = 0; i < 3; i++)
    {
      for(int j = i + 1; j < 3; j++)
	{
	  for(int k = 0; k < 3; k++)
	    {
	      for(int l = 0; l < 3; l++)
		{
		  g[j][i][l][k] = g[i][j][k][l];
		  count++;
		}
	    }
	}
    }

  // Print symmetrized tensor
  for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
	{
	  for(int k = 0; k < 3; k++)
	    {
	      for(int l = 0; l < 3; l++)
		{
		  printf("g[%d][%d][%d][%d] = %d\n", i, j, k, l, g[i][j][k][l]);
		}
	    }
	}
    }

  // How many operations did we perform?
  printf("count = %d\n", count);

  return 0;
}
