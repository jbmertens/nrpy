set term post enh color fontscale 2.0

set xlabel "x/b"
set ylabel "{/Symbol r}/b"

unset key

set size ratio 0.5
set out "modtwopuncts_grids.eps"
p [-2:2] [0:2] "coord_output.txt" u 3:4 w l

set out "twopuncts_grids.eps"
p [-2:2] [0:2] "coord_output.txt" u 5:6 w l
