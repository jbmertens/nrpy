#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
//#include <gsl/gsl_errno.h>
//#include <gsl/gsl_spline.h>

#define MIN(a,b) ( ((a) < (b)) ? (a) : (b) )

int NG;
int NR;
double drmin;
double dr_gf;

// r(t, R)
static inline double r_of_t_R(const double t, const double R)
{
  const double r = (0.5 * (dr_gf + 1.0) * exp((R * NR - 0.5) * log(dr_gf)) - 1.0) / (dr_gf - 1.0) * drmin;
  return r;
}

// \dot{r} = d_{t} r
static inline double r_dot(const double t, const double R)
{
  return 0;
}

// r' = d_{R} r
static inline double r_prime(const double t, const double R)
{
  const double rp = 0.5 * (dr_gf + 1.0) * exp((R * NR - 0.5) * log(dr_gf)) * log(dr_gf) * NR / (dr_gf - 1.0) * drmin;
  return rp;
}

// \ddot{r} = d_{t} d_{t} r
static inline double r_ddot(const double t, const double R)
{
  return 0;
}

// \dot{r}' = d_{R} d_{t} r
static inline double r_dot_prime(const double t, const double R)
{
  return 0;
}

// r'' = d_{R} d_{R} r
static inline double r_pprime(const double t, const double R)
{
  const double rpp = 0.5 * (dr_gf + 1.0) * exp((R * NR - 0.5) * log(dr_gf)) * log(dr_gf) * log(dr_gf) * NR * NR / (dr_gf - 1.0) * drmin;
  return rpp;
}

// Exact solution u = soln[0], and time derivative v = soln[1]
void Exact_soln(const double t, const double R, double soln[2])
{
  double r = r_of_t_R(t, R);

  // Spherical traveling Gaussian packet
  /*double rmt = r - t;
  double rpt = r + t;

  soln[0] = 0.5 / r * (rmt * exp(-rmt * rmt) + rpt * exp(-rpt * rpt)) + 1;
  soln[1] = 0.5 / r * (exp(-rmt * rmt) * (2 * rmt * rmt - 1) - exp(-rpt * rpt) * (2 * rpt * rpt - 1));*/

  // Spherical Bessel function eigensolution of the wave equation
  double j0 = sin(r) / r;

  soln[0] =  cos(t) * j0 + 1.0;
  soln[1] = -sin(t) * j0;

  return;
}

// Apply boundary conditions
void Apply_out_sym_bcs(const int Npts, const int apply_to_v, const double t, double *RR, double *func)
{
  // Loop over ghost zones
  for(int which_bdry_pt = 0; which_bdry_pt < NG; which_bdry_pt++)
    {
      // Exact outer boundary conditions
      int imax = Npts - NG + which_bdry_pt;
      double soln[2];
      Exact_soln(t, RR[imax], soln);
      func[imax] = soln[apply_to_v]; // u = soln[0], v = soln[1]

      // Then apply symmetry BC
      // First apply r => -r BC, near r = 0:
      // ASSUME r grid is staggered (to avoid r = 0 singularity):
      func[which_bdry_pt] = func[2 * NG - 1 - which_bdry_pt]; 
    }

  return;
}

double L_2(const int Npts, const double t, const double R, const double dR, const int i, double *u)
{
  const int ip4 = i + 4;
  const int ip3 = i + 3;
  const int ip2 = i + 2;
  const int ip1 = i + 1;
  const int im1 = i - 1;
  const int im2 = i - 2;
  const int im3 = i - 3;
  const int im4 = i - 4;

  // Uniform (static) finite difference
  double u_R; // d_{R} u
  double u_RR; // d_{R} d_{R} u

  if(NG == 1)
    {
      u_R = 0.5 * (u[ip1] - u[im1]) / dR; // Second-order first derivative
      u_RR = (u[ip1] - 2.0 * u[i] + u[im1]) / (dR * dR); // Second-order second derivative
    }
  else if(NG == 2)
    {
      u_R = (1.0 / 12.0 * (u[im2] - u[ip2]) - 2.0 / 3.0 * (u[im1] - u[ip1])) / dR; // Fourth-order first derivative
      u_RR = (-1.0 / 12.0 * (u[im2] + u[ip2]) + 4.0 / 3.0 * (u[im1] + u[ip1]) - 2.5 * u[i]) / (dR * dR); // Fourth-order second derivative
    }
  else if(NG == 3)
    {
      u_R = ((1.0 / 60.0) * (u[ip3] - u[im3]) + (-3.0 / 20.0) * (u[ip2] - u[im2]) + 0.75 * (u[ip1] - u[im1])) / dR; // Sixth-order first derivative
      u_RR = ((1.0 / 90.0) * (u[ip3] + u[im3]) + (-3.0 / 20.0) * (u[ip2] + u[im2]) + 1.5 * (u[ip1] + u[im1]) - 49.0 / 18.0 * u[i]) / (dR * dR); // Sixth-order second derivative
    }
  else if(NG == 4)
    {
      u_R = ((-1.0 / 280.0) * (u[ip4] - u[im4]) + (4.0 / 105.0) * (u[ip3] - u[im3]) + (-1.0 / 5.0) * (u[ip2] - u[im2]) + (4.0 / 5.0) * (u[ip1] - u[im1])) / dR; // Eighth-order first derivative
      u_RR = ((-1.0 / 560.0) * (u[ip4] + u[im4]) + (8.0 / 315.0) * (u[ip3] + u[im3]) + (-1.0 / 5.0) * (u[ip2] + u[im2]) + (8.0 / 5.0) * (u[ip1] + u[im1]) - 205.0 / 72.0 * u[i]) / (dR * dR); // Eighth-order second derivative
    }
  else
    {
      printf("Invalid choice of NG\n");
      exit(1);
    }
  
  const double r = r_of_t_R(t, R);      // r(t, R)
  const double rd = r_dot(t, R);        // d_{t} r
  const double rp = r_prime(t, R);      // d_{R} r
  const double rdd = r_ddot(t, R);      // d_{t} d_{t} r
  const double rdp = r_dot_prime(t, R); // d_{t} d_{R} r
  const double rpp = r_pprime(t, R);    // d_{R} d_{R} r

  // Inverse metric
  const double gRR = (1 - rd * rd) / (rp * rp); // g^{R R}

  // Contracted Christoffel symbols
  const double GamR = (r * rpp * (1 - rd * rd) + 2 * r * rd * rp * rdp - rp * rp * (2 + r * rdd)) / (r * rp * rp * rp); // \Gamma^{R}

  return gRR * u_RR - GamR * u_R;
}

double L_3(const int Npts, const double t, const double R, const double dR, const int i, double *u, double *v)
{
  const int ip4 = i + 4;
  const int ip3 = i + 3;
  const int ip2 = i + 2;
  const int ip1 = i + 1;
  const int im1 = i - 1;
  const int im2 = i - 2;
  const int im3 = i - 3;
  const int im4 = i - 4;

  // Uniform (static) finite difference
  double v_R; // d_{R} v

  if(NG == 1)
    {
      v_R = 0.5 * (v[ip1] - v[im1]) / dR; // Second-order first derivative
    }
  else if(NG == 2)
    {
      v_R = (1.0 / 12.0 * (v[im2] - v[ip2]) - 2.0 / 3.0 * (v[im1] - v[ip1])) / dR; // Fourth-order first derivative
    }
  else if(NG == 3)
    {
      v_R = ((1.0 / 60.0) * (v[ip3] - v[im3]) + (-3.0 / 20.0) * (v[ip2] - v[im2]) + 0.75 * (v[ip1] - v[im1])) / dR; // Sixth-order first derivative
    }
  else if(NG == 4)
    {
      v_R = ((-1.0 / 280.0) * (v[ip4] - v[im4]) + (4.0 / 105.0) * (v[ip3] - v[im3]) + (-1.0 / 5.0) * (v[ip2] - v[im2]) + (4.0 / 5.0) * (v[ip1] - v[im1])) / dR; // Eighth-order first derivative
    }
  else
    {
      printf("Invalid choice of NG\n");
      exit(1);
    }

  const double rd = r_dot(t, R);   // d_{t} r
  const double rp = r_prime(t, R); // d_{R} r
  
  const double gtR = rd / rp;

  return 2.0 * gtR * v_R;
}

// A function relating growth factors for some NR0 and conv_factor
// Used by Newton's method
double GF_f(const double c, const double NR0, const double K, const double L)
{
  const double a = (0.5 * (K + 1.0) * pow(K, NR0 - 1.0) - 1.0) / (K - 1.0);
  return pow(L, c * NR0) + pow(L, c * NR0 - 1.0) - 2.0 * a * c * L + 2.0 * (a * c - 1.0);
}

// The derivative of a function relating growth factors for some NR0 and conv_factor
// Used by Newton's method
double GF_fp(const double c, const double NR0, const double K, const double L)
{
  const double a = (0.5 * (K + 1.0) * pow(K, NR0 - 1.0) - 1.0) / (K - 1.0);
  return c * NR0 * pow(L, c * NR0 - 1.0) + (c * NR0 - 1.0) * pow(L, c * NR0 - 2.0) - 2.0 * a * c;
}

int main(int argc, char *argv[])
{
  //const int conv_factor = atoi(argv[1]); // Read in convergence factor from command line
  //NG = atoi(argv[2]); // Read in number of ghost zones (FD order) from command line

  const int conv_factor = 1; // Resolution scaling factor, for convergence studies
  NG = 1; // Number of ghost zones. Possible choices {1, 2, 3, 4}
  const int NR0 = 32; // Number of points mapping to the physical domain
  NR = NR0 * conv_factor;
  const int Npts = NR + 2 * NG; // Total number of points, including ghost zones on either end of the domain
  const double dr_gf0 = 1.0668; // Growth factor for exponential coordinate distribution
  const double drmin0 = 0.1; // Smallest physical step size
  drmin = drmin0 / conv_factor;

  const double Newton_tol = 1.0e-8; // Set tolerance for Newton's method
  dr_gf = dr_gf0; // Set Newton's method initial guess

  // Use Newton's method to find the growth factor such that, given
  // NR0 and conv_factor, the outer boundary remains at the same radius
  while(fabs(GF_f(conv_factor, NR0, dr_gf0, dr_gf)) > Newton_tol)
    {
      const double f = GF_f(conv_factor, NR0, dr_gf0, dr_gf);
      const double fp = GF_fp(conv_factor, NR0, dr_gf0, dr_gf);
      dr_gf -= f / fp;
    }

  // Initialize storage
  double *u =    (double *)malloc(Npts * sizeof(double));
  double *v =    (double *)malloc(Npts * sizeof(double));
  double *u1 =   (double *)malloc(Npts * sizeof(double));
  double *v1 =   (double *)malloc(Npts * sizeof(double));
  double *unp1 = (double *)malloc(Npts * sizeof(double));
  double *vnp1 = (double *)malloc(Npts * sizeof(double));
  double *L2un = (double *)malloc(Npts * sizeof(double));
  double *L3un = (double *)malloc(Npts * sizeof(double));
  double *RR =   (double *)malloc(Npts * sizeof(double));

  // Poison grid functions with NaNs
  for(int i = 0; i < Npts; i++)
    {
      u[i] = 1.0 / 0.0;
      v[i] = 1.0 / 0.0;
      u1[i] = 1.0 / 0.0;
      v1[i] = 1.0 / 0.0;
      unp1[i] = 1.0 / 0.0;
      vnp1[i] = 1.0 / 0.0;
      L2un[i] = 1.0 / 0.0;
      L3un[i] = 1.0 / 0.0;
      RR[i] = 1.0 / 0.0;
    }

  const double dR = 1.0 / (double)NR; // Uniform R step size

  // Initialize static (uniform) coordinates
  for(int i = 0; i < Npts; i++)
    {
      RR[i] = dR * (i - NG + 0.5); // cell-centered in R on the unit sphere (avoids R = 0)
    }

  // Static radius 0 < R < 1 set by radial resolution
  const double Rmin = RR[NG];
  const double Rmax = RR[Npts - NG - 1];
  const double rmin = r_of_t_R(0, Rmin);
  const double rmax = r_of_t_R(0, Rmax);

  // Print domain bounds and step sizes
  printf("Rmin = %f, Rmax = %f, dR = %f\n", Rmin, Rmax, dR);
  printf("rmin = %f, rmax = %f, drmin = %f\n", rmin, rmax, drmin);

  // Set initial conditions
  for(int i = NG; i < Npts - NG; i++)
    {
      double soln[2]; // soln[i] = {u, v}
      Exact_soln(0.0, RR[i], soln);
      
      u[i] = soln[0];
      v[i] = soln[1];
    }

  // Apply boundary and symmetry conditions to the initial conditions
  Apply_out_sym_bcs(Npts, 0, 0.0, RR, u);
  Apply_out_sym_bcs(Npts, 1, 0.0, RR, v);

  // Time step, satisfying the CFL condition
  const double CFL = 0.4;
  const double dt = CFL * drmin;

  // Number of time steps
  const int frame_jump = 1 * conv_factor; // Number of time steps between output frames
  const int N_iters = 100 * frame_jump; // Total number of time steps to output
  int frame_ind = -1; // Count the output frames

  // Print time step size and number of frames, used by the plotting script
  printf("frame dt = %e, final frame index = %i\n", dt * frame_jump, N_iters / frame_jump);

  char out0D_filename[50];
  char out1D_filename[50];
  //char out1D_interp_filename[50];

  // Output filenames
  sprintf(out0D_filename, "out_0D_cf%d_FD%d.txt", conv_factor, 2 * NG);
  sprintf(out1D_filename, "out_1D_cf%d_FD%d.txt", conv_factor, 2 * NG);
  //sprintf(out1D_interp_filename, "out_1D_interp_cf%d_FD%d.txt", conv_factor, 2 * NG);

  // Open file IO
  FILE *out0D, *out1D;//, *out1Dinterp;
  out0D = fopen(out0D_filename, "w");
  out1D = fopen(out1D_filename, "w");
  //out1Dinterp = fopen(out1D_interp_filename, "w");

  // IO headers
  //fprintf(out0D, "# out_0D.txt\n# $1 frame_ind\n# $2 t\n# $3 L2err\n\n");
  //fprintf(out1D, "# out_1D.txt\n# $1 frame_ind\n# $2 t\n# $3 R\n# $4 r\n# $5 num\n# $6 ana\n# $7 rel err\n# $8 abs err\n\n");

  // Initialize GSL cubic spline interpolation
  //gsl_interp_accel *acc = gsl_interp_accel_alloc();
  //gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, Npts);
  
  // Iterate through time
  for(int n = 0; n <= N_iters; n++)
    {
      // Current time
      const double t = n * dt;

      /*
       * Baumgarte et al (2012) arXiv:1211.6632
       * Integrate u and v using PIRK, Eqs. 29 and 30.
       */
      
#pragma omp parallel for
      for(int i = NG; i < Npts - NG; i++)
	{
	  u1[i] = u[i] + dt * v[i];
	}
      
      Apply_out_sym_bcs(Npts, 0, t + dt, RR, u1);

#pragma omp parallel for
      for(int i = NG; i < Npts - NG; i++)
	{
	  L2un[i] = L_2(Npts, t, RR[i], dR, i, u);
	  L3un[i] = L_3(Npts, t, RR[i], dR, i, u, v);
	  v1[i] = v[i] + dt * (0.5 * (L2un[i] + L_2(Npts, t, RR[i], dR, i, u1)) + L3un[i]);
	}
      
      Apply_out_sym_bcs(Npts, 1, t + dt, RR, v1);
      
#pragma omp parallel for
      for(int i = NG; i < Npts - NG; i++)
	{
	  unp1[i] = 0.5 * (u[i] + u1[i] + dt * v1[i]);
	}
      
      Apply_out_sym_bcs(Npts, 0, t + dt, RR, unp1);

#pragma omp parallel for
      for(int i = NG; i < Npts - NG; i++)
	{
	  vnp1[i] = v[i] + 0.5 * dt * (L2un[i] + L_2(Npts, t, RR[i], dR, i, unp1) + L3un[i] + L_3(Npts, t, RR[i], dR, i, u1, v1));
	}

      Apply_out_sym_bcs(Npts, 1, t + dt, RR, vnp1);

      // Write data to files
      if(n % frame_jump == 0)
	{
	  frame_ind++; // frame index, zero inclusive

	  // Progress indicator printing to stdout
	  printf("%.2f%% Complete\r", 100 * (double)n / (double)N_iters); // \r is carriage return, move cursor to the beginning of the line
	  fflush(stdout); // Flush the stdout buffer

	  // Evaluate the L2 RMS absolute and relative errors over the entire domain
	  const int L2_Npts = Npts - 2 * NG;
	  double L2_abs_err = 0;
	  double L2_rel_err = 0;
	  double integrated_rel_err = 0;
	  double growth_factor = dr_gf;

#pragma omp parallel for
	  for(int i = NG; i < Npts - NG; i++)
	    {
	      const double num = u[i];
	      double soln[2];
	      Exact_soln(t, RR[i], soln);
	      const double ana = soln[0];
	      const double var = (num - ana) * (num - ana);
	      L2_abs_err += var;
	      L2_rel_err += var / (ana * ana);
	      integrated_rel_err += growth_factor * fabs((num - ana) / ana);
	      growth_factor *= growth_factor;
	    }

	  L2_abs_err = sqrt(L2_abs_err / L2_Npts);
	  L2_rel_err = sqrt(L2_rel_err / L2_Npts);
	  integrated_rel_err *= drmin;

	  fprintf(out0D, "%d %e %.16e %.16e %.16e\n", frame_ind, t, L2_rel_err, L2_abs_err, integrated_rel_err);

	  for(int i = NG; i < Npts - NG; i++)
	    {
	      const double r = r_of_t_R(t, RR[i]);
	      const double num = u[i];
	      double soln[2];
	      Exact_soln(t, RR[i], soln);
	      const double ana = soln[0];

	      fprintf(out1D, "%d %e %e %e %e %e %.16e %.16e\n", frame_ind, t, RR[i], r, num, ana, fabs((num - ana) / ana), fabs(num - ana));
	    }

	  fprintf(out1D, "\n\n");

	  // Initialize spline interpolating u on RR
	  /*gsl_spline_init(spline, RR, u, Npts);

	  for(int i = 0; i < NR; i++)
	    {
	      double Rmin1 = Rmin, Rmax1 = Rmax;
	      double rmin1 = rmin, rmax1 = rmax;
	      double B1 = (log(rmax1) - log(rmin1)) / (Rmax1 - Rmin1);
	      double A1 = rmin1 * exp(-B1 * Rmin1);
	      double RR1 = (i + 0.5) / NR;
	      double rr1 = A1 * exp(B1 * RR1); // Physical radius when conv_factor = 1
	      double B2 = (log(rmax) - log(rmin)) / (Rmax - Rmin);
	      double A2 = rmin * exp(-B2 * Rmin);
	      double RR2 = log(rr1 / A2) / B2; // Inverse transformation from physical to uniform grid
	      double num = gsl_spline_eval(spline, RR2, acc); // Interpolate at points as if conv_factor = 1
	      double soln[2];
	      Exact_soln(t, RR2, soln); // Evaluate the exact solution
	      double ana = soln[0];

	      //printf("RR1 = %f, rr1 = %f\n", RR1, rr1);

	      fprintf(out1Dinterp, "%d %e %e %e %e %e %.16e %.16e\n", frame_ind, t, RR2, rr1, num, ana, fabs((num - ana) / ana), fabs(num - ana));
	    }

	    fprintf(out1Dinterp, "\n\n");*/
	} // END if(n % frame_jump == 0)

      // Update grid functions for next time step
#pragma omp parallel for
      for(int i = 0; i < Npts; i++)
	{
	  // Update time step
	  u[i] = unp1[i];
	  v[i] = vnp1[i];
      
	  if(!isfinite(u[i]))
	    {
	      printf("BAD n = %d, t = %e, u[%d] = %e\n", n, t, i, u[i]);
	      exit(1);
	    }
	}
    } // END for(n < N_iters)

  // Free GSL spline interpolator
  //gsl_spline_free(spline);
  //gsl_interp_accel_free(acc);
  
  // Close file IO
  fclose(out0D);
  fclose(out1D);
  //fclose(out1Dinterp);
  
  // Free variable storage
  free(RR);
  free(L2un);
  free(L3un);
  free(u);
  free(v);
  free(u1);
  free(v1);
  free(unp1);
  free(vnp1);

  return 0;
}
