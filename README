NRPy+: Python-based Code Generation for 
       Numerical Relativity... and Beyond!

Core Developers:
Zachariah B. Etienne
Ian Ruchlin

=================================================

This repository contains NRPy+ as well as the following 
codes that depend on NRPy+:

1) SENR: a Simple, Efficient Numerical Relativity code

SENR/NRPy+ Homepage:
   http://math.wvu.edu/~zetienne/SENR

   SENR/NRPy+ Paper:
   https://arxiv.org/abs/1712.07658

2) scalarwave: a tiny, pedagogic code for solving the scalar
   wave equation in many coordinate systems
   
3) FishboneMoncrief: a black hole accretion disk initial data 
   module ("thorn") for the Einstein Toolkit ("Cactus"). 
   Uses a Kerr-Schild background metric for the black hole.

=================================================

Quick Start for new users of NRPy+:
0) Download NRPy+ using git (https://git-scm.com/downloads):
   git clone https://bitbucket.org/zach_etienne/nrpy.git
1) Go to the tutorial/ subdirectory, generate the scalar wave
   tutorial PDF file with `make` (you will need LaTeX installed),
   and then follow the tutorial. This will familiarize you with 
   NRPy+ in the context of solving the scalar wave equation.

Quick Start for SENR ("a Simple, Efficient Numerical 
    Relativity code"):
0) Download SENR/NRPy+ using git (https://git-scm.com/downloads):
   git clone https://bitbucket.org/zach_etienne/senr.git
1) Follow directions in SENR_NRPy_quick_intro.txt to
   generate needed C code for SENR.
2) Go to the SENR/ subdirectory. This is 
   where the NRPy+/SENR (BSSN) code is located.
3) If you have the Intel C Compiler (icc) installed,
   (RECOMMENDED), to compile the SENR (BSSN) code, type:
   make -j
   If you have only the GCC C compiler installed,
   (NOT RECOMMENDED, much slower executables), to compile
   the BSSN code, type:
   make -f Makefile-gcc -j
4) Run SENR (BSSN) via the command
   ./SENR 128 32 2 1000.0 0.15
   Tip: Install 'taskset' to use processor core affinity
        for better performance. If you have N cores, type
        taskset -c 0,1,..,(N/2)-1 ./SENR 128 32 2 1000.0 0.15
        For example, if you have 8 cores, type:
        taskset -c 0,1,2,3 ./SENR 128 32 2 1000.0 0.15
    You will find output files in the output/ directory.
    To see what is stored in these files, check out
    diagnostics/Data_File_Output.c

Repository Directory Contents:

- [root directory, probably "nrpy"]/
  > NRPy+, the Python-based code generator for SENR,
    scalar wave, and Fishbone-Moncrief (Einstein Toolkit
    initial data module) exists in the NRPy+/ subdirectory. 
    * See SENR_NRPy_quick_intro.txt for a brief introduction
- SENR/ (a.k.a. SENR/NRPy+; the numerical relativity code)
  > Currently contains:
    * Full BSSN implementation in spherical polar, sinh-radial
      (r -> sinh(r)) spherical polar, Cartesian, cylindrical,
      sinh-cylindrical, and modified (symmetric) TwoPunctures,
      including
      - RK4 timestepping (same stability features as PIRK2,
        except without the additional algorithmic 
        complications)
      - Outer boundary conditions: quadratic extrapolation
        (default), linear extrapolation, Sommerfeld
      - Diagnostics: 
        . Hamiltonian and momentum constraints
        . ADM Mass, ADM angular momentum, ADM linear momentum
        . 1D, 2D, and (disabled by default) 3D output of
          grid and diagnostic functions
        . SinhSpherical-to-Cartesian Einstein Toolkit diagnostic
          compatibility layer (enable with
          -DENABLE_SPHERICAL_TO_CARTESIAN_ETK_LAYER
          compiler flag in Makefile, currently only for 
          SinhSpherical coordinates).

    * Initial data modules in arbitrary coordinates for
      - Single black hole in UIUC coordinates 
        (https://arxiv.org/abs/1001.4077)
      - Two nonspinning black holes initially at rest 
        (Brill-Lindquist initial data)
      - Static trumpet solution 
        (https://arxiv.org/abs/1403.5484)
      - Boosted Schwarzschild black hole
        (https://arxiv.org/abs/1410.8607)
      - Minkowski spacetime (add initial random perturbation
        to this initial data to perform Apples-to-Apples
        robust stability test: 
        https://arxiv.org/abs/0709.3559)
- utilities/DynamicalCoordinates/
  > Various utilities for visualizing & testing dynamical 
    coords:
    * TP_mods/
      + A code to generate both original and
        modified TwoPunctures coordinate distributions.
    * WaveToy1D/
      + A 1D scalar wave evolved with a second-
        order PIRK integration scheme, on a
        logarithmic coordinate grid.
- utilities/EinsteinToolkit/ET__SENR_bridge__ET_thorn/
  > This Einstein Toolkit thorn enables the large suite of 
    diagnostics within the Einstein Toolkit to be used 
    directly with SENR/NRPy+ output files.
- utilities/EinsteinToolkit/compute_spin
  > Computes the dimensionless black hole spin parameter
    based on (apparent) horizon proper polar to 
    circumferential radii, using Eq 5.2 of 
    https://arxiv.org/pdf/gr-qc/0411149.pdf
  > Can process output from AHFinderDirect
- SENR/old-BSSN-Cartesian-firsttry-arborder-agreeswithKranc/
  > Original (early, experimental) implementation of the BSSN 
    equations (right-hand-sides) in Cartesian coordinates. 
    Implements arbitrary-order finite-difference stencils 
    via adjustment of single parameter ORDER. Results 
    demonstrated to agree to roundoff error with Kranc output
    at 2nd, 4th, 6th, and 8th orders.
- SENR/version_of_SENR_roundoff_agreement_with_Baumgarte_code/
  > Unmaintained version of SENR that implements PIRK2 
    timestepping, in spherical polar coordinates. Used
    for validation, as this version has been shown to agree
    with Baumgarte et al's code to roundoff error.