// Evaluate the Hamiltonian constraint
void Hamiltonian_Constraint(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  {
    Compute_Ricci_pt1(t,x1G,x2G,x3G,yy,in_gfs,gfs_aux,precomp,params);
    Compute_Ricci_pt2(t,x1G,x2G,x3G,yy,in_gfs,gfs_aux,precomp,params);
  }

  LOOP_NOGZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];
#include "NRPy_codegen/Hamiltonian_Constraint.h"
  }

  return;
}

// Evaluate momentum constraint
void Momentum_Constraint(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, REAL *gfs_aux, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  LOOP_NOGZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];

    REAL term1[3], term2[3], term3[3], term4[3], term5[3], AbarUU[3][3], phi, phid1, phid2, phid3;

#include "NRPy_codegen/Momentum_Constraint.h"
  }

  return;
}

// Evaluate the Conformal Connection Coefficient constraint
void Connection_Coefficient_Constraint(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, REAL *gfs_aux, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  LOOP_NOGZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];
#include "NRPy_codegen/Connection_Constraint.h"
  }

  return;
}
