#ifdef ENABLE_SPHERICAL_TO_CARTESIAN_ETK_LAYER
void interpolate_dump_gammaij_Kij_Cartesian_grid(FILE *outCartGrid, paramstruct params, const int N, /**/ const int Nx, const int Ny, const int Nz,
                                                 const REAL xmin,const REAL ymin,const REAL zmin, /**/ const REAL xmax,const REAL ymax,const REAL zmax,
                                                 const int numgfs, int interp_gfs_list[numgfs],REAL *in_gfs, REAL *x1G,REAL *x2G,REAL *x3G) {
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/
#include "read_FD_dxs.h" // Located in ../common_functions/

  double x[N];
  double y[N];
  double z[N];
  for(int i=0;i<N;i++) {
    x[i] = i*del[0];
    y[i] = i*del[1];
    z[i] = i*del[2];
  }

  double w_x[N];
  double w_y[N];
  double w_z[N];
  get_w_double(N,w_x,x);
  get_w_double(N,w_y,y);
  get_w_double(N,w_z,z);

  // x should range from [xmin,xmax], inclusive. So if
  // x_i = xmin + i*out_dx , and i ranges from [0,Nx-1], then
  // xmax = xmin + (Nx-1)*out_dx -> out_dx = (xmax-xmin)/(Nx-1)
  double out_dx = (double)(xmax - xmin) / ((double)Nx - 1.0);
  double out_dy = (double)(ymax - ymin) / ((double)Ny - 1.0);
  double out_dz = (double)(zmax - zmin) / ((double)Nz - 1.0);

#define ALLOCATE_2D_GENERIC(type,array,ni,nj) type **array=(type **)malloc(ni * sizeof(type *)); \
  for(int cc = 0; cc < ni; cc++)                 array[cc]=(type * )malloc(nj * sizeof(type));
#define FREE_2D_GENERIC(type,array,ni,nj) for(int cc = 0; cc < ni;cc++) free((void *)array[cc]); \
  /**/                                                                  free((void *)array);

  ALLOCATE_2D_GENERIC(double, out_interp_gfs,   numgfs, Nx*Ny*Nz);

#pragma omp parallel for
  for(int kk=0;kk<Nz;kk++) {
    double zCart = (double)zmin + (double)kk*out_dz;
    for(int jj=0;jj<Ny;jj++) {
      double yCart = (double)ymin + (double)jj*out_dy;
      for(int ii=0;ii<Nx;ii++) {
        double xCart = (double)xmin + (double)ii*out_dx;
        int ijk_idx[3][N];
        int ll= ii + Nx * ( jj + Ny * kk );

        double xxCart[3];
        {
        // xCart,yCart,zCart = output x,y,z
        // Following .h file: Inputs xCart,yCart,zCart; Outputs xxCart[0,1,2] corresponding to x0,x1,x2
#include "reference_metric/NRPy_codegen/xx_in_terms_of_Cartxyz.h"
        }
        {
          double Cartxyz[3][1];
          const int idx = 0;
          double x1 = xxCart[0];
          double x2 = xxCart[1];
          double x3 = xxCart[2];
#include "reference_metric/NRPy_codegen/xyz.h"
          if(fabs(Cartxyz[0][0]-xCart) > out_dx*1e-4 ||
             fabs(Cartxyz[1][0]-yCart) > out_dy*1e-4 ||
             fabs(Cartxyz[2][0]-zCart) > out_dz*1e-4) {
            printf("BADVAL: %.15e %.15e | %.15e %.15e | %.15e %.15e\n",(double)Cartxyz[0][0],(double)xCart,(double) Cartxyz[1][0],(double)yCart,(double) Cartxyz[2][0],(double)zCart);
            exit(1);
          }
        }


        double l_of_x = 1.0;
        double l_of_y = 1.0;
        double l_of_z = 1.0;

        double w_x_i_times_xdiff_inv[N];
        double w_y_i_times_ydiff_inv[N];
        double w_z_i_times_zdiff_inv[N];

        // x_i = x_0 + i * dx0
        // -> i = (x_i - x_0) / dx0
        // --> nearest point x_i to x0_out will be at index i_out = (x0_out - x_0) / dx0
        // Our interpolation stencil should be centered at i,j,k.
        // Thus, leftmost point of the stencil in i-direction
        //       should be at i - N/2
        // Note that we add 0.5 to i before the integer typecast to
        //       ensure proper rounding!
        int first_idx[3] = {(int)((xxCart[0] - x1G[0]) * invdx0 + 0.5) - N/2,
                            (int)((xxCart[1] - x2G[0]) * invdx1 + 0.5) - N/2,
                            (int)((xxCart[2] - x3G[0]) * invdx2 + 0.5) - N/2};

        for(int i=0;i<N;i++) {

          for(int j=0;j<3;j++) ijk_idx[j][i] = i + first_idx[j];

          if(ijk_idx[0][i] > Npts1-1) { printf("BAD I %d %e %e\n",ijk_idx[0][i],(double)xxCart[0],(double)x1G[0]); }
          if(ijk_idx[1][i] > Npts2-1) { printf("BAD J %d %e %e\n",ijk_idx[1][i],(double)xxCart[1],(double)x2G[0]); }
          if(ijk_idx[2][i] > Npts3-1) { printf("BAD K %d %e %e\n",ijk_idx[2][i],(double)xxCart[2],(double)x3G[0]); }

          if(ijk_idx[0][i] < 0) { printf("minBAD I %d %e %e\n",ijk_idx[0][i],(double)xxCart[0],(double)x1G[0]); }
          if(ijk_idx[1][i] < 0) { printf("minBAD J %d %e %e\n",ijk_idx[1][i],(double)xxCart[1],(double)x2G[0]); }
          if(ijk_idx[2][i] < 0) { printf("minBAD K %d %e %e\n",ijk_idx[2][i],(double)xxCart[2],(double)x3G[0]); }

          double xdiff = xxCart[0] - x1G[ijk_idx[0][i]];
          double ydiff = xxCart[1] - x2G[ijk_idx[1][i]];
          double zdiff = xxCart[2] - x3G[ijk_idx[2][i]];

          w_x_i_times_xdiff_inv[i] = w_x[i] / xdiff;
          w_y_i_times_ydiff_inv[i] = w_y[i] / ydiff;
          w_z_i_times_zdiff_inv[i] = w_z[i] / zdiff;
          l_of_x *= xdiff;
          l_of_y *= ydiff;
          l_of_z *= zdiff;
        }
        // Now that we have precomputed the coefficients, let's perform the interpolations.
        for(int which_gf_i=0;which_gf_i<numgfs;which_gf_i++) {
          int which_gf = interp_gfs_list[which_gf_i];
          out_interp_gfs[which_gf_i][ll]=0.0;
          // First fill the stencil with all needed points.
          //for(int i=0;i<N;i++) for(int j=0;j<N;j++) for(int k=0;k<N;k++) {
          for(int k=0;k<N;k++) for(int j=0;j<N;j++) for(int i=0;i<N;i++) {
                const double f_ijk  = (double)in_gfs[IDX4(which_gf, ijk_idx[0][i],ijk_idx[1][j],ijk_idx[2][k])];
                out_interp_gfs[which_gf_i][ll] += f_ijk*w_x_i_times_xdiff_inv[i]*w_y_i_times_ydiff_inv[j]*w_z_i_times_zdiff_inv[k];
              }
          out_interp_gfs[which_gf_i][ll] *= l_of_x*l_of_y*l_of_z;
        }

        // Next un-rescale. Need xxCart
        {
          double x1 = xxCart[0];
          double x2 = xxCart[1];
          double x3 = xxCart[2];
          double scalefactor_orthog[3];
          {
#include "reference_metric/NRPy_codegen/scalefactor_orthog.h"
          }
          int count=0;
          for(int i=0;i<3;i++) for(int j=i;j<3;j++) {
              //int interp_gfs_list[14] = {A11,A12,A13,A22,A23,A33,H11,H12,H13,H22,H23,H33,TRK,CF};
              //                             0   1   2   3   4   5   6   7   8   9  10  11  12 13

              // Unrescaling step:
              // a_{ij} -> Abar_{ij}:
              out_interp_gfs[count  ][ll] *= scalefactor_orthog[i]*scalefactor_orthog[j];
              // h_{ij} -> epsilon_{ij}:
              out_interp_gfs[count+6][ll] *= scalefactor_orthog[i]*scalefactor_orthog[j];

              // Add the gammahat back to the 3-metric
              // epsilon_{ij} -> gammabar_{ij}
              if(i==j) out_interp_gfs[count+6][ll] += scalefactor_orthog[i]*scalefactor_orthog[j];

              count++;
            }
          // Then change basis to Cartesian:
          // Jacobian matrix:
          double dxdy[3][3];
          {
#include "reference_metric/NRPy_codegen/Inverse_Jacobian_Matrix.h"
          }
          double Abar_ij_Sph[3][3] ,gammabar_ij_Sph[3][3];
          double Abar_ij_Cart[3][3],gammabar_ij_Cart[3][3];
          count=0;
          for(int i=0;i<3;i++) for(int j=i;j<3;j++) {
              /**/Abar_ij_Sph[i][j] =     Abar_ij_Sph[j][i] = out_interp_gfs[count  ][ll];
              gammabar_ij_Sph[i][j] = gammabar_ij_Sph[j][i] = out_interp_gfs[count+6][ll];

              if(isnan(Abar_ij_Sph[i][j])) printf("Bad1 %d %d %e\n",i,j,(double)Abar_ij_Sph[i][j]);

              count++;
            }
          for(int i=0;i<3;i++) for(int j=0;j<3;j++) {
              Abar_ij_Cart[i][j]     = 0.0;
              gammabar_ij_Cart[i][j] = 0.0;
            }
          for(int i=0;i<3;i++) for(int j=0;j<3;j++) {
              for(int k=0;k<3;k++) for(int l=0;l<3;l++) {
                  /**/Abar_ij_Cart[i][j] += dxdy[k][i]*dxdy[l][j]*    Abar_ij_Sph[k][l];
                  gammabar_ij_Cart[i][j] += dxdy[k][i]*dxdy[l][j]*gammabar_ij_Sph[k][l];
                  if(isnan(Abar_ij_Cart[i][j])) printf("Bad2 %d %d %e\n",i,j,(double)Abar_ij_Cart[i][j]);
                }
            }
          count=0;
          for(int i=0;i<3;i++) for(int j=i;j<3;j++) {
              out_interp_gfs[count  ][ll] = Abar_ij_Cart[i][j];
              out_interp_gfs[count+6][ll] = gammabar_ij_Cart[i][j];
              count++;
            }
        }
      }
    }
  }
  //FIXME: CHECK CFEvolution variable. Hard-coded to W = psi^{-2}.
#pragma omp parallel for
  for(int ll=0;ll<Nx*Ny*Nz;ll++) {
    // Next convert to ADM variables:
    //int interp_gfs_list[14] = {A11,A12,A13,A22,A23,A33,H11,H12,H13,H22,H23,H33,TRK,CF};
    //                             0   1   2   3   4   5   6   7   8   9  10  11  12 13
    double psim2 = out_interp_gfs[13][ll];
    double psi4 = 1.0/(psim2*psim2);
    double trK  = out_interp_gfs[12][ll];
    for(int count=0;count<6;count++) {
      double Abar_ij     = out_interp_gfs[count  ][ll];
      double gammabar_ij = out_interp_gfs[count+6][ll];
      double gamma_ij = psi4*gammabar_ij;
      double K_ij     = psi4*Abar_ij + (1.0/3.0)*gamma_ij*trK;
      if(isnan(K_ij)) printf("Bad3 %d %e | %e %e %e %e %e\n",count,(double)K_ij,(double)psi4,(double)Abar_ij,(double)trK,(double)gamma_ij,(double)psim2);
      out_interp_gfs[count  ][ll] = K_ij;
      out_interp_gfs[count+6][ll] = gamma_ij;
    }
  }
  printf("checksum\nkxx: %e \ngxx: %e\n",(double)out_interp_gfs[0][0],(double)out_interp_gfs[6][0]);
  /*
  for(int ll=0;ll<Nx*Ny*Nz;ll++) {
    out_interp_gfs[0][ll] = out_interp_gfs[13][ll];
  }
  */
  // Output ADM variables :
  for(int which_gf_i=0;which_gf_i</*K_ij & gamma_ij ONLY*/12/**/;which_gf_i++) {
    fwrite(out_interp_gfs[which_gf_i],sizeof(double),Nx*Ny*Nz,outCartGrid);
  }
  FREE_2D_GENERIC(double, out_interp_gfs,   numgfs, Nx*Ny*Nz);
}
#undef ALLOCATE_2D_GENERIC
#undef FREE_2D_GENERIC

#endif // ENABLE_SPHERICAL_TO_CARTESIAN_ETK_LAYER


void Data_File_Output(FILE *out0D, FILE *out1D, FILE *out2D, FILE *out3D, const int frame_ind, const int n, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/

  //const int whichpt_x1 = Npts1 / 2;
  const int whichpt_x2 = Npts2 / 2;
  //const int whichpt_x2 = NGHOSTS;
  const int whichpt_x3 = Npts3 / 2;

  // Evaluate Hamiltonian constraint gfs_aux[HAM]
  Hamiltonian_Constraint(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, precomp,params);

  // Evaluate momentum constraint gfs_aux[MOM1], gfs_aux[MOM2], gfs_aux[MOM3]
  Momentum_Constraint(x1G,x2G,x3G, gfs_n, gfs_aux, params);

  // Evaluate Conformal Connection Coefficient constraint gfs_aux[CC1], gfs_aux[CC2], gfs_aux[CC3]
  //Connection_Coefficient_Constraint(x1G,x2G,x3G, gfs_n, gfs_aux, params);

  const REAL ham_L2 = L2_Norm_Over_Entire_Grid(HAM, gfs_aux, params);
  const REAL ham_RMS = RMS_Norm_Over_Entire_Grid(HAM, gfs_aux, params);
  const REAL ham_max = Max_Over_Entire_Grid(HAM, gfs_aux, params);
  const REAL cf_sample = gfs_n[IDX4(CF,Npts1 / 2, Npts2 / 2, Npts3 / 2)];
  //const REAL cc1_L2 = L2_Norm_Over_Entire_Grid(CC1, gfs_aux, params);
  //const REAL cc2_L2 = L2_Norm_Over_Entire_Grid(CC2, gfs_aux, params);
  //const REAL cc3_L2 = L2_Norm_Over_Entire_Grid(CC3, gfs_aux, params);

#if defined FIND_HORIZON
  // Evaluate null expansion, assuming spherical symmetry
  Spherical_Symmetry_Null_Expansion(x1G,x2G,x3G, yy, gfs_n, gfs_aux, params);
  const REAL rAH = Spherical_Symmetry_Find_Horizon_Radius(yy, gfs_aux, params);
#endif

  // 0D output
  fprintf(out0D, "%e ", (double)t); // Time

#if defined EVALUATE_ADM_INTEGRALS
  for(int i=0;i<TARGET_RADIUS_NUM_RADII;i++) {
    // Evaluate ADM mass, linear momentum, and angular momentum integrals
    REAL ADM_MPJ[7];
    REAL ACTUAL_RADIUS = ADM_Integrals(x1G,x2G,x3G, yy, gfs_n, ADM_MPJ, params, TARGET_RADIUS[i]);

    // Output ADM Mass, ADM Jx,Jy,Jz, then ADM Px,Py,Pz:
    fprintf(out0D, "%.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e || ",(double)ACTUAL_RADIUS,(double) ADM_MPJ[0],(double) ADM_MPJ[4],(double) ADM_MPJ[5],(double) ADM_MPJ[6],(double) ADM_MPJ[1],(double) ADM_MPJ[2],(double) ADM_MPJ[3]);
  }
  //printf("ADM M = %.15e\nADM P = (%.15e, %.15e, %.15e)\nADM J = (%.15e, %.15e, %.15e)\n", ADM_MPJ[0], ADM_MPJ[1], ADM_MPJ[2], ADM_MPJ[3], ADM_MPJ[4], ADM_MPJ[5], ADM_MPJ[6]);
#endif

  fprintf(out0D, "%.15e %.15e %.15e %.15e ", (double)ham_L2, (double)ham_RMS, (double)cf_sample, (double)ham_max); // Grid function data
  // fprintf(out0D, "%.15e %.15e %.15e ", (double)cc1_L2, (double)cc2_L2, (double)cc3_L2); // Conformal connection coefficient constraint

#if defined FIND_HORIZON
  fprintf(out0D, "%.15e ", (double)rAH);
#endif
  fprintf(out0D, "%d %d ", frame_ind, n); // Grid function data

  fprintf(out0D, "\n");

  REAL *in_gfs = gfs_n;

  // 1D output
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
    const int idx = IDX3(i, whichpt_x2, whichpt_x3); // Output the y1 axis
    const REAL x1 = x1G[i];
    const REAL y1 = yy[IDX4pt(0,idx)];
    const REAL y2 = yy[IDX4pt(1,idx)];
    const REAL y3 = yy[IDX4pt(2,idx)];
    const REAL vet1 = gfs_n[IDX4pt(VET1,idx)];
    const REAL h11 = gfs_n[IDX4pt(H11,idx)];
    const REAL cf = gfs_n[IDX4pt(CF,idx)];
    const REAL alpha = gfs_n[IDX4pt(ALPHA,idx)];
    const REAL trK = gfs_n[IDX4pt(TRK,idx)];
    const REAL ham = gfs_aux[IDX4pt(HAM,idx)];
    const REAL mom1 = gfs_aux[IDX4pt(MOM1,idx)];
    const REAL mom2 = gfs_aux[IDX4pt(MOM2,idx)];
    const REAL mom3 = gfs_aux[IDX4pt(MOM3,idx)];

    fprintf(out1D, "%.15e %f %f %f %f ",(double)y1, log10(fabs((double)ham)), log10(fabs((double)mom1)), log10(fabs((double)mom2)), log10(fabs((double)mom3)));
    fprintf(out1D, "%.15e %.15e %.15e %.15e %.15e %d ", (double)cf, (double)alpha, (double)vet1, (double)h11, (double)trK, frame_ind);

#if defined FIND_HORIZON
    // Print null expansion
    // Assuming spherical symmetry
    fprintf(out1D, "%.15e ", (double)gfs_aux[IDX4pt(NULLEXP,idx)]);
#endif

    fprintf(out1D, "\n");
  }

  fprintf(out1D, "\n\n");

  // DISABLE 2D
  //return;

  for(int jj = 0; jj < Npts2 - NGHOSTS; jj++) {
    for(int ii = 0; ii < Npts1 - NGHOSTS; ii++) {
      const int idx = IDX3(ii, jj, whichpt_x3);
      const REAL cf = gfs_n[IDX4pt(CF,idx)];
      const REAL alpha = gfs_n[IDX4pt(ALPHA,idx)];
      const REAL ham = gfs_aux[IDX4pt(HAM,idx)];
      const REAL vet1 = gfs_n[IDX4pt(VET1,idx)];
      const REAL vet2 = gfs_n[IDX4pt(VET2,idx)];
      const REAL vet3 = gfs_n[IDX4pt(VET3,idx)];
      int kk = whichpt_x3;
      const REAL x1 = x1G[ii];
      const REAL x2 = x2G[jj];
      const REAL x3 = x3G[kk];
#include "NRPy_codegen/shift.h"

      REAL xCart, yCart, zCart;

      {
#include "../../common_functions/reference_metric/NRPy_codegen/xxCart.h"
      }

      fprintf(out2D, "%f %f ", (double)xCart, (double)yCart); // Coordinates
      fprintf(out2D, "%.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e | %d %d %e %d %d \n", log10(fabs((double)ham)), (double)cf, (double)alpha,(double)vet1,(double)vet2,(double)vet3,(double)beta1,(double)beta2,(double)beta3, frame_ind, n, (double)t, ii, jj); // Grid function data
    }

    fprintf(out2D, "\n");
  }

  fprintf(out2D, "\n");


  /*
  // 3D output
  //for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++)
  for(int k = 0; k < Npts3; k++)
    {
      //for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++)
      for(int j = 0; j < Npts2; j++)
	{
	  //for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++)
	  for(int i = 0; i < Npts1; i++)
	    {
	      const int idx = IDX3(i, j, k);
	      const REAL y1 = yy[IDX4pt(0,idx)];
	      const REAL y2 = yy[IDX4pt(1,idx)];
	      const REAL y3 = yy[IDX4pt(2,idx)];
	      const REAL phi = gfs_n[IDX4pt(CF,idx)];
	      const REAL alpha = gfs_n[IDX4pt(ALPHA,idx)];
	      const REAL ham = gfs_aux[IDX4pt(HAM,idx)];

	      fprintf(out3D, "%d %d %e %d %d %d %e %e %e ", frame_ind, n, t, i, j, k, y1, y2, y3); // Coordinates
	      //fprintf(out3D, "%.15e %.15e %.15e\n", phi, alpha, ham); // Grid function data

	      fprintf(out3D, "%.15e %.15e %.15e %.15e %.15e %.15e\n", gfs_n[IDX4pt(H11,idx)], gfs_n[IDX4pt(H12,idx)], gfs_n[IDX4pt(H13,idx)], gfs_n[IDX4pt(H22,idx)], gfs_n[IDX4pt(H23,idx)], gfs_n[IDX4pt(H33,idx)]);
	    }

	  fprintf(out3D, "\n");
	}

      //fprintf(out3D, "\n");
      }

  */
  //fprintf(out3D, "\n");
  return;
}
