// University of Illinois Urbana-Champaign Kerr initial data.
// UIUC definitions appear in Liu, Etienne, & Shapiro PRD 80 121503(R) (2009); 
//                            http://arxiv.org/abs/1001.4077.
// Calculate the 3D (spatial) Lorentz transformation and its inverse
void Lorentz_Transformation_3D(double LT[3][3], double iLT[3][3])
{
  // Add zero boost:
  double par_P[3]; for(int i=0;i<3;i++) par_P[i]=0.0;
  const double par_M = 1.0;

  double KD[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}; // 3D Kronecker delta
  double Pmag = sqrt(par_P[0] * par_P[0] + par_P[1] * par_P[1] + par_P[2] * par_P[2]); // Momentum magnitude
  double v[3]; // Three-velocity

  for(int i = 0; i < 3; i++)
    {
      v[i] = par_P[i] / sqrt(par_M * par_M + Pmag * Pmag);
    }

  double W = pow(1.0 - v[0] * v[0] - v[1] * v[1] - v[2] * v[2], -0.5); // Lorentz factor

  for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
	{
	  LT[i][j]  = KD[i][j] + W * W / (1.0 + W) * v[i] * v[j]; // Lorentz transformation
	  iLT[i][j] = KD[i][j] -     W / (1.0 + W) * v[i] * v[j]; // Inverse Lorentz transformation
	}
    }
}

void UIUC_ID(const double x, const double y, const double z, double g[3][3], double K[3][3], double N[4])
{
  const int shift_type = 1; // stationary shift
  //const int shift_type = 0; // shift zero everywhere
  const int lapse_type = 4; // puncture_r2: alpha = pow(psi, -2);
  const double par_M   = 1.0; // Mass of BH
  const double par_chi = 0.9; // Spin parameter
  const int avoid_puncture = 1;

  double epsilon = 1.0e-10;

  double M = par_M; // Mass parameter
  double a = M * par_chi; // Spin per unit mass, in terms of dimensionless spin parameter par_chi
  double rm = M - sqrt(M * M - a * a); // UIUC inner horizon, equation appears in text after Eq (1) in Liu et al
  double rp = M + sqrt(M * M - a * a); // UIUC outer horizon, equation appears in text after Eq (1) in Liu et al

  // Calculate Lorentz transformation matrix
  double LT[3][3], iLT[3][3];
  Lorentz_Transformation_3D(LT, iLT);

  double xf[3] = {x, y, z}; // Field point
  double xS[3]; // Stationary coordinates

  for(int i = 0; i < 3; i++)
    {
      xS[i] = 0;

      for(int j = 0; j < 3; j++)
	{
	  xS[i] += iLT[i][j] * xf[j]; // Transform field point from boosted to stationary frame
	}
    }

  double rUIUC = sqrt(xS[0] * xS[0] + xS[1] * xS[1] + xS[2] * xS[2]); // UIUC radial coordinate
  double r_small = 1e-5*par_M;
  
  // If we're too close to the coordinate singularity at the puncture, shift away
  if(rUIUC < r_small && avoid_puncture)
    {
      rUIUC = r_small;
    }

  double th = acos(xS[2] / rUIUC); // UIUC latitude coordinate
  double ph = atan2(xS[1], xS[0]); // UIUC longitude coordinate
  double Sth = sin(th), Cth = cos(th), Tth = tan(th), Sph = sin(ph), Cph = cos(ph);
  double rBL = rUIUC * pow(1 + 0.25 * rp / rUIUC, 2); // Boyer-Lindquist radial coordinate, Liu et al Eq (11)
  double SIG = rBL * rBL + pow(a * Cth, 2); // Boyer-Lindquist "Sigma", equation appears in text after Eq (2) in Liu et al
  double DEL = rBL * rBL - 2 * M * rBL + a * a; // Boyer-Lindquist "Delta", equation appears in text after Eq (2) in Liu et al
  double A = pow(rBL * rBL + a * a, 2) - DEL * pow(a * Sth, 2); // equation appears in text after Eq (2) in Liu et al

  // Physical spatial metric in spherical basis, Liu et al Eq (13)
  double grr = SIG * pow(rUIUC + 0.25 * rp, 2) * pow(rUIUC, -3) / (rBL - rm);
  double gthth = SIG;
  double gphph = A * Sth * Sth / SIG;

  // Physical extrinsic curvature in spherical basis, Liu et al Eqs (14) and (15)
  double Krph = M * a * Sth * Sth * (3 * pow(rBL, 4) + 2 * pow(a * rBL, 2) - pow(a, 4) - (rBL * rBL - a * a) * pow(a * Sth, 2)) * (1 + 0.25 * rp / rUIUC) / (SIG * sqrt(A * SIG * rUIUC * (rBL - rm)));
  double Kthph = - 2 * pow(a, 3) * M * rBL * Cth * pow(Sth, 3) * (rUIUC - 0.25 * rp) * sqrt(rBL - rm) / (SIG * sqrt(A * SIG * rUIUC));

  double g0[3][3], K0[3][3]; // Stationary spatial metric and extrinsic curvature

  // Near the z-axis
  if(rUIUC * Sth < epsilon)
    {
      g0[0][0] = (a * a + pow(rp + 4 * fabs(xS[2]), 4) / (256 * xS[2] * xS[2])) / (xS[2] * xS[2]);
      g0[0][1] = 0;
      g0[0][2] = 0;
      g0[1][1] = g0[0][0];
      g0[1][2] = 0;
      g0[2][2] = -pow(rp + 4 * fabs(xS[2]), 2) * g0[0][0] / (a * a - 2 * (M * rp + 8 * xS[2] * xS[2]) + 8 * fabs(xS[2]) * (M - 3 * sqrt(M * M - a * a)));
      
      g0[1][0] = g0[0][1];
      g0[2][0] = g0[0][2];
      g0[2][1] = g0[1][2];

      int i, j;

      for(i = 0; i < 3; i++)
	{
	  for(j = 0; j < 3; j++)
	    {
	      K0[i][j] = 0;
	    }
	}
    }
  else
    {
      // ADMBase physical spatial metric (Cartesian basis)
      g0[0][0] = (gthth * pow(Cph * Cth, 2)) / pow(rUIUC, 2) + (gphph * pow(Sph, 2)) / (pow(rUIUC, 2) * pow(Sth, 2)) + grr * pow(Cph, 2) * pow(Sth, 2);
      g0[0][1] = (gthth * Cph * pow(Cth, 2) * Sph) / pow(rUIUC, 2) - (gphph * Cph * Sph) / (pow(rUIUC, 2) * pow(Sth, 2)) + grr * Cph * Sph * pow(Sth, 2);
      g0[0][2] = grr * Cph * Cth * Sth - (gthth * Cph * Cth * Sth) / pow(rUIUC, 2);
      g0[1][1] = (gphph * pow(Cph, 2)) / (pow(rUIUC, 2) * pow(Sth, 2)) + (gthth * pow(Cth, 2) * pow(Sph, 2)) / pow(rUIUC, 2) + grr * pow(Sph, 2) * pow(Sth, 2);
      g0[1][2] = grr * Cth * Sph * Sth - (gthth * Cth * Sph * Sth) / pow(rUIUC, 2);
      g0[2][2] = grr * pow(Cth, 2) + (gthth * pow(Sth, 2)) / pow(rUIUC, 2);
      
      g0[1][0] = g0[0][1];
      g0[2][0] = g0[0][2];
      g0[2][1] = g0[1][2];

      // ADMBase physical extrinsic curvature (Cartesian basis)
      K0[0][0] = (-2 * Krph * Cph * Sph) / rUIUC - (2 * Kthph * Cph / Tth * Sph) / pow(rUIUC, 2);
      K0[0][1] = (Krph * pow(Cph, 2)) / rUIUC + (Kthph * pow(Cph, 2) / Tth) / pow(rUIUC, 2) - (Krph * pow(Sph, 2)) / rUIUC - (Kthph / Tth * pow(Sph, 2)) / pow(rUIUC, 2);
      K0[0][2] = (Kthph * Sph) / pow(rUIUC, 2) - (Krph / Tth * Sph) / rUIUC;
      K0[1][1] = (2 * Krph * Cph * Sph) / rUIUC + (2 * Kthph * Cph / Tth * Sph) / pow(rUIUC, 2);
      K0[1][2] = -((Kthph * Cph) / pow(rUIUC, 2)) + (Krph * Cph / Tth) / rUIUC;
      K0[2][2] = 0;

      K0[1][0] = K0[0][1];
      K0[2][0] = K0[0][2];
      K0[2][1] = K0[1][2];
    }

  // Lorentz transformation
  for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
	{
	  g[i][j] = 0;
	  K[i][j] = 0;

	  for(int k = 0; k < 3; k++)
	    {
	      for(int l = 0; l < 3; l++)
		{
		  g[i][j] += LT[i][k] * LT[j][l] * g0[k][l];
		  K[i][j] += LT[i][k] * LT[j][l] * K0[k][l];
		}
	    }
	}
    }

  // Determinant of spatial metric
  double detg = -g[0][2] * g[0][2] * g[1][1] + 2 * g[0][1] * g[0][2] * g[1][2] - g[0][0] * g[1][2] * g[1][2] - g[0][1] * g[0][1] * g[2][2] + g[0][0] * g[1][1] * g[2][2];
  double psi = pow(detg, 1.0/12.0);

  // Conformal factor, equation appears in last paragraph of Sec. IIB in Liu et al

  double alpha;
  double beta_x;
  double beta_y;
  double beta_z;

  if(lapse_type == 0)
    {
      // stationary
      alpha = sqrt(DEL * SIG / A); // Lapse function, Liu et al Eq (6)
    }
  else if(lapse_type == 1)
    {
      // unit
      alpha = 1.0;
    }
  else if(lapse_type == 2)
    {
      // trumpet
      alpha = 1.0 / (2.0 * psi - 1.0);
    }
  else if(lapse_type == 3)
    {
      // iso_schw
      alpha = 2.0 / (1.0 + pow(psi, 4));
    }
  else if(lapse_type == 4)
    {
      // puncture_r2
      alpha = pow(psi, -2);
    }

  if(shift_type == 0)
    {
      // zero
      beta_x = 0;
      beta_y = 0;
      beta_z = 0;
    }
  else if(shift_type == 1)
    {
      // stationary
      double beta_phi = - 2 * M * a * rBL / A; // Shift vector in spherical basis, Liu et al Eq (7)
      beta_x = - beta_phi * rUIUC * Sth * Sph;
      beta_y =   beta_phi * rUIUC * Sth * Cph;
      beta_z = 0; 
    }
  /*
  else if(shift_type == 2)
    {
      double beta_r = beta_r_interior * exp(-(rUIUC-beta_r_x0)*(rUIUC-beta_r_x0)/(beta_r_w*beta_r_w)); //ERF(rUIUC, beta_r_x0, beta_r_w);
      beta_x = beta_r * Sth * Cph;
      beta_y = beta_r * Sth * Sph;
      beta_z = beta_r * Cth;
    }
  */

  double N0[3] = {beta_x, beta_y, beta_z};

  // Lorentz transformation
  for(int i = 0; i < 3; i++)
    {
      N[i+1] = 0;

      for(int j = 0; j < 3; j++)
	{
	  N[i+1] += LT[i][j] * N0[j];
	}
    }

  // Lapse and shift
  N[0] = alpha;
  N[1] = beta_x;
  N[2] = beta_y;
  N[3] = beta_z;
}

