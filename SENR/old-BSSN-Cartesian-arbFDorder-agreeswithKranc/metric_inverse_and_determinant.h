#ifndef METRIC_INVERSE_AND_DETERMINANT_H__
#define METRIC_INVERSE_AND_DETERMINANT_H__

double determinant(double gDD[DIM][DIM]) { 
  return 
    gDD[0][0] * gDD[1][1] * gDD[2][2] + 
    gDD[0][1] * gDD[1][2] * gDD[0][2] + 
    gDD[0][2] * gDD[0][1] * gDD[1][2] - 
    gDD[0][2] * gDD[1][1] * gDD[0][2] - 
    gDD[0][1] * gDD[0][1] * gDD[2][2] - 
    gDD[0][0] * gDD[1][2] * gDD[1][2];
}
#define Invertgammabar(gUU, gDD,g) {                                    \
    gUU[0][0] =   ( gDD[1][1] * gDD[2][2] - gDD[1][2] * gDD[1][2] ) / g; \
    gUU[0][1] = - ( gDD[0][1] * gDD[2][2] - gDD[1][2] * gDD[0][2] ) / g; \
    gUU[0][2] =   ( gDD[0][1] * gDD[1][2] - gDD[1][1] * gDD[0][2] ) / g; \
    gUU[1][1] =   ( gDD[0][0] * gDD[2][2] - gDD[0][2] * gDD[0][2] ) / g; \
    gUU[1][2] = - ( gDD[0][0] * gDD[1][2] - gDD[0][1] * gDD[0][2] ) / g; \
    gUU[2][2] =   ( gDD[0][0] * gDD[1][1] - gDD[0][1] * gDD[0][1] ) / g; \
  }
#endif // METRIC_INVERSE_AND_DETERMINANT_H__
