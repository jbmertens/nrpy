#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "../grid_and_gridfunction_setup.h"
#include "../loop_setup.h"

#define N ((ORDER)+1) //Size of the to-be-inverted N x N square matrix 'A'.
#include "matrix_inverse.h"

// Enabling DEBUGMATRIX will cause code to exit after matrix inversion
//#define DEBUGMATRIX

void print_matrix(double matrix[N+1][N+1]) {
  for(int i = 1; i <= N; i++) {
      for(int j = 1; j <= N; j++) printf("\t%E", (float)matrix[i][j]);
      printf("\n"); }
}


int compute_finite_difference_coeffs(double *coeffFIRST,double *coeffSECOND,double *coeffUPWIND) {
  // Set up matrix for computing centered finite difference coefficients.
  /* 'A' is the to-be-inverted matrix. A1 is its copy, which is used to calculate
   * 'I = inverse(A).A1'. Ideally, 'I' should be a perfect identity matrix. 'I' is
   * used to check the quality of the calculated inverse. */
  double A[N+1][N+1], A1[N+1][N+1], I[N+1][N+1];

  /* Its i-th row shows the position of '1' in the i-th row of the pivot that is used
   * when performing the LUP decomposition of A. The rest of the elements in that row of
   * the pivot would be zero. In this program, we call this array 'P' a 'permutation'. */
  int P[N+1];

  /* This function calculates inverse of the matrix A. It accepts the LUP decomposed
   * matrix through 'LU' and the corresponding pivot through 'P'. The inverse is
   * returned through 'LU' itself. The 'B' matrix and 'X' & 'Y' vectors are used for
   * temporary storage. This function is defined after the function 'LUPdecompose()'.
   * * */
  double B[N+1][N+1], X[N+1], Y[N+1]; //Temporary storage.

  /* Define the to-be-inverted matrix, A. 
     We define A row-by-row, via the following rules
     that work for arbitrary order:
     1) The top row is all 1's,
     2) If the second row has N elements (N must be odd),
     .... then the radius of the stencil is rs = (N-1)/2
     .... and the j'th element e_j = j-rs-1. For example,
     .... for 4th order, we have rs = 2
     .... j  | element
     .... 1  | -2
     .... 2  | -1
     .... 3  |  0
     .... 4  |  1
     .... 5  |  2
     3) The L'th row, L>2 will be the same as the second 
     .... row, but with each element e_j -> e_j^(L-1)
     A1 is used later to validate the inverted
   * matrix. */
  for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
    A[i][j] = A1[i][j] = 1.0;
  }
  for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
      double rs = (N-1.0)/2.0;
      A[i][j] = A1[i][j] = pow((double)j - rs - 1.0, i-1.0);
    }

#ifdef DEBUGMATRIX
  printf("#The to-be-inverted matrix 'A':\n"); print_matrix(A);
#endif

  /* Performing LUP-decomposition of the matrix 'A'. If successful, the 'U' is stored in
   * its upper diagonal, and the 'L' is stored in the remaining traigular space. Note that
   * all the diagonal elements of 'L' are 1, which are not stored. */
  if(LUPdecompose(N+1, A, P) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#The LUP decomposition of 'A' is successful.\n#Pivot:\n");
  for(int i = 1; i <= N; i++) { for(int j = 1; j <= N; j++) printf("\t%d", j == P[i] ? 1:0); printf("\n"); }
  printf("#LU (where 1. the diagonal of the matrix belongs to 'U', and 2. the\n"\
         "#diagonal elements of 'L' are not printed, because they are all 1):\n");
  print_matrix(A);
#endif

  /* Inverting the matrix based on the LUP decomposed A. The inverse is returned through
   * the matrix 'A' itself. */
  if(LUPinverse(N+1, P, A, B, X, Y) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#FIRST and SECOND FD coeff matrix inversion successful.\n#Inverse of A:\n");
  print_matrix(A);
#endif

  for(int i=1;i<=N;i++) { 
    coeffFIRST [i-1] = A[i][2];
    coeffSECOND[i-1] = 2.0*A[i][3]; // In general, must multiply Nth derivative by N!
  }
  
  //Next compute UPWINDed coeffs:
  for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
    A[i][j] = A1[i][j] = 1.0;
  }
  for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
      double rs = (N-1.0)/2.0;
      A[i][j] = A1[i][j] = pow((double)j - rs - 0.0, i-1.0); // <- j - rs = index offset for UPWINDed stencils
    }
  if(LUPdecompose(N+1, A, P) < 0) return -1;
  if(LUPinverse(N+1, P, A, B, X, Y) < 0) return -1;
  for(int i=1;i<=N;i++) { 
    coeffUPWIND [i-1] = A[i][2];
  }

#ifdef DEBUGMATRIX
  printf("#UPWIND FD coeff matrix inversion successful.\n#Inverse of A:\n");
  print_matrix(A);
#endif
  return 0;
}

/*
 * This code is designed to set up efficient finite difference 
 *    derivative stencils for each BSSN gridfunction.
 * The inputs are: 
 *  1) finite difference order (integer)
 *  2) whether to enable upwinding (boolean)
 * The output is text containing:
 *  1) An array for each gridfunction that ensure no gridpoint 
 *        is read from main memory more than once (reading from
 *        main memory is a very expensive operation, often
 *        exceeding the computational cost of evaluating the
 *        BSSN RHSs)
 *  2) "const"-type finite difference coefficient & stencil arrays
 */

int main(int argc, char *argv[]) {

  if(argc < 3) {
    printf("At least 2 arguments are required. Proper syntax is\n");
    printf("./FDcode_generator_arb_order <gridfunction name> <list of deriv operators.>\n\n");
    printf("Supported operators include: ZERO, FIRST, SECOND, and UPDOWNWIND\n");
    exit(1);
  }

  if(DIM!=3) { printf("Sorry, non-3D grids currently unsupported!\n"); exit(1); }

  char gf_prefix[100];

  sprintf(gf_prefix,"%s",argv[1]);
  /* Now determine how many unique gridfunctions correspond to this prefix.
   * E.g., gammabarDD would have 6.
   */
  int count=0; int first_idx=0;
  for(int i=0;i<NUM_GFS;i++) {
    if(strcmp(gf_name[i*2],gf_prefix) == 0) {
      if(count==0) first_idx = i;
      count++;
    }
  }

  if(count==0) { printf("Could not find gridfunction named %s. Exiting.\n",gf_prefix); exit(1); }

  // Compute all possible FD stencils.
  double coeffFIRST    [(ORDER)+1];
  double coeffSECOND   [(ORDER)+1];
  double coeffUPWIND   [(ORDER)+1];
  int retval = compute_finite_difference_coeffs(coeffFIRST,coeffSECOND,coeffUPWIND);
  if(retval !=0 ) {printf("Error in FD coefficient matrix solve\n"); exit(1); }

  for(int gf=first_idx;gf<first_idx+count;gf++) {
    char deriv_type[100];
    for(int ii=2;ii<argc;ii++) {
      sprintf(deriv_type,"%s",argv[ii]);
#ifdef SHIFTADVECT
#else
      if(strcmp(gf_prefix,"betaU")==0 && strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif
#ifdef BIADVECT
#else
      if(strcmp(gf_prefix,"BU"   )==0 && strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif
#ifdef UPWIND
#else
      if(strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif

      /* Output format:
       * Column | Data
       *    1   | gridfunction number
       *   2-4  | index (i,j,k), respectively
       *    5   | FD coefficient
       *    6   | number of points in this FD derivative
       *    7   | gridfunction/derivative name
       */
      if       (strcmp(deriv_type,"ZERO")==0) {
        
        printf("%d %d %d %d %.16e %d %s%s gplist\n",gf, 0,0,0, 1.0, 1, gf_prefix,gf_name[gf*2+1]);

      } else if(strcmp(deriv_type,"FIRST")==0) {

        // Symmetric first derivatives always have coefficient zero if (i,j,k) = (0,0,0)
        for(int i=0;i<ORDER+1;i++) {
          if(i != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, i-ORDER/2,0,0, coeffFIRST[i]/del[0], (ORDER), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<ORDER+1;j++) {
          if(j != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, 0,j-ORDER/2,0, coeffFIRST[j]/del[1], (ORDER), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<ORDER+1;k++) {
          if(k != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, 0,0,k-ORDER/2, coeffFIRST[k]/del[2], (ORDER), gf_prefix,gf_name[gf*2+1],2); }

      } else if(strcmp(deriv_type,"SECOND")==0) {

        for(int i=0;i<ORDER+1;i++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,0,0, coeffSECOND[i]/del[0]/del[0], (ORDER+1), gf_prefix,gf_name[gf*2+1], 0,0);
        for(int j=0;j<ORDER+1;j++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,j-ORDER/2,0, coeffSECOND[j]/del[1]/del[1], (ORDER+1), gf_prefix,gf_name[gf*2+1], 1,1);
        for(int k=0;k<ORDER+1;k++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,0,k-ORDER/2, coeffSECOND[k]/del[2]/del[2], (ORDER+1), gf_prefix,gf_name[gf*2+1], 2,2);

        /* Now mixed second-order derivs: */
        // First count the number of terms with nonzero coeffs
        int mix_2deriv_size = 0;
        for(int i=0;i<ORDER+1;i++) for(int j=0;j<ORDER+1;j++) {
            if(i != ORDER/2 && j != ORDER/2) mix_2deriv_size++;
          }

        // second derivative wrt x1, then x2 (or vice-versa):
        for(int i=0;i<ORDER+1;i++) for(int j=0;j<ORDER+1;j++) {
            if(i != ORDER/2 && j != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,j-ORDER/2,0, coeffFIRST[i]*coeffFIRST[j]/del[0]/del[1], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 0,1); }
          }

        // second derivative wrt x1, then x3 (or vice-versa):
        for(int i=0;i<ORDER+1;i++) for(int k=0;k<ORDER+1;k++) {
            if(i != ORDER/2 && k != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,0,k-ORDER/2, coeffFIRST[i]*coeffFIRST[k]/del[0]/del[2], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 0,2); }
          }

        // second derivative wrt x2, then x3 (or vice-versa):
        for(int j=0;j<ORDER+1;j++) for(int k=0;k<ORDER+1;k++) {
            if(j != ORDER/2 && k != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,j-ORDER/2,k-ORDER/2, coeffFIRST[j]*coeffFIRST[k]/del[1]/del[2], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 1,2); }
          }
      } else if(strcmp(deriv_type,"UPDOWNWIND")==0) {
        for(int i=0;i<ORDER+1;i++) {
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, i-ORDER/2+1,0,0, coeffUPWIND[i]/del[0], (ORDER+1), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<ORDER+1;j++) {
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, 0,j-ORDER/2+1,0, coeffUPWIND[j]/del[1], (ORDER+1), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<ORDER+1;k++) {
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, 0,0,k-ORDER/2+1, coeffUPWIND[k]/del[2], (ORDER+1), gf_prefix,gf_name[gf*2+1],2); }

        for(int i=0;i<ORDER+1;i++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, i-ORDER/2-1,0,0, -coeffUPWIND[ORDER-i]/del[0], (ORDER+1), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<ORDER+1;j++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, 0,j-ORDER/2-1,0, -coeffUPWIND[ORDER-j]/del[1], (ORDER+1), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<ORDER+1;k++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, 0,0,k-ORDER/2-1, -coeffUPWIND[ORDER-k]/del[2], (ORDER+1), gf_prefix,gf_name[gf*2+1],2); }
      } else if(strcmp(deriv_type,"IGNORE")==0) {
        /* Do nothing */
      } else {
        printf("ERROR: Unknown derivative type: %s gplist\n",deriv_type);
      }
    }
  }
  return 0;
}
