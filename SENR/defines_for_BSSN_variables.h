#ifndef DEFINES_FOR_BSSN_VARIABLES_H__
#define DEFINES_FOR_BSSN_VARIABLES_H__

/* DON'T TOUCH THIS! Only DIM=3 supported at the moment. TODO: Support DIM!=3 as well. */
#define DIM 3

#include "parameters_struct_def-NRPyGEN.h"

#include "gridfunction_defines-NRPyGEN.h"
// Re-use BSSN gridfunctions to store temporary gridfunctions needed for analysis.
#define KOSTRNGH  7 // Kreiss-Oliger strength
#define HAM       8 // Hamiltonian constraint
#define NORMHAM   9 // "Normalized" Hamiltonian constraint
#define NULLEXP  10 // Expansion of null geodesics
#define CC1      11 // Conformal connection coefficient constraints
#define CC2      12
#define CC3      13
#define MOM1     14 // Momentum constraint
#define MOM2     15
#define MOM3     16
#define NUM_AUX_GFS 17
/* Auxiliary grid functions */

#endif /* DEFINES_FOR_BSSN_VARIABLES_H__ */
