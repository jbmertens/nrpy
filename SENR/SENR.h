#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <time.h>

// Allocation and free macros for grid functions

#include "loop_setup.h" // Located in ../common_functions/
#include "defines_for_BSSN_variables.h"
#include "input_parameters.h"
// Declare the struct datatype for precomputed hatted (reference-metric-related) quantities.
#include "reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h" // Located in ../common_functions/

// Boundary condition function declarations
#define BSSN_EVOLUTION // <- Needed so that vector and tensor parity conditions are properly applied. E.g., in spherical polar coordinates r->-r, theta->pi+dtheta, etc.
#include "boundary_conditions/boundary_condition_include.h" // Located in ../common_functions/

#include "metric_inverse_and_determinant.c"
void Compute_Ricci_pt1(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params);
void Compute_Ricci_pt2(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params);
#include "diagnostics/Constraints.c"

void RK4_RHS_pt1(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);
void RK4_RHS_pt2(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);
void RK4_RHS_pt3(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);
void RK4_RHS_pt4(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);
void RK4_RHS_pt5(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params);

#include "evolution_equations/Evaluate_RHS.c"

#ifdef FIND_HORIZON
#include "diagnostics/Horizon_Finder.c"
#endif

// Find the minimum grid spacing so we can set timestep
#include "Find_Time_Step.c" // Located in ../common_functions/

// Set initial data
#include "initial_data/Set_Initial_Data.c"

#ifdef EVALUATE_ADM_INTEGRALS
#include "diagnostics/ADM_Integrals.c"
#endif

// Add interpolation routines.
#ifdef ENABLE_SPHERICAL_TO_CARTESIAN_ETK_LAYER
#include "interpolate/interpolate.c" // Located in ../common_functions/
#endif

// Diagnostics
#include "diagnostics/Grid_Function_Reduction.c" // Located in ../common_functions/
#include "diagnostics/Data_File_Output.c" // Depends on diagnostics/Grid_Function_Reduction.c. Located in ../common_functions/
#include "Checkpoint.c"  // Located in ../common_functions/
#include "NaN_Checker.c" // Located in ../common_functions/
