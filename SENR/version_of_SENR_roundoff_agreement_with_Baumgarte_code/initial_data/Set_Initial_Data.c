void Set_Initial_Data(double **xx, double **yy, double **in_gfs, double **gfs_aux)
{
  //LOOP_NOGZFILL(ii, jj, kk)
  LOOP_GZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);
      const double t = 0.0;
      const double x1 = xx[0][ii];
      const double x2 = xx[1][jj];
      const double x3 = xx[2][kk];

      double var[NUM_GFS], auxvar[NUM_AUX_GFS];

      {
#if defined EVOLVE_SCALAR_WAVE
#include "Scalar_Wave_uv.c"
#elif defined EVOLVE_BSSN
#include "UIUC_metric_curv_shift.c"
#endif
      }

      for(int gf = 0; gf < NUM_GFS; gf++)
	{
	  in_gfs[gf][idx] = var[gf];
	}

      {
#if defined EVOLVE_SCALAR_WAVE
#include "Scalar_Wave_aux.c"
#elif defined EVOLVE_BSSN
#include "BSSN_aux.c"
#endif
      }

      for(int gf = 0; gf < NUM_AUX_GFS; gf++)
	{
	  gfs_aux[gf][idx] = auxvar[gf];
	}	
    }

  return;
}

// Exact solution used for outer boundary conditions
void Exact_soln(const double t, const double x1, const double x2, const double x3, double *soln)
{
  double var[NUM_GFS];

#if defined EVOLVE_SCALAR_WAVE
#include "Scalar_Wave_uv.c"
#elif defined EVOLVE_BSSN
#include "UIUC_metric_curv_shift.c"
#endif

  for(int gf = 0; gf < NUM_GFS; gf++)
    {
      soln[gf] = var[gf];
    }

  return;
}

#if defined ID_FD_LAMB
// Finite difference the lambdaU initial data
void Set_Initial_Data_FD_LAMB(double **xx, double **yy, double **in_gfs)
{
  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);
      const double x1 = xx[0][ii];
      const double x2 = xx[1][jj];
      const double x3 = xx[2][kk];

      // Declare array that stores the gridfunction data on the stencils
      double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.
      
      // Declare local variable arrays
      double hDD[DIM][DIM], hDDdD[DIM][DIM][DIM];
      double detg;

      /* Inputs for autogenFDcode/ code generation. 
	 Note that UPDOWNWIND is contingent on UPWIND being
	 #define'd, and betU/betaU's UPDOWNWIND are contingent
	 on SHIFTADVECT & BIADVECT being #define'd, respectively.

	 To generate all finite difference code for this file,
	 simply go to the autogenFDcode directory and run
	 ./gen_stencil [this filename]

	 DVGENSTART Initial_Data_FD_LAMB.h
	 hDD      	ZERO	FIRST   
	 detg   	ZERO
	 DVGENEND
      */

#include "../autogenFDcode/Initial_Data_FD_LAMB.h"

      //double gammabarDD[DIM][DIM];

      //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);

      double gammabarUU[DIM][DIM];
      //double oneoverdetgammabar = 1.0 / detgammabar;
      //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

      Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

      double lambdaUs[DIM];

#include "../autogenMathcode/lambdaUs.txt-parsed.h"

      // Finite difference lambdaU
      int which_gf = 0;
      for(int gf = LAMB1; gf <= LAMB3; gf++) { in_gfs[gf][idx] = lambdaUs[which_gf]; which_gf++; }
    }
}
#endif

void Set_Random_Initial_Data(double **yy, double **in_gfs, double **gfs_aux)
{
  const double pert_mag = 2.0e-1;

  // Random number generator seeder is not OMP safe
  for(int kk = 0; kk < Npts[2]; kk++)
    {
      for(int jj = 0; jj < Npts[1]; jj++)
	{
	  for(int ii = 0; ii < Npts[0]; ii++)
	    {
	      const int idx = GFIDX(ii, jj, kk);
	      srand48(1e6 * ii + 1e3 * jj + kk);

	      in_gfs[H11][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[H12][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[H13][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[H22][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[H23][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[H33][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[PHI][idx] = fabs((2.0 * drand48() - 1.0) * pert_mag);
	      in_gfs[ALPHA][idx] = 1.0 - drand48() * pert_mag;
	      in_gfs[TRK][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A11][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A12][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A13][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A22][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A23][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[A33][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[LAMB1][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[LAMB2][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[LAMB3][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[VET1][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[VET2][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[VET3][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[BET1][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[BET2][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	      in_gfs[BET3][idx] = (2.0 * drand48() - 1.0) * pert_mag;
	    }
	}
    }

  return;
}
