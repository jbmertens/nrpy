#!/bin/bash

for i in $(ls *.txt -1 | sed -e 's/\..*$//')
do
cat $i.txt | sed "s/\\\/LINEBREAK/g" | tr -d "\n" | sed "s/;/;\n/g;s/LINEBREAK//g;" | awk '{print "const double "$0}' > parsed.txt

sed -i "s/Sech(/1.0\/cosh(/g;s/Erf(/erf(/g;s/Erfc(/erfc(/g;s/Sqrt(/sqrt(/g;s/pow(\([a-zA-Z0-9]*\),1.5)/sqrt(\1*\1*\1)/g;s/pow(\([a-zA-Z0-9]*\),-2.)/(1.0\/(\1*\1))/g;s/pow(\([a-zA-Z0-9]*\),-1.5)/(1.0\/sqrt(\1*\1*\1))/g;s/pow(\([a-zA-Z0-9]*\),3.)/(\1*\1*\1)/g;s/pow(\([a-zA-Z0-9]*\),-3.)/1.\/(\1*\1*\1)/g;s/pow(\([a-zA-Z0-9]*\),4.)/(\1*\1*\1*\1)/g;s/pow(\([a-zA-Z0-9]*\),-4.)/1.\/(\1*\1*\1*\1)/g;s/abs/fabs/g;s/Complex(0.,\([-0-9]*\.[0-9]*\))/\1*I/g;s/Complex(\([-0-9]*\.[0-9]*\),\([-0-9]*\.[0-9]*\))/(\1+\2*I)/g;s/Derivative(\([0-9]\))(S\([A-Z]\))(theta)/S\2_d\1/g;s/S\([A-Z]\)(theta)/S\1/g;s/=/ = /g;" parsed.txt

sed -i "s/Derivative(0,1)(f2)(A,B)/f2_B(A, B)/g;s/Derivative(0,1)(f1)(A,B)/f1_B(A, B)/g;s/Derivative(1,0)(f2)(A,B)/f2_A(A, B)/g;s/Derivative(1,0)(f1)(A,B)/f1_A(A, B)/g;s/Derivative(0,2)(f2)(A,B)/f2_BB(A, B)/g;s/Derivative(0,2)(f1)(A,B)/f1_BB(A, B)/g;s/Derivative(2,0)(f2)(A,B)/f2_AA(A, B)/g;s/Derivative(2,0)(f1)(A,B)/f1_AA(A, B)/g;s/Derivative(1,1)(f2)(A,B)/f2_AB(A, B)/g;s/Derivative(1,1)(f1)(A,B)/f1_AB(A, B)/g;s/f2(A,B)/f2(A, B)/g;s/f1(A,B)/f1(A, B)/g;" parsed.txt

sed -i "s/const double var/var/g;s/const double auxvar/auxvar/g;" parsed.txt

cat parsed.txt > $i.c

rm parsed.txt
rm $i.txt
done
