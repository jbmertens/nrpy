#ifndef LOOP_SETUP_H__
#define LOOP_SETUP_H__

/* Base gridfunction index. Note that this must be consistent with LOOP_GZFILL and LOOP_NOGZFILL below, to ensure we are not cache-missing ourselves to death. */
#define GFIDX(i,j,k) ( (i) + Npts[0] * ( (j) + Npts[1] * (k) ) )

/* Main Loops */
#define LOOP_GZFILL(i,j,k) _Pragma("omp parallel for")		\
  for(int k=0;k<Npts[2];k++) for(int j=0;j<Npts[1];j++) for(int i=0;i<Npts[0];i++)
#define LOOP_NOGZFILL(i,j,k) _Pragma("omp parallel for")		\
  for(int k=NGHOSTS;k<Npts[2]-NGHOSTS;k++) for(int j=NGHOSTS;j<Npts[1]-NGHOSTS;j++) for(int i=NGHOSTS;i<Npts[0]-NGHOSTS;i++)


/* Declare Rank-zero through Rank-four indexed quantities, and then initialize all components to zero. */
#define I0(f) double f=0.0;
#define I1(f) double f[DIM];                for(int i=0;i<DIM;i++) { f[i] = 0.0; }
#define I2(f) double f[DIM][DIM];           for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) { f[i][j] = 0.0; }
#define I3(f) double f[DIM][DIM][DIM];      for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) for(int k=0;k<DIM;k++) { f[i][j][k] = 0.0; }
#define I4(f) double f[DIM][DIM][DIM][DIM]; for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) for(int k=0;k<DIM;k++) for(int l=0;l<DIM;l++) { f[i][j][k][l] = 0.0; }

/* Declare Rank-zero through Rank-four indexed quantities. NI = do not initialize. */
#define I0NI(f) double f;
#define I1NI(f) double f[DIM];
#define I2NI(f) double f[DIM][DIM];
#define I3NI(f) double f[DIM][DIM][DIM];
#define I4NI(f) double f[DIM][DIM][DIM][DIM];

/* Basic loops. Use for implied sums, etc. */
#define F1(i        ) for(int i=0;i<DIM;i++)
#define F2(i,j      ) for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) 
#define F3(i,j,k    ) for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) for(int k=0;k<DIM;k++) 
#define F4(i,j,k,l  ) for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) for(int k=0;k<DIM;k++) for(int l=0;l<DIM;l++)
#define F5(i,j,k,l,m) for(int i=0;i<DIM;i++) for(int j=0;j<DIM;j++) for(int k=0;k<DIM;k++) for(int l=0;l<DIM;l++) for(int m=0;m<DIM;m++)

/* Basic loops for implied sums, etc. Impose indexed-quantity symmetry. */
#define F2s12(i,j) for(int i=0;i<DIM;i++) for(int j=i;j<DIM;j++) 

/* Loops for imposing symmetries of indexed quantities. */
/* Here, SS2s12 means "Set Symmetry: Rank-2 indexed quantity, apply symmetry to first and second indices */
#define SS2s12(f) for(int i_=0;i_<DIM;i_++) for(int j_=i_;j_<DIM;j_++) f[j_][i_] = f[i_][j_];
#define SS3s12(f) for(int i_=0;i_<DIM;i_++) for(int j_=i_;j_<DIM;j_++) for(int k_=0;k_<DIM;k_++) f[j_][i_][k_] = f[i_][j_][k_];
#define SS3s23(f) for(int i_=0;i_<DIM;i_++) for(int j_=0;j_<DIM;j_++) for(int k_=j_;k_<DIM;k_++) f[i_][k_][j_] = f[i_][j_][k_];
#define SS4s12(f) for(int i_=0;i_<DIM;i_++) for(int j_=i_;j_<DIM;j_++) for(int k_=0;k_<DIM;k_++) for(int l_=0;l_<DIM;l_++) f[j_][i_][k_][l_] = f[i_][j_][k_][l_];
#define SS4s34(f) for(int i_=0;i_<DIM;i_++) for(int j_=i_;j_<DIM;j_++) for(int k_=0;k_<DIM;k_++) for(int l_=k_;l_<DIM;l_++) f[i_][j_][l_][k_] = f[i_][j_][k_][l_];




#endif // LOOP_SETUP_H__
