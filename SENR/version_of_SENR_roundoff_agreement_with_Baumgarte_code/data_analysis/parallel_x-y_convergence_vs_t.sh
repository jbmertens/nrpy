#!/usr/local/bin/parallel --shebang-wrap -v A={} /usr/bin/env gnuplot

reset

IND = system("echo $A")
ind = IND + 0
file1 = "../output/out_2D_N128_FD6_CFL0.4000.txt"

xmax = 5
dt = 3.125000e-02
omega = 0.0
tc = 2 * xmax
t = ind * dt

set terminal png enhanced size 800,800
unset key

set output sprintf("frames/frame%.5d.png", ind)
set multiplot title sprintf("t / t_{c} = %.1f", t / tc)

set xlabel ""
set ylabel ""
set pm3d map
set parametric
set size square
unset border
unset xtics
unset ytics
unset grid
set xrange [-xmax:xmax]
set yrange [-xmax:xmax]

set tmargin at screen 0.96
set bmargin at screen 0.53
set lmargin at screen 0.01
set rmargin at screen 0.46
set pal positive color
set colorbox horizontal user origin 0.035,0.51 size 0.4,0.02
set cbrange [0.0:1.0]
set format cb "%.1f"
set cbtics 0.0,0.2,1.0
set cblabel "u" offset 0,0.6,0
splot file1 i ind u ($5):($6):($7), \
      file1 i ind u ($5):($6):(0) every 1 w p lc 5

set tmargin at screen 0.96
set bmargin at screen 0.53
set lmargin at screen 0.51
set rmargin at screen 0.99
set colorbox horizontal user origin 0.55,0.51 size 0.4,0.02
set cbrange [-16:0]
set format cb "%.0f"
set cbtics -16,2,0
set cblabel "log_{10}(E_{rel})" offset 0,0.6,0
splot file1 i ind u ($5):($6):(log10(abs($9))), \
      file1 i ind u ($5):($6):(0) every 8 w p lc 5

xC(x, y) = x * cos(omega * t) + y * sin(omega * t)
yC(x, y) = -x * sin(omega * t) + y * cos(omega * t)

set tmargin at screen 0.45
set bmargin at screen 0.01
set lmargin at screen 0.00
set rmargin at screen 0.49
#set pal positive color
#set colorbox horizontal user origin 0.023,0.1 size 0.455,0.04
unset colorbox
set cbrange [0.0:1.0]
set xrange [0.0:2.0]
set yrange [0.0:2.0]
#set format cb "%.1f"
#set cbtics -0.3,0.1,0.3
#set cblabel "u" offset 0,0.6,0
splot file1 i ind u (xC($5, $6)):(yC($5, $6)):($7), \
      file1 i ind u (xC($5, $6)):(yC($5, $6)):(0) every 1 w p lc 5

set tmargin at screen 0.45
set bmargin at screen 0.01
set lmargin at screen 0.51
set rmargin at screen 0.99
#set colorbox horizontal user origin 0.525,0.1 size 0.455,0.04
unset colorbox
set cbrange [-16:0]
set xrange [0.0:2.0]
set yrange [0.0:2.0]
#set format cb "%.0f"
#set cbtics -5,1,0
#set cblabel "log_{10}(E_{rel})" offset 0,0.6,0
splot file1 i ind u (xC($5, $6)):(yC($5, $6)):(log10(abs($9))), \
      file1 i ind u (xC($5, $6)):(yC($5, $6)):(0) every 1 w p lc 5

unset multiplot

#yes | ffmpeg -i frames/frame%05d.png movie.gif
