%\documentclass[twocolumn,showpacs,aps,prd,nobibnotes,floatfix]{revtex4-1}
\documentclass[11pt]{article}
%\documentclass[aps,prd]{revtex4-1}
\usepackage[hmargin=1.5cm,vmargin=1.5cm,bindingoffset=0.0cm]{geometry}
%\usepackage[hmargin=1.2cm,vmargin=1.2cm,bindingoffset=0.0cm]{geometry}

\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{epsf}
\usepackage{longtable}
\usepackage{graphics,epsfig,placeins,subfigure,wrapfig}
\usepackage[usenames]{color}
\usepackage{amsmath}
\usepackage{soul}
\newcommand{\beq}{\begin{equation*}}
\newcommand{\eeq}{\end{equation*}}
\newcommand{\beqn}{\begin{eqnarray*}}
\newcommand{\eeqn}{\end{eqnarray*}}
\newcommand{\ve}[1]{\mbox{\boldmath $#1$}}
%\renewcommand*{\arraystretch}{2}
\linespread{0.935}

% Turns off section numbering:
\setcounter{secnumdepth}{0} % <-- Turns off section numbering
\begin{document}
\title{\vspace{-1cm}Notes on arbitrary-order finite difference stencils}
\author{Zachariah B.~Etienne}
\date{}
\maketitle
\vspace{-0.5cm}

As an illustration let's first compute for uniform grids the
first-order, finite-difference coefficients accurate to
fourth-order in $\Delta x$:
\beq
\left[\partial_x u(x,t,...)\right]_j = \frac{1}{\Delta x}
\left(
-\frac{1}{12} (u_{j+2} - u_{j-2}) 
+ \frac{2}{3}  (u_{j+1} - u_{j-1}) \right)
+ \mathcal{O}((\Delta x)^4).
\eeq

SOLUTION:

Expanding neighboring points by their Taylor series approximation, we
get
\beqn
u_{j-2} &=& u_j - (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j - \frac{(2 \Delta x)^3}{3!} u'''_j + \frac{(2 \Delta x)^4}{4!} u^{(4)}_j +\mathcal{O}((\Delta x)^5)\\
u_{j-1} &=& u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j + \frac{(\Delta x)^4}{4!} u^{(4)}_j +\mathcal{O}((\Delta x)^5)\\
u_{j} &=& u_j \\
u_{j+1} &=& u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j + \frac{(\Delta x)^4}{4!} u^{(4)}_j +\mathcal{O}((\Delta x)^5)\\
u_{j+2} &=& u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j + \frac{(2 \Delta x)^4}{4!} u^{(4)}_j +\mathcal{O}((\Delta x)^5)\\
\eeqn

Our goal is to compute coefficients $a_j$ such that
$(a_{j-2} u_{j-2} + a_{j-1} u_{j-1}...)/(\Delta x) = u'_j +
\mathcal{O}((\Delta x)^4)$. So we get

\beqn
&& (a_{j-2} u_{j-2} + a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} +a_{j+2} u_{j+2})/(\Delta x) \\
= && \left( u_j - (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j -\frac{(2 \Delta x)^3}{3!} u'''_j+\frac{(2 \Delta x)^4}{4!} u^{(4)}_j \right) a_{j-2} \\
&& + \left( u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j-1} \\
&& + \left( u_j \right) a_{j} \\
&& + \left( u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j+1} \\
&& + \left( u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j+\frac{(2 \Delta x)^4}{4!} u^{(4)}_j \right) a_{j+2}
\eeqn
First notice that each time we take a derivative in the Taylor
expansion, we multiply by a $\Delta x$. Notice that this helps to keep
the units consistent (e.g., if $x$ were in units of meters). Let's
just agree to absorb those $\Delta x$'s into the derivatives and next
rearrange terms:
\beqn
&& a_{j-2} u_{j-2} + a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} + a_{j+2} u_{j+2} \\
= && \left( a_{j-2} + a_{j-1} + a_j + a_{j+1} + a_{j+2} \right) u_j \\
&& + \left( -2 a_{j-2} - a_{j-1} + a_{j+1} + 2 a_{j+2} \right) u'_j \\
&& + \left( 2^2 a_{j-2} + a_{j-1} + a_{j+1} + 2^2 a_{j+2} \right)/2! u''_j \\
&& + \left( -2^3 a_{j-2} - a_{j-1} + a_{j+1} + 2^3 a_{j+2} \right)/3! u'''_j \\
= && u'_j
\eeqn

In order for the above to hold true for any values 
$\left\{ u_j,u'_j,u''_j,u'''_j,u^{(4)}_j\right\}$, the following set of equations must also
hold:
\beqn
0 &=& a_{j-2} + a_{j-1} + a_j + a_{j+1} + a_{j+2}\\
1 &=& -2 a_{j-2} - a_{j-1} + a_{j+1} + 2 a_{j+2}\\
0 \times 2! &=& 2^2 a_{j-2} + a_{j-1} + a_{j+1} + 2^2 a_{j+2}\\
0 \times 3! &=& -2^3 a_{j-2} - a_{j-1} + a_{j+1} + 2^3 a_{j+2} \\
0 \times 4! &=& 2^4 a_{j-2} + a_{j-1} + a_{j+1} + 2^3 a_{j+2}
\eeqn

Notice that for any first derivative, the factorial does not come into
play. However, for second or third derivatives, it will play a role.

Now we write this expression in matrix form:
\beq
\left(
\begin{array}{c}
0 \\
1 \\
0\times 2! \\
0\times 3! \\
0\times 4! \\
\end{array}
\right)
=
\left(
\begin{array}{ccccc}
 1 &  1 & 1 & 1 & 1 \\
(-2)^1 &(-1)^1 & 0 & 1 & 2 \\
(-2)^2 &(-1)^2 & 0 & 1 & 2^2 \\
(-2)^3 &(-1)^3 & 0 & 1 & 2^3 \\
(-2)^4 &(-1)^4 & 0 & 1 & 2^4 \\
\end{array}
\right)
\left(
\begin{array}{c}
a_{j-2} \\
a_{j-1} \\
a_{j} \\
a_{j+1} \\
a_{j+2} \\
\end{array}
\right)
\eeq

So we have reduced the computation of finite difference coefficients
to the simple inversion of a matrix equation. Notice that the elements
of the matrix will vary from the one given above if the grid spacing
is not constant, but are otherwise invariant to $\Delta x$.

The inverted matrix reads
\beq
\left(
\begin{array}{ccccc}
0 & 1/12 & -1/24 & -1/12 & 1/24 \\
0 & -2/3 & 2/3 & 1/6 & -1/6 \\
1 & 0 & -5/4 & 0 & 1/4 \\
0 & 2/3 & 2/3 & -1/6 & -1/6 \\
0 & -1/12 & -1/24 & 1/12 & 1/24 \\
\end{array}
\right)
\eeq

The coefficients for the $N$th derivative can be immediately read by
multiplying the $(N+1)$st column by $N!$. In short, this matrix gives
the highest-order FD derivative coefficients for a stencil size of 5
gridpoints. It can be shown by analyzing cancellations within high
order terms of the Taylor series that the first \&
second derivative coefficients are correct to $(\Delta x)^4$ and third
\& fourth derivatives are correct to $(\Delta x)^2$.

Next we concern ourselves with crossed second derivatives, like
$\partial_{xy}$:

\beqn
\partial_y (\partial_x u)_{i,j} &=& \partial_y \left(a_{i-2} u_{i-2,j} +
a_{i-1} u_{i-1,j} + a_{i+1} u_{i+1,j} + a_{i+2} u_{i+2,j}\right) \\
&=& \sum_{n,m} a_{i+n} a_{i+m} u_{i+n,j+m},
\eeqn
where the sum is over all but $n=0$ and $m=0$.

The pattern for higher or lower order derivatives should be
straightforward.

\section{Upwind \& Downwind Derivatives}

Let's try first upwind derivatives. 
Expanding neighboring points by their Taylor series approximation, we
get
\beqn
u_{j-1} &=& u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4)\\
u_{j} &=& u_j \\
u_{j+1} &=& u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4)\\
u_{j+2} &=& u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4).
\eeqn
Our goal is to compute coefficients $a_j$ such that
$(a_{j-1} u_{j-1} + a_{j} u_{j}+...)/(\Delta x) = u'_j +\mathcal{O}((\Delta x)^3)$. 

So we get
\beqn
&& \frac{a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} +a_{j+2} u_{j+2}}{\Delta x} \\
= && + \left( u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j-1} \\
&& + \left( u_j \right) a_{j} \\
&& + \left( u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j+1} \\
&& + \left( u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j+\frac{(2 \Delta x)^4}{4!} u^{(4)}_j \right) a_{j+2}
\eeqn

First notice that each time we take a derivative in the Taylor
expansion, we multiply by a $\Delta x$. Notice that this helps to keep
the units consistent (e.g., if $x$ were in units of meters). Let's
just agree to absorb those $\Delta x$'s into the derivatives and next
rearrange terms:
\beqn
&& a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} + a_{j+2} u_{j+2} \\
= && \left(+a_{j-1} + 1*a_j + a_{j+1} + 2^0*a_{j+2} \right) u_j \\
  && \left(-a_{j-1} + 0*a_j + a_{j+1} + 2^1*a_{j+2} \right) u_j'/1! \\
  && \left(+a_{j-1} + 0*a_j + a_{j+1} + 2^2*a_{j+2} \right) u_j''/2! \\
  && \left(-a_{j-1} + 0*a_j + a_{j+1} + 2^3*a_{j+2} \right) u_j'''/3! \\
= && u'_j
\eeqn

In order for the above to hold true for any values 
$\left\{ u_j,u'_j,u''_j,u'''_j,u^{(4)}_j\right\}$, the following set of equations must also
hold:
\beqn
0 &=& (-1)^0*a_{j-1} + 1*a_j + a_{j+1} + a_{j+2}\\
1 &=& (-1)^1*a_{j-1} + 0*a_j + a_{j+1} + 2 a_{j+2}\\
0 \times 2! &=& (-1)^2*a_{j-1} + 0*a_j + a_{j+1} + 2^2 a_{j+2}\\
0 \times 3! &=& (-1)^3*a_{j-1} + 0*a_j + a_{j+1} + 2^3 a_{j+2} \\
\eeqn

In matrix form, this becomes ({\bf NOTE THAT WE SET $0^0=1$ FOR
  CONVENIENCE OF NOTATION}):
\beq
\left(
\begin{array}{c}
0 \\
1 \\
0\times 2! \\
0\times 3! \\
\end{array}
\right)
=
\left(
\begin{array}{cccc}
(-1)^0 & 0^0 & (1)^0 & (2)^0 \\
(-1)^1 & 0^1 & (1)^1 & (2)^1 \\
(-1)^2 & 0^2 & (1)^2 & (2)^2 \\
(-1)^3 & 0^3 & (1)^3 & (2)^3 \\
\end{array}
\right)
\left(
\begin{array}{c}
a_{j-1} \\
a_{j} \\
a_{j+1} \\
a_{j+2} \\
\end{array}
\right)
\eeq

\end{document}
