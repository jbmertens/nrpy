#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "../SENR.h"
#include "../grid_setup.h"

#if defined EVOLVE_SCALAR_WAVE
#include "../Scalar_Wave__grid_and_gridfunction_setup.h"
#elif defined EVOLVE_BSSN
#include "../BSSN__grid_and_gridfunction_setup.h"
#endif
#include "../loop_setup.h"

// Include coordinate-specific grid step sizes del[DIM]
#if defined COORD_CARTESIAN
#include "../boundary_conditions/coord_cartesian.c"
#elif defined COORD_CYLINDRICAL
#include "../boundary_conditions/coord_cylindrical.c"
#elif defined COORD_SPHERICAL
#include "../boundary_conditions/coord_spherical.c"
#elif defined COORD_SYMTP
#include "../boundary_conditions/coord_symtp.c"
#endif

#include "../initial_data/Set_Initial_Data.c"

//#define N ((ORDER)+1) //Size of the to-be-inverted N x N square matrix 'A'.
#include "matrix_inverse.h"

// Sets which_coeffs_we_want
const int UPDOWNWIND=-1;
const int ZERO  = 0;
const int FIRST = 1;
const int SECOND= 2;
const int KREISSOLIGER= 1000;

// Enabling DEBUGMATRIX will cause code to exit after matrix inversion
//#define DEBUGMATRIX

double factorial(int nn) {
  int result=1;
  for(int i=1;i<=nn;i++) result*=i;
  return (double)result;
}

#define PRINT_MATRIX(NN,MATR)                                           \
  for(int i_ = 1; i_ <= NN; i_++) {                                     \
    for(int j_ = 1; j_ <= NN; j_++) printf("\t%E", (float)MATR[i_][j_]); \
    printf("\n"); }                                                     \

int compute_finite_difference_coeffs(const int order, double *coeffs, int which_coeffs_we_want) {
  /* 
     Computing finite difference coefficients of
     "order" order requires inversion of an
     N x N matrix, where N = order+1:
   */
  const int N=order+1;

  // Set up matrix for computing centered finite difference coefficients.
  // NOTE THAT THIS IS A ONE-OFFSET MATRIX, NOT ZERO-OFFSET, LIKE MOST C ARRAYS.
  /* 'A' is the to-be-inverted matrix. A1 is its copy, which is used to calculate
   * 'I = inverse(A).A1'. Ideally, 'I' should be a perfect identity matrix. 'I' is
   * used to check the quality of the calculated inverse. */
  double A[N+1][N+1];
  double A1[N+1][N+1];
  //double I[N+1][N+1];

  /* Its i-th row shows the position of '1' in the i-th row of the pivot that is used
   * when performing the LUP decomposition of A. The rest of the elements in that row of
   * the pivot would be zero. In this program, we call this array 'P' a 'permutation'. */
  int P[N+1];

  /* This function calculates inverse of the matrix A. It accepts the LUP decomposed
   * matrix through 'LU' and the corresponding pivot through 'P'. The inverse is
   * returned through 'LU' itself. The 'B' matrix and 'X' & 'Y' vectors are used for
   * temporary storage. This function is defined after the function 'LUPdecompose()'.
   * * */
  double B[N+1][N+1]; //Temporary storage.
  double X[N+1]; //Temporary storage.
  double Y[N+1]; //Temporary storage.

  /* Define the to-be-inverted matrix, A. 
     We define A row-by-row, according to the prescription 
     derived in notes/notes.pdf, via the following pattern
     that applies for arbitrary order.
     
     As an example, consider a 5-point finite difference 
     stencil (4th-order accurate), where we wish to compute 
     some derivative at the center point. 

     Then A is given by:

     2^0  1^0  1  1^0   2^0
     2^1  1^1  0  1^1   2^1
     2^2  1^2  0  1^2   2^2
     2^3  1^3  0  1^3   2^3
     2^4  1^4  0  1^4   2^4

     Then multiplying A^{-1} 
     by (1 0 0 0 0)^T will yield 0th deriv. stencil
     by (0 1 0 0 0)^T will yield 1st deriv. stencil
     by (0 0 1 0 0)^T will yield 2nd deriv. stencil
     etc.

     Next suppose we want an upwinded, 4th-order accurate
     stencil. For this case, A is given by:

     1^0  1  1^0   2^0   3^0
     1^1  0  1^1   2^1   3^1
     1^2  0  1^2   2^2   3^2
     1^3  0  1^3   2^3   3^3
     1^4  0  1^4   2^4   3^4

     ... and similarly for the downwinded derivative.

     Finally, let's consider a 3rd-order accurate
     stencil. This would correspond to an in-place
     upwind stencil with stencil radius of 2 gridpoints,
     where other, centered derivatives are 4th-order
     accurate. For this case, A is given by:

     1^0  1  1^0   2^0
     1^1  0  1^1   2^1
     1^2  0  1^2   2^2
     1^3  0  1^3   2^3
     1^4  0  1^4   2^4

     ... and similarly for the downwinded derivative.

     The general pattern is as follows:

     1) The top row is all 1's,
     2) If the second row has N elements (N must be odd),
     .... then the radius of the stencil is rs = (N-1)/2
     .... and the j'th row e_j = j-rs-1. For example,
     .... for 4th order, we have rs = 2
     .... j  | element
     .... 1  | -2
     .... 2  | -1
     .... 3  |  0
     .... 4  |  1
     .... 5  |  2
     3) The L'th row, L>2 will be the same as the second 
     .... row, but with each element e_j -> e_j^(L-1)
     A1 is used later to validate the inverted
     matrix. */

  if(which_coeffs_we_want!=UPDOWNWIND) {
    // Set up centered matrix elements
    /* Row 1: */
    for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
        A[i][j] = A1[i][j] = 1.0;
      }
    /* Rows 2--N: */
    for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
        int rs = (N-1)/2; // rs = radius of stencil
        A[i][j] = A1[i][j] = pow((double)j - (double)rs - 1.0, (double)i-1.0);
      }
  } else if(which_coeffs_we_want==UPDOWNWIND) {
    // Set up upwinded matrix elements
    /* Row 1: */
    for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
        A[i][j] = A1[i][j] = 1.0;
      }
    /* Rows 2--N: */
    for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
        int rs = (N-1)/2;
#ifdef UPWINDINPLACE
        rs += 1;
#endif
        A[i][j] = A1[i][j] = pow((double)j - (double)rs - 0.0, (double)i-1.0); // <- j - rs = index offset for UPDOWNWINDed stencils
      }
  }

#ifdef DEBUGMATRIX
  printf("#The to-be-inverted matrix 'A':\n"); PRINT_MATRIX(N,A);
#endif

  /* Performing LUP-decomposition of the matrix 'A'. If successful, the 'U' is stored in
   * its upper diagonal, and the 'L' is stored in the remaining traigular space. Note that
   * all the diagonal elements of 'L' are 1, which are not stored. */
  if(LUPdecompose(N+1, A, P) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#The LUP decomposition of 'A' is successful.\n#Pivot:\n");
  for(int i = 1; i <= N; i++) { for(int j = 1; j <= N; j++) printf("\t%d", j == P[i] ? 1:0); printf("\n"); }
  printf("#LU (where 1. the diagonal of the matrix belongs to 'U', and 2. the\n"\
         "#diagonal elements of 'L' are not printed, because they are all 1):\n");
  PRINT_MATRIX(N,A);
#endif

  /* Inverting the matrix based on the LUP decomposed A. The inverse is returned through
   * the matrix 'A' itself. */
  if(LUPinverse(N+1, P, A, B, X, Y) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#FD coeff matrix inversion successful.\n#Inverse of A:\n");
  PRINT_MATRIX(N,A);
#endif

  if(which_coeffs_we_want==UPDOWNWIND) { for(int i=1;i<=N;i++) coeffs[i-1] = 1.0*A[i][2]; } // In general, must multiply Nth derivative by N!
  if(which_coeffs_we_want==FIRST)  { for(int i=1;i<=N;i++) coeffs[i-1] = 1.0*A[i][2]; } // In general, must multiply Nth derivative by N!
  if(which_coeffs_we_want==SECOND) { for(int i=1;i<=N;i++) coeffs[i-1] = 2.0*A[i][3]; } // In general, must multiply Nth derivative by N!
  // Highest-order derivative possible with given stencil size = 2*(radius of stencil) = order.
  if(which_coeffs_we_want==KREISSOLIGER) { for(int i=1;i<=N;i++) coeffs[i-1] =factorial(order)*A[i][order+1]; } // In general, must multiply Nth derivative by N!

  return 0;
}

/*
 * This code is designed to set up efficient finite difference 
 *    derivative stencils for each BSSN gridfunction.
 * The inputs are: 
 *  1) finite difference order (integer)
 *  2) whether to enable upwinding (boolean)
 * The output is text containing:
 *  1) An array for each gridfunction that ensure no gridpoint 
 *        is read from main memory more than once (reading from
 *        main memory is a very expensive operation, often
 *        exceeding the computational cost of evaluating the
 *        BSSN RHSs)
 *  2) "const"-type finite difference coefficient & stencil arrays
 */

int main(int argc, char *argv[]) {

  if(argc < 3) {
    printf("At least 2 arguments are required. Proper syntax is\n");
    printf("./FDcode_generator_arb_order <gridfunction name> <list of deriv operators.>\n\n");
    printf("Supported operators include: ZERO, FIRST, SECOND, and UPDOWNWIND\n");
    exit(1);
  }

  if(DIM!=3) { printf("Sorry, non-3D grids currently unsupported!\n"); exit(1); }

  char gf_prefix[100];

  sprintf(gf_prefix,"%s",argv[1]);
  /* Now determine how many unique gridfunctions correspond to this prefix.
   * E.g., gammabarDD would have 6.
   */
  int count=0; int first_idx=0;
  for(int i=0;i<NUM_GFS;i++) {
    if(strcmp(gf_name[i*2],gf_prefix) == 0) {
      if(count==0) first_idx = i;
      count++;
    }
  }

  if(count==0) { printf("Could not find gridfunction named %s. Exiting.\n",gf_prefix); exit(1); }

  // Compute all possible FD stencils.
  double coeffs    [(ORDER)+1];

  for(int gf=first_idx;gf<first_idx+count;gf++) {
    char deriv_type[100];
    for(int ii=2;ii<argc;ii++) {
      sprintf(deriv_type,"%s",argv[ii]);
#ifdef SHIFTADVECT
#else
      if(strcmp(gf_prefix,"vetU")==0 && strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif
#ifdef BIADVECT
#else
      if(strcmp(gf_prefix,"betU")==0 && strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif
#ifdef UPWIND
#else
      if(strcmp(deriv_type,"UPDOWNWIND")==0) sprintf(deriv_type,"IGNORE");
#endif

      /* Output format:
       * Column | Data
       *    1   | gridfunction number
       *   2-4  | index (i,j,k), respectively
       *    5   | FD coefficient
       *    6   | number of points in this FD derivative
       *    7   | gridfunction/derivative name
       */
      if       (strcmp(deriv_type,"ZERO")==0) {
        
        printf("%d %d %d %d %.16e %d %s%s gplist\n",gf, 0,0,0, 1.0, 1, gf_prefix,gf_name[gf*2+1]);

      } else if(strcmp(deriv_type,"FIRST")==0) {
        if(compute_finite_difference_coeffs(ORDER, coeffs, FIRST) != 0) {printf("Error in FD coefficient matrix solve\n"); exit(1); }

        // Symmetric first derivatives always have coefficient zero if (i,j,k) = (0,0,0)
        for(int i=0;i<ORDER+1;i++) {
          if(i != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, i-ORDER/2,0,0, coeffs[i]/del[0], (ORDER), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<ORDER+1;j++) {
          if(j != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, 0,j-ORDER/2,0, coeffs[j]/del[1], (ORDER), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<ORDER+1;k++) {
          if(k != ORDER/2) printf("%d %d %d %d %.16e %d %sdD%s[%d] gplist\n",gf, 0,0,k-ORDER/2, coeffs[k]/del[2], (ORDER), gf_prefix,gf_name[gf*2+1],2); }

      } else if(strcmp(deriv_type,"SECOND")==0) {
        if(compute_finite_difference_coeffs(ORDER, coeffs, SECOND) != 0) {printf("Error in FD coefficient matrix solve\n"); exit(1); }

        for(int i=0;i<ORDER+1;i++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,0,0, coeffs[i]/del[0]/del[0], (ORDER+1), gf_prefix,gf_name[gf*2+1], 0,0);
        for(int j=0;j<ORDER+1;j++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,j-ORDER/2,0, coeffs[j]/del[1]/del[1], (ORDER+1), gf_prefix,gf_name[gf*2+1], 1,1);
        for(int k=0;k<ORDER+1;k++)
          printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,0,k-ORDER/2, coeffs[k]/del[2]/del[2], (ORDER+1), gf_prefix,gf_name[gf*2+1], 2,2);

        /* Now mixed second-order derivs: */
        // compute first-order derivative FD coeffs; mixed second-order derivs are just combinations of first-order derivs:
        if(compute_finite_difference_coeffs(ORDER, coeffs, FIRST) != 0) {printf("Error in FD coefficient matrix solve\n"); exit(1); }
        // First count the number of terms with nonzero coeffs
        int mix_2deriv_size = 0;
        for(int i=0;i<ORDER+1;i++) for(int j=0;j<ORDER+1;j++) {
            if(i != ORDER/2 && j != ORDER/2) mix_2deriv_size++;
          }

        // second derivative wrt x1, then x2 (or vice-versa):
        for(int i=0;i<ORDER+1;i++) for(int j=0;j<ORDER+1;j++) {
            if(i != ORDER/2 && j != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,j-ORDER/2,0, coeffs[i]*coeffs[j]/del[0]/del[1], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 0,1); }
          }

        // second derivative wrt x1, then x3 (or vice-versa):
        for(int i=0;i<ORDER+1;i++) for(int k=0;k<ORDER+1;k++) {
            if(i != ORDER/2 && k != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, i-ORDER/2,0,k-ORDER/2, coeffs[i]*coeffs[k]/del[0]/del[2], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 0,2); }
          }

        // second derivative wrt x2, then x3 (or vice-versa):
        for(int j=0;j<ORDER+1;j++) for(int k=0;k<ORDER+1;k++) {
            if(j != ORDER/2 && k != ORDER/2) {
              printf("%d %d %d %d %.16e %d %sdDD%s[%d][%d] gplist\n",gf, 0,j-ORDER/2,k-ORDER/2, coeffs[j]*coeffs[k]/del[1]/del[2], 
                     mix_2deriv_size, gf_prefix,gf_name[gf*2+1], 1,2); }
          }
      } else if(strcmp(deriv_type,"UPDOWNWIND")==0) {
#ifdef UPWINDINPLACE
        const int order_we_want = ORDER-1;
#else
        const int order_we_want = ORDER;
#endif
        if(compute_finite_difference_coeffs(order_we_want, coeffs, UPDOWNWIND) != 0) {printf("Error in FD coefficient matrix solve\n"); exit(1); }
        for(int i=0;i<order_we_want+1;i++) {
          int varied_idx=i-(order_we_want/2)+1;
          if(order_we_want%2!=0) varied_idx--; // Fix an off-by-one issue in odd finite difference order upwindings, due to the rounding-down of order/2.
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, varied_idx,0,0, coeffs[i]/del[0], (order_we_want+1), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<order_we_want+1;j++) {
          int varied_idx=j-(order_we_want/2)+1;
          if(order_we_want%2!=0) varied_idx--; // Fix an off-by-one issue in odd finite difference order upwindings, due to the rounding-down of order/2.
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, 0,varied_idx,0, coeffs[j]/del[1], (order_we_want+1), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<order_we_want+1;k++) {
          int varied_idx=k-(order_we_want/2)+1;
          if(order_we_want%2!=0) varied_idx--; // Fix an off-by-one issue in odd finite difference order upwindings, due to the rounding-down of order/2.
          printf("%d %d %d %d %.16e %d %sdupD%s[%d] gplist\n",gf, 0,0,varied_idx, coeffs[k]/del[2], (order_we_want+1), gf_prefix,gf_name[gf*2+1],2); }

        for(int i=0;i<order_we_want+1;i++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, i-order_we_want/2-1,0,0, -coeffs[order_we_want-i]/del[0], (order_we_want+1), gf_prefix,gf_name[gf*2+1],0); }
        for(int j=0;j<order_we_want+1;j++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, 0,j-order_we_want/2-1,0, -coeffs[order_we_want-j]/del[1], (order_we_want+1), gf_prefix,gf_name[gf*2+1],1); }
        for(int k=0;k<order_we_want+1;k++) {
          printf("%d %d %d %d %.16e %d %sddnD%s[%d] gplist\n",gf, 0,0,k-order_we_want/2-1, -coeffs[order_we_want-k]/del[2], (order_we_want+1), gf_prefix,gf_name[gf*2+1],2); }
      } else if(strcmp(deriv_type,"IGNORE")==0) {
        /* Do nothing */
      } else {
        printf("ERROR: Unknown derivative type: %s gplist\n",deriv_type); exit(1);
      }
    }
  }
  return 0;
}
