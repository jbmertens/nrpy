// Cartesian coordinates (x, y, z) = (yy[0], yy[1], yy[2])
#define XMAX  5.0
#define XMIN -5.0
#define YMAX  5.0
#define YMIN -5.0
#define ZMAX  5.0
#define ZMIN -5.0
const double del[DIM] = {(XMAX - XMIN) / Nx1, (YMAX - YMIN) / Nx2, (ZMAX - ZMIN) / Nx3};
const double slope[DIM] = {XMAX - XMIN, YMAX - YMIN, ZMAX - ZMIN};
const double intercept[DIM] = {XMIN, YMIN, ZMIN};

#define REFERENCE_METRIC {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}
#define RESCALING_MATRIX {{1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}}
#define RESCALING_VECTOR {1.0, 1.0, 1.0}

// xmin < x(x1) < xmax
double y1_of_t_x(const double t, const double x1, const double x2, const double x3) { return x1; }
// ymin < y(x2) < ymax
double y2_of_t_x(const double t, const double x1, const double x2, const double x3) { return x2; }

// zmin < z(x3) < zmax
double y3_of_t_x(const double t, const double x1, const double x2, const double x3) { return x3; }

// Apply boundary conditions
void Apply_out_sym_bcs(const int gf, const double t, double **xx, double **yy, double **in_gfs)
{
#if defined OUTER_BOUNDARY_EXACT
  // Set upper and lower x=constant faces of the cube
  for(int k = 0; k < Npts[2]; k++) for(int j = 0; j < Npts[1]; j++) {
      for(int i = 0; i < NGHOSTS; i++) { /* lower x-face */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) { /* upper x-face */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
    }

  /* Set upper and lower y=constant faces of the cube. 
   *   Note we've already updated the upper and lower 
   *   x=constant faces, so do not update them again. */
  for(int k = 0; k < Npts[2]; k++) for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) {
      for(int j = 0; j < NGHOSTS; j++) { /* lower y-face. */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
      for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) { /* upper y-face */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
    }

  /* Set upper and lower z=constant faces of the cube. 
   *   Note we've already updated the upper and lower 
   *   x=constant and y=constant faces, so don't update them again */
  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++) {
      for(int k = 0; k < NGHOSTS; k++) { /* lower z-face. */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
      for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) { /* upper z-face. */
        double soln[2];Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // u = soln[0], v = soln[1]
        in_gfs[gf][GFIDX(i, j, k)] = soln[gf];
      }
    }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
  /* LINEAR EXTRAPOLATION OUTER BOUNDARY CONDITIONS */

  /* Set upper and lower x=constant faces of the cube.
   * We do not include ghostzones in j and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i; /* lower x-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) { /* upper x-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
    }

  /* Set upper and lower y=constant faces of the cube.
   * We do not include ghostzones in i and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++) for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) {
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j; /* lower y-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(i, jj, k)] = 2.0 * in_gfs[gf][GFIDX(i, jj + 1, k)] - in_gfs[gf][GFIDX(i, jj + 2, k)];
      }
      for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) { /* upper y-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i, j - 1, k)] - in_gfs[gf][GFIDX(i, j - 2, k)];
      }
    }

  /* Set upper and lower z=constant faces of the cube.
   * We do not include ghostzones in i and j directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++) {
      for(int k = 0; k < NGHOSTS; k++) {
        const int kk = NGHOSTS - 1 - k; /* lower z-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(i, j, kk)] = 2.0 * in_gfs[gf][GFIDX(i, j, kk + 1)] - in_gfs[gf][GFIDX(i, j, kk + 2)];
      }
      for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) { /* upper z-face: start from the inside and work our way outward */
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i, j, k - 1)] - in_gfs[gf][GFIDX(i, j, k - 2)];
      }
    }

  /* Set the four lower z=constant edges of the cube. */
  for(int k = 0; k < NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
    }
    for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[gf][GFIDX(i, jj, k)] = 2.0 * in_gfs[gf][GFIDX(i, jj + 1, k)] - in_gfs[gf][GFIDX(i, jj + 2, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i, j - 1, k)] - in_gfs[gf][GFIDX(i, j - 2, k)];
      }
    }
  }

  /* Set the four upper z=constant edges of the cube. */
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) {
    for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
    }

    for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[gf][GFIDX(i, jj, k)] = 2.0 * in_gfs[gf][GFIDX(i, jj + 1, k)] - in_gfs[gf][GFIDX(i, jj + 2, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i, j - 1, k)] - in_gfs[gf][GFIDX(i, j - 2, k)];
      }
    }
  }

  /* Set remaining two edges on lower y=constant face of the cube. */
  for(int j = 0; j < NGHOSTS; j++) for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
    }

  /* Set remaining two edges on upper y=constant face of the cube. */
  for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
    }

  /* Set all that remains: the 8 corners of the cube */
  // Corner (0,0,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
  // Corner (0,0,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
  // Corner (0,1,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
  // Corner (0,1,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
  // Corner (1,0,0)
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
  // Corner (1,0,1)
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
  // Corner (1,1,0)
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[gf][GFIDX(ii, j, k)] = 2.0 * in_gfs[gf][GFIDX(ii + 1, j, k)] - in_gfs[gf][GFIDX(ii + 2, j, k)];
      }
  // Corner (1,1,1)
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++) for(int j = Npts[1] - NGHOSTS; j < Npts[1]; j++) for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++) {
        in_gfs[gf][GFIDX(i, j, k)] = 2.0 * in_gfs[gf][GFIDX(i - 1, j, k)] - in_gfs[gf][GFIDX(i - 2, j, k)];
      }
#endif
    
  return;
}

double Find_Time_Step(double **yy, double y_min[3], double y_max[3])
{
  const int idx = GFIDX(NGHOSTS, NGHOSTS, NGHOSTS);

  y_min[0] = yy[0][idx];
  y_min[1] = yy[1][idx];
  y_min[2] = yy[2][idx];
  y_max[0] = yy[0][GFIDX(Npts[0] - NGHOSTS - 1, NGHOSTS, NGHOSTS)];
  y_max[1] = yy[1][GFIDX(NGHOSTS, Npts[1] - NGHOSTS - 1, NGHOSTS)];
  y_max[2] = yy[2][GFIDX(NGHOSTS, NGHOSTS, Npts[2] - NGHOSTS - 1)];
  
  const double dx = fabs(yy[0][idx] - yy[0][GFIDX(NGHOSTS + 1, NGHOSTS, NGHOSTS)]);
  const double dy = fabs(yy[1][idx] - yy[1][GFIDX(NGHOSTS, NGHOSTS + 1, NGHOSTS)]);
  const double dz = fabs(yy[2][idx] - yy[2][GFIDX(NGHOSTS, NGHOSTS, NGHOSTS + 1)]);

  double dt = (dx < dy) ? dx : dy;
  dt = (dz < dt) ? dz : dt;
  
  return dt;
}
