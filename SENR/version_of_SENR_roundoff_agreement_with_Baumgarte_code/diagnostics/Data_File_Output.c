void Data_File_Output(FILE *out0D, FILE *out1D, FILE *out2D, FILE *out3D, const int frame_ind, const double t, double **xx, double **yy, double **gfs_n, double **gfs_aux)
{
  const int whichpt_x2 = Npts[1] / 2;
  const int whichpt_x3 = Npts[2] / 2;

#if defined EVOLVE_SCALAR_WAVE
  // Store the analytical solution gfs_aux[ANA]
  // Store the relative error gfs_aux[RELERR]
  LOOP_NOGZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);
      const double num = gfs_n[U][idx];
      double soln[2];
      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln);
      const double ana = soln[0];
      gfs_aux[ANA][idx] = ana;
      gfs_aux[RELERR][idx] = fabs(num / ana - 1.0);
    }

  const double L2_err = L2_Norm_Over_Entire_Grid(RELERR, gfs_aux);
  const double mean_log_err = MeanLog_Norm_Over_Entire_Grid(RELERR, gfs_aux);

  // Relative error at center and corners of grid
  const double center_rel_err     = gfs_aux[RELERR][GFIDX(Npts[0] / 2,           Npts[1] / 2,           Npts[2] / 2)];           // Center point
  const double corner_000_rel_err = gfs_aux[RELERR][GFIDX(NGHOSTS + 2,           NGHOSTS + 2,           NGHOSTS + 2)];           // Corner (0,0,0)
  const double corner_001_rel_err = gfs_aux[RELERR][GFIDX(Npts[0] - NGHOSTS - 2, NGHOSTS + 2,           NGHOSTS + 2)];           // Corner (0,0,1)
  const double corner_010_rel_err = gfs_aux[RELERR][GFIDX(NGHOSTS + 2,           Npts[1] - NGHOSTS - 2, NGHOSTS + 2)];           // Corner (0,1,0)
  const double corner_011_rel_err = gfs_aux[RELERR][GFIDX(Npts[0] - NGHOSTS - 2, Npts[1] - NGHOSTS - 2, NGHOSTS + 2)];           // Corner (0,1,1)
  const double corner_100_rel_err = gfs_aux[RELERR][GFIDX(NGHOSTS + 2,           NGHOSTS + 2,           Npts[2] - NGHOSTS - 2)]; // Corner (1,0,0)
  const double corner_101_rel_err = gfs_aux[RELERR][GFIDX(Npts[0] - NGHOSTS - 2, NGHOSTS + 2,           Npts[2] - NGHOSTS - 2)]; // Corner (1,0,1)
  const double corner_110_rel_err = gfs_aux[RELERR][GFIDX(NGHOSTS + 2,           Npts[1] - NGHOSTS - 2, Npts[2] - NGHOSTS - 2)]; // Corner (1,1,0)
  const double corner_111_rel_err = gfs_aux[RELERR][GFIDX(Npts[0] - NGHOSTS - 2, Npts[1] - NGHOSTS - 2, Npts[2] - NGHOSTS - 2)]; // Corner (1,1,1)

  fprintf(out0D, "%d %e %.16e %.16e %.16e %.16e %.16e %.16e %.16e %.16e %.16e %.16e %.16e\n", frame_ind, t, L2_err, mean_log_err, center_rel_err, corner_000_rel_err, corner_001_rel_err, corner_010_rel_err, corner_011_rel_err, corner_100_rel_err, corner_101_rel_err, corner_110_rel_err, corner_111_rel_err);

  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
    {
      const int idx = GFIDX(i, whichpt_x2, whichpt_x3);
      
      fprintf(out1D, "%d %e %e %e %e %.16e\n", frame_ind, t, yy[0][idx], gfs_n[U][idx], gfs_aux[ANA][idx], gfs_aux[RELERR][idx]);
    }

  fprintf(out1D, "\n\n");

  // Output top half of xy-plane
  for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
    { 
      for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	{
	  const int idx = GFIDX(i, j, NGHOSTS);

	  // WARNING: SymTP SPECIFIC
	  const double x = yy[0][idx];
	  const double y = yy[1][idx] * cos(yy[2][idx]);
		  
	  fprintf(out2D, "%d %e %d %d %e %e %e %e %.16e\n", frame_ind, t, i, j, x, y, gfs_n[U][idx], gfs_aux[ANA][idx], gfs_aux[RELERR][idx]);
	}
	      
      fprintf(out2D, "\n");
    }

  const int NGHOSTS_plus_pi = NGHOSTS + (NGHOSTS + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi

  // Output bottom half of xy-plane
  for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
    { 
      for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	{
	  const int idx = GFIDX(i, j, NGHOSTS_plus_pi);

	  // WARNING: SymTP SPECIFIC
	  const double x = yy[0][idx];
	  const double y = yy[1][idx] * cos(yy[2][idx]);

	  fprintf(out2D, "%d %e %d %d %e %e %e %e %.16e\n", frame_ind, t, i, j, x, y, gfs_n[U][idx], gfs_aux[ANA][idx], gfs_aux[RELERR][idx]);
	}
	      
      fprintf(out2D, "\n");
    }

  fprintf(out2D, "\n");

  // 3D output
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	    {
	      const int idx = GFIDX(i, j, k);

	      fprintf(out3D, "%d %e %d %d %d %e %e %e %e %e %.16e\n", frame_ind, t, i, j, k, yy[0][idx], yy[1][idx], yy[2][idx], gfs_n[U][idx], gfs_aux[ANA][idx], gfs_aux[RELERR][idx]);
	    }

	  fprintf(out3D, "\n");
	}

      //fprintf(out3D, "\n");
    }

  //fprintf(out3D, "\n");

  // END SCALAR WAVE OUTPUT
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#elif defined EVOLVE_BSSN
  // Evaluate Hamiltonian constraint gfs_aux[HAM]
  Hamiltonian_Constraint(xx, yy, gfs_n, gfs_aux);

  const double ham_L2 = L2_Norm_Over_Entire_Grid(HAM, gfs_aux);
  const double ham_RMS = RMS_Norm_Over_Entire_Grid(HAM, gfs_aux);
  //const double ham_sample = gfs_aux[HAM][GFIDX(3 * Npts[0] / 4, 3 * Npts[1] / 4, 3 * Npts[2] / 4)]; // yum
  //const double ham_corner = gfs_aux[HAM][GFIDX(NGHOSTS + 1, NGHOSTS + 1, NGHOSTS + 1)]; // corner
  const double phi_sample = gfs_n[PHI][GFIDX(Npts[0] / 2, Npts[1] / 2, Npts[2] / 2)];

  // 0D output
  fprintf(out0D, "%d %e ", frame_ind, t); // Time
  fprintf(out0D, "%.16e %.16e %.16e\n", ham_L2, ham_RMS, phi_sample); // Grid function data

  // 1D output
  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
    {
      const int idx = GFIDX(i, whichpt_x2, whichpt_x3); // Output the y1 axis
      //const int idx = GFIDX(i, i, i); // Output the y1 = y2 = y3 diagonal
      const double y1 = yy[0][idx];
      const double y2 = yy[1][idx];
      const double y3 = yy[2][idx];
      const double phi = gfs_n[PHI][idx];
      const double alpha = gfs_n[ALPHA][idx];
      const double K = gfs_n[TRK][idx];
      const double ham = gfs_aux[HAM][idx];
      
      const double gammahat[DIM][DIM] = REFERENCE_METRIC; // Reference metric macro in boundary_conditions/coord_*.c
      const double RM[DIM][DIM] = RESCALING_MATRIX; // Rescaling matrix macro in boundary_conditions/coord_*.c
      const double RV[DIM] = RESCALING_VECTOR; // Rescaling vector macro in boundary_conditions/coord_*.c

      double gt[DIM][DIM], At[DIM][DIM], beta[DIM]; // Conformal quantities to compare with KRANC BSSN

      int which_gf = H11;
      F2s12(ii, jj) { gt[ii][jj] = gammahat[ii][jj] + RM[ii][jj] * gfs_n[which_gf][idx]; which_gf++; }

      which_gf = A11;
      F2s12(ii, jj) { At[ii][jj] = RM[ii][jj] * gfs_n[which_gf][idx]; which_gf++; }

      which_gf = VET1;
      F1(ii) { beta[ii] = RV[ii] * gfs_n[which_gf][idx]; which_gf++; }

      fprintf(out1D, "%d %e %d %f %f %f ", frame_ind, t, i, y1, y2, y3); // Coordinate
      fprintf(out1D, "%.16e\n", ham); // Grid function data
      //fprintf(out1D, "%.16e %.16e %.16e\n", phi, alpha, K); // Grid function data
      //fprintf(out1D, "%.16e %.16e %.16e\n", gfs_n[LAMB1][idx], gfs_n[LAMB2][idx], gfs_n[LAMB3][idx]); // Grid function data
      //fprintf(out1D, "%.16e %.16e %.16e %.16e %.16e %.16e\n", gfs_n[VET1][idx], gfs_n[VET2][idx], gfs_n[VET3][idx], gfs_n[BET1][idx], gfs_n[BET2][idx], gfs_n[BET3][idx]);
      //fprintf(out1D, "%.16e %.16e %.16e %.16e %.16e %.16e\n", gfs_n[H11][idx], gfs_n[H12][idx], gfs_n[H13][idx], gfs_n[H22][idx], gfs_n[H23][idx], gfs_n[H33][idx]);
      //fprintf(out1D, "%.16e %.16e %.16e %.16e %.16e %.16e\n", gfs_n[A11][idx], gfs_n[A12][idx], gfs_n[A13][idx], gfs_n[A22][idx], gfs_n[A23][idx], gfs_n[A33][idx]);
    }

  fprintf(out1D, "\n\n");

  // 2D output
  for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
    {
      for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	{
	  const int idx = GFIDX(i, j, whichpt_x3);
	  const double y1 = yy[0][idx];
	  const double y2 = yy[1][idx];
	  const double phi = gfs_n[PHI][idx];
	  const double alpha = gfs_n[ALPHA][idx];
	  const double ham = gfs_aux[HAM][idx];

	  fprintf(out2D, "%d %e %d %d %f %f ", frame_ind, t, i, j, y1, y2); // Coordinates
	  fprintf(out2D, "%.16e %.16e %.16e\n", phi, alpha, ham); // Grid function data
	}

      fprintf(out2D, "\n");
    }

  fprintf(out2D, "\n");

  /*
  // 3D output
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	    {
	      const int idx = GFIDX(i, j, k);
	      const double y1 = yy[0][idx];
	      const double y2 = yy[1][idx];
	      const double y3 = yy[2][idx];
	      const double phi = gfs_n[PHI][idx];
	      const double alpha = gfs_n[ALPHA][idx];
	      const double ham = gfs_aux[HAM][idx];

	      const double gammahat[DIM][DIM] = REFERENCE_METRIC; // Reference metric macro in boundary_conditions/coord_*.c
	      const double RM[DIM][DIM] = RESCALING_MATRIX; // Rescaling matrix macro in boundary_conditions/coord_*.c
	      const double RV[DIM] = RESCALING_VECTOR; // Rescaling vector macro in boundary_conditions/coord_*.c

	      double gt[DIM][DIM], At[DIM][DIM], beta[DIM]; // Conformal quantities to compare with KRANC BSSN

	      int which_gf = H11;
	      F2s12(ii, jj) { gt[ii][jj] = gammahat[ii][jj] + RM[ii][jj] * gfs_n[which_gf][idx]; which_gf++; }

	      which_gf = A11;
	      F2s12(ii, jj) { At[ii][jj] = RM[ii][jj] * gfs_n[which_gf][idx]; which_gf++; }

	      which_gf = VET1;
	      F1(ii) { beta[ii] = RV[ii] * gfs_n[which_gf][idx]; which_gf++; }

	      fprintf(out3D, "%d %e %d %d %d %e %e %e ", frame_ind, t, i, j, k, y1, y2, y3); // Coordinates
	      //fprintf(out3D, "%.16e %.16e %.16e\n", phi, alpha, ham); // Grid function data

	      //fprintf(out3D, "%e %e %e %e %e %e\n", gt[0][0], gt[0][1], gt[0][2], gt[1][1], gt[1][2], gt[2][2]);
	      //fprintf(out3D, "%e %e %e %e %e %e\n", At[0][0], At[0][1], At[0][2], At[1][1], At[1][2], At[2][2]);
	      fprintf(out3D, "%.16e %.16e %.16e %.16e %.16e %.16e\n", gfs_n[H11][idx], gfs_n[H12][idx], gfs_n[H13][idx], gfs_n[H22][idx], gfs_n[H23][idx], gfs_n[H33][idx]);
	    }

	  fprintf(out3D, "\n");
	}

      //fprintf(out3D, "\n");
      }
  */

  //fprintf(out3D, "\n");
#endif

  return;
}
