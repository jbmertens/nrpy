double L2_Norm_Over_Entire_Grid(const int gf, double **in_gfs)
{
  double L2 = 0.0;

  //#pragma omp parallel for reduction(+:L2)
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	    {
	      const double val = in_gfs[gf][GFIDX(i, j, k)];
	      L2 += val * val;

	      //printf("%d %d %d %.15e\n", i, j, k, val);
	    }
	}
    }

  return sqrt(L2);
}

double RMS_Norm_Over_Entire_Grid(const int gf, double **in_gfs)
{
  // Number of grid points, not counting ghost zones
  const int Npts_RMS = Nx1 * Nx2 * Nx3;
  const double L2 = L2_Norm_Over_Entire_Grid(gf, in_gfs);

  return sqrt(L2 * L2 / Npts_RMS);
}

double MeanLog_Norm_Over_Entire_Grid(const int gf, double **in_gfs)
{
  // Number of grid points, not counting ghost zones
  const int Npts_meanlog = Nx1 * Nx2 * Nx3;

  double meanlog = 0.0;

#pragma omp parallel for reduction(+: meanlog)
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	    {
	      const double val = in_gfs[gf][GFIDX(i, j, k)];
	      meanlog += log10(val);
	    }
	}
    }

  return pow(10.0, meanlog / Npts_meanlog);
}
