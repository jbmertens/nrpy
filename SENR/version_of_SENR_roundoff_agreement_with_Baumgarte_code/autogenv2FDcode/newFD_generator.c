#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "matrix_inverse.h"

const int UPWIND=-1;
const int ZERO  = 0;
const int FIRST = 1;
const int SECOND= 2;
const int KREISSOLIGER= 1000;

// Enabling DEBUGMATRIX will cause code to exit after matrix inversion
//#define DEBUGMATRIX

double factorial(int nn) {
  int result=1;
  for(int i=1;i<=nn;i++) result*=i;
  return (double)result;
}

void print_matrix(const int N,double **matrix) {
  for(int i = 1; i <= N; i++) {
      for(int j = 1; j <= N; j++) printf("\t%E", (float)matrix[i][j]);
      printf("\n"); }
}


int compute_finite_difference_coeffs(const int order, double *coeffs, int which_coeffs_we_want) {
  /* 
     Computing finite difference coefficients of
     "order" order requires inversion of an
     N x N matrix, where N = order+1:
   */
  const int N=order+1;

  // Set up matrix for computing centered finite difference coefficients.
  // NOTE THAT THIS IS A ONE-OFFSET MATRIX, NOT ZERO-OFFSET, LIKE MOST C ARRAYS.
  /* 'A' is the to-be-inverted matrix. A1 is its copy, which is used to calculate
   * 'I = inverse(A).A1'. Ideally, 'I' should be a perfect identity matrix. 'I' is
   * used to check the quality of the calculated inverse. */
  double A[N+1][N+1];
  double A1[N+1][N+1];
  //double I[N+1][N+1];

  /* Its i-th row shows the position of '1' in the i-th row of the pivot that is used
   * when performing the LUP decomposition of A. The rest of the elements in that row of
   * the pivot would be zero. In this program, we call this array 'P' a 'permutation'. */
  int P[N+1];

  /* This function calculates inverse of the matrix A. It accepts the LUP decomposed
   * matrix through 'LU' and the corresponding pivot through 'P'. The inverse is
   * returned through 'LU' itself. The 'B' matrix and 'X' & 'Y' vectors are used for
   * temporary storage. This function is defined after the function 'LUPdecompose()'.
   * * */
  double B[N+1][N+1]; //Temporary storage.
  double X[N+1]; //Temporary storage.
  double Y[N+1]; //Temporary storage.

  /* Define the to-be-inverted matrix, A. 
     We define A row-by-row, according to the prescription 
     derived in notes/notes.pdf, via the following pattern
     that applies for arbitrary order.
     
     As an example, consider a 5-point finite difference 
     stencil (4th-order accurate), where we wish to compute 
     some derivative at the center point. 

     Then A is given by:

     2^0  1^0  1  1^0   2^0
     2^1  1^1  0  1^1   2^1
     2^2  1^2  0  1^2   2^2
     2^3  1^3  0  1^3   2^3
     2^4  1^4  0  1^4   2^4

     Then multiplying A^{-1} 
     by (1 0 0 0 0)^T will yield 0th deriv. stencil
     by (0 1 0 0 0)^T will yield 1st deriv. stencil
     by (0 0 1 0 0)^T will yield 2nd deriv. stencil
     etc.

     Next suppose we want an upwinded, 4th-order accurate
     stencil. For this case, A is given by:

     1^0  1  1^0   2^0   3^0
     1^1  0  1^1   2^1   3^1
     1^2  0  1^2   2^2   3^2
     1^3  0  1^3   2^3   3^3
     1^4  0  1^4   2^4   3^4

     ... and similarly for the downwinded derivative.

     Finally, let's consider a 3rd-order accurate
     stencil. This would correspond to an in-place
     upwind stencil with stencil radius of 2 gridpoints,
     where other, centered derivatives are 4th-order
     accurate. For this case, A is given by:

     1^0  1  1^0   2^0
     1^1  0  1^1   2^1
     1^2  0  1^2   2^2
     1^3  0  1^3   2^3
     1^4  0  1^4   2^4

     ... and similarly for the downwinded derivative.

     The general pattern is as follows:

     1) The top row is all 1's,
     2) If the second row has N elements (N must be odd),
     .... then the radius of the stencil is rs = (N-1)/2
     .... and the j'th row e_j = j-rs-1. For example,
     .... for 4th order, we have rs = 2
     .... j  | element
     .... 1  | -2
     .... 2  | -1
     .... 3  |  0
     .... 4  |  1
     .... 5  |  2
     3) The L'th row, L>2 will be the same as the second 
     .... row, but with each element e_j -> e_j^(L-1)
     A1 is used later to validate the inverted
     matrix. */

  if(which_coeffs_we_want!=UPWIND) {
    // Set up centered matrix elements
    /* Row 1: */
    for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
        A[i][j] = A1[i][j] = 1.0;
      }
    /* Rows 2--N: */
    for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
        double rs = (N-1.0)/2.0; // rs = radius of stencil
        A[i][j] = A1[i][j] = pow((double)j - rs - 1.0, (double)i-1.0);
      }
  } else if(which_coeffs_we_want==UPWIND) {
    // Set up upwinded matrix elements
    /* Row 1: */
    for(int i = 1; i <= 1; i++) for(int j = 1; j <= N; j++) {
        A[i][j] = A1[i][j] = 1.0;
      }
    /* Rows 2--N: */
    for(int i = 2; i <= N; i++) for(int j = 1; j <= N; j++) {
        double rs = (N-1.0)/2.0;
        A[i][j] = A1[i][j] = pow((double)j - rs - 0.0, (double)i-1.0); // <- j - rs = index offset for UPWINDed stencils
      }
  }

#ifdef DEBUGMATRIX
  printf("#The to-be-inverted matrix 'A':\n"); print_matrix(A);
#endif

  /* Performing LUP-decomposition of the matrix 'A'. If successful, the 'U' is stored in
   * its upper diagonal, and the 'L' is stored in the remaining traigular space. Note that
   * all the diagonal elements of 'L' are 1, which are not stored. */
  if(LUPdecompose(N+1, A, P) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#The LUP decomposition of 'A' is successful.\n#Pivot:\n");
  for(int i = 1; i <= N; i++) { for(int j = 1; j <= N; j++) printf("\t%d", j == P[i] ? 1:0); printf("\n"); }
  printf("#LU (where 1. the diagonal of the matrix belongs to 'U', and 2. the\n"\
         "#diagonal elements of 'L' are not printed, because they are all 1):\n");
  print_matrix(A);
#endif

  /* Inverting the matrix based on the LUP decomposed A. The inverse is returned through
   * the matrix 'A' itself. */
  if(LUPinverse(N+1, P, A, B, X, Y) < 0) return -1;
#ifdef DEBUGMATRIX
  printf("#FD coeff matrix inversion successful.\n#Inverse of A:\n");
  print_matrix(A);
#endif

  if(which_coeffs_we_want==UPWIND) { for(int i=1;i<=N;i++) coeffs[i-1] = 1.0*A[i][2]; } // In general, must multiply Nth derivative by N!
  if(which_coeffs_we_want==FIRST)  { for(int i=1;i<=N;i++) coeffs[i-1] = 1.0*A[i][2]; } // In general, must multiply Nth derivative by N!
  if(which_coeffs_we_want==SECOND) { for(int i=1;i<=N;i++) coeffs[i-1] = 2.0*A[i][3]; } // In general, must multiply Nth derivative by N!
  // Highest-order derivative possible with given stencil size = 2*(radius of stencil) = order.
  if(which_coeffs_we_want==KREISSOLIGER) { for(int i=1;i<=N;i++) coeffs[i-1] =factorial(order)*A[i][order+1]; } // In general, must multiply Nth derivative by N!

  return 0;
}

/*
 * This code computes finite difference derivative stencils
 * -= Input =-
 *  1) finite difference order (integer)
 *  2) which derivative? Supported list:
 *     ZERO, FIRST, UPWIND (first), & SECOND
 *
 * -= Output =-
 * Column | Data
 *   1-3  | index (i,j,k), respectively
 *    4   | FD coefficient
 *    5   | number of points in this FD derivative
 *    6+  | derivative with respect to which coords. E.g., 0 1 -> \partial_0\partial_1
 */

int main(int argc, char *argv[]) {

  if(argc != 3) {
    printf("2 arguments are required. Proper syntax is\n");
    printf("./newFD_generator <order> <what derivative? ZERO, FIRST, UPWIND (first), & SECOND supported>\n\n");
    exit(1);
  }

  const int order=atoi(argv[1]);
  if(order>100) {
    printf("Setting the finite differencing order > 100 may cause stack overflow issues. Override this if() statement in the if you're confident you know what you're doing. Exiting.\n");
    exit(1);
  }

  int which_coeffs_we_want=-1000;
  char deriv_type[100];
  snprintf(deriv_type,99,"%s",argv[2]);

  if(strcmp(deriv_type,"UPWIND")==0) which_coeffs_we_want=UPWIND;
  if(strcmp(deriv_type,"ZERO"  )==0) which_coeffs_we_want=ZERO;
  if(strcmp(deriv_type,"FIRST" )==0) which_coeffs_we_want=FIRST;
  if(strcmp(deriv_type,"SECOND")==0) which_coeffs_we_want=SECOND;
  if(strcmp(deriv_type,"KREISSOLIGER")==0) which_coeffs_we_want=KREISSOLIGER;
  //if(strcmp(deriv_type,"FOURTH")==0) which_coeffs_we_want=FOURTH;

  if(which_coeffs_we_want==-1000) {
    printf("Derivative operator %s not recognized. Exiting.\n",deriv_type); exit(1);
  }

  // Only UPWIND option supported for odd finite difference order:
  if(order%2!=0 && which_coeffs_we_want!=UPWIND) {
    printf("When using odd finite differencing order (you chose order=%d), only UPWIND option supported (you chose %s). Exiting.\n",order,deriv_type); exit(1);
  }

  // FD coefficients.
  double coeffs[(order)+1];

  /* Output format:
   * Column | Data
   *   1-3  | index (i,j,k), respectively
   *    4   | FD coefficient
   *    5   | number of points in this FD derivative
   *    6+  | derivative with respect to which coords. E.g., 0 1 -> \partial_0\partial_1
   */
  if(which_coeffs_we_want==ZERO) { printf("0 0 0 %.16e %d gplist\n", 1.0, 1); return 0; }

  int retval = compute_finite_difference_coeffs(order, coeffs,which_coeffs_we_want);
  if(retval !=0 ) {printf("Error in FD coefficient matrix solve\n"); exit(1); }

  if(which_coeffs_we_want==FIRST || which_coeffs_we_want==KREISSOLIGER) {
    for(int idx=0;idx<order+1;idx++) {
      int varied_idx = (idx) - (order/2);
      // Symmetric first derivatives always have coefficient zero if (i,j,k) = (0,0,0)
      if(varied_idx != 0 || which_coeffs_we_want==KREISSOLIGER) {
        printf("%d %d %d %.16e %d %s %d gplist\n",varied_idx,0,0, coeffs[idx], (order), deriv_type, 0);
        printf("%d %d %d %.16e %d %s %d gplist\n",0,varied_idx,0, coeffs[idx], (order), deriv_type, 1);
        printf("%d %d %d %.16e %d %s %d gplist\n",0,0,varied_idx, coeffs[idx], (order), deriv_type, 2);
      }
    }
  } else if(which_coeffs_we_want==SECOND) {
    /* First double second derivatives, e.g., \partial_x^2 */
    for(int idx=0;idx<order+1;idx++) {
      int varied_idx = (idx) - (order/2);
      printf("%d %d %d %.16e %d %s %d %d gplist\n",varied_idx,0,0, coeffs[idx], (order+1), deriv_type, 0,0);
      printf("%d %d %d %.16e %d %s %d %d gplist\n",0,varied_idx,0, coeffs[idx], (order+1), deriv_type, 1,1);
      printf("%d %d %d %.16e %d %s %d %d gplist\n",0,0,varied_idx, coeffs[idx], (order+1), deriv_type, 2,2);
    }

    /* Next compute mixed second-order derivs coeffs: */
    // First compute first-order finite difference coefficients.
    int retval = compute_finite_difference_coeffs(order, coeffs,FIRST);
    if(retval !=0 ) {printf("Error in FD coefficient matrix solve\n"); exit(1); }

    // Then count the number of terms with nonzero coeffs
    int mix_2deriv_size = 0;
    for(int idx1=0;idx1<order+1;idx1++) for(int idx2=0;idx2<order+1;idx2++) {
        // Symmetric first derivatives always have coefficient zero if (i,j,k) = (0,0,0)
        if(idx1 != order/2 && idx2 != order/2) mix_2deriv_size++;
      }

    for(int idx1=0;idx1<order+1;idx1++) for(int idx2=0;idx2<order+1;idx2++) {
        int varied_idx1=idx1-(order/2);
        int varied_idx2=idx2-(order/2);

        if(varied_idx1!=0 && varied_idx2!=0) {
          // second derivative wrt x1, then x2 (or vice-versa):
          printf("%d %d %d %.16e %d %s %d %d gplist\n",varied_idx1,varied_idx2,0, coeffs[idx1]*coeffs[idx2], mix_2deriv_size, deriv_type, 0,1); 
          // second derivative wrt x1, then x3 (or vice-versa):
          printf("%d %d %d %.16e %d %s %d %d gplist\n",varied_idx1,0,varied_idx2, coeffs[idx1]*coeffs[idx2], mix_2deriv_size, deriv_type, 0,2); 
          // second derivative wrt x2, then x3 (or vice-versa):
          printf("%d %d %d %.16e %d %s %d %d gplist\n",0,varied_idx1,varied_idx2, coeffs[idx1]*coeffs[idx2], mix_2deriv_size, deriv_type, 1,2); 
        }
      }
  } else if(which_coeffs_we_want==UPWIND) {
    // First upwinded derivatives:
    for(int idx=0;idx<order+1;idx++) {
      int varied_idx=idx-(order/2)+1;
      if(order%2!=0) varied_idx--; // Fix an off-by-one issue in odd finite difference order upwindings, due to the rounding-down of order/2.
      printf("%d %d %d %.16e %d UPWIND %d gplist\n",varied_idx,0,0, coeffs[idx], (order+1), 0);
      printf("%d %d %d %.16e %d UPWIND %d gplist\n",0,varied_idx,0, coeffs[idx], (order+1), 1);
      printf("%d %d %d %.16e %d UPWIND %d gplist\n",0,0,varied_idx, coeffs[idx], (order+1), 2);
    }
    /* Then downwinded derivatives, same as upwinded coeffs, but 
     * with a minus sign. See 
     * https://en.wikipedia.org/wiki/Finite_difference_coefficient for details */
    for(int idx=order;idx>=0;idx--) {
      int varied_idx=idx-(order/2)-1;
      printf("%d %d %d %.16e %d DNWIND %d gplist\n",varied_idx,0,0, -coeffs[order-idx], (order+1), 0);
      printf("%d %d %d %.16e %d DNWIND %d gplist\n",0,varied_idx,0, -coeffs[order-idx], (order+1), 1);
      printf("%d %d %d %.16e %d DNWIND %d gplist\n",0,0,varied_idx, -coeffs[order-idx], (order+1), 2);
    }
  }
  return 0;
}
