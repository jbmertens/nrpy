#ifndef METRIC_INVERSE_AND_DETERMINANT_H__
#define METRIC_INVERSE_AND_DETERMINANT_H__

double determinant(double gDD[DIM][DIM]) { 
  return 
    gDD[0][0] * gDD[1][1] * gDD[2][2] + 
    gDD[0][1] * gDD[1][2] * gDD[0][2] + 
    gDD[0][2] * gDD[0][1] * gDD[1][2] - 
    gDD[0][2] * gDD[1][1] * gDD[0][2] - 
    gDD[0][1] * gDD[0][1] * gDD[2][2] - 
    gDD[0][0] * gDD[1][2] * gDD[1][2];
}

#define Invertgammabar(gUU, gDD, oneoverg) {                             \
    gUU[0][0] =   ( gDD[1][1] * gDD[2][2] - gDD[1][2] * gDD[1][2] ) * oneoverg; \
    gUU[0][1] = - ( gDD[0][1] * gDD[2][2] - gDD[1][2] * gDD[0][2] ) * oneoverg; \
    gUU[0][2] =   ( gDD[0][1] * gDD[1][2] - gDD[1][1] * gDD[0][2] ) * oneoverg; \
    gUU[1][1] =   ( gDD[0][0] * gDD[2][2] - gDD[0][2] * gDD[0][2] ) * oneoverg; \
    gUU[1][2] = - ( gDD[0][0] * gDD[1][2] - gDD[0][1] * gDD[0][2] ) * oneoverg; \
    gUU[2][2] =   ( gDD[0][0] * gDD[1][1] - gDD[0][1] * gDD[0][1] ) * oneoverg; \
  }

void BSSN_gammabarDD(const int ii, const int jj, const int kk, double **in_gfs, double **yy, double gammabarDD[DIM][DIM])
{
  const int idx = GFIDX(ii, jj, kk);
  int which_gf = H11;
  const double gammahat[DIM][DIM] = REFERENCE_METRIC; // Reference metric macro in boundary_conditions/coord_*.c
  const double RM[DIM][DIM] = RESCALING_MATRIX; // Rescaling matrix macro in boundary_conditions/coord_*.c     
  F2s12(i,j) { gammabarDD[i][j] = gammahat[i][j] + RM[i][j] * in_gfs[which_gf][idx]; which_gf++; }

  return;
}

void BSSN_Det(double **in_gfs)
{
  LOOP_GZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);
      const double h11 = in_gfs[H11][idx] + 1.0;
      const double h12 = in_gfs[H12][idx];
      const double h13 = in_gfs[H13][idx];
      const double h22 = in_gfs[H22][idx] + 1.0;
      const double h23 = in_gfs[H23][idx];
      const double h33 = in_gfs[H33][idx] + 1.0;

      in_gfs[DETG][idx] = h11 * h22 * h33 + 2.0 * h13 * h12 * h23 - h11 * h23 * h23 - h12 * h12 * h33 - h13 * h22 * h13;
    }

  return;
}

void Compute_Inverse_Metric(const int ii, const int jj, const int kk, double **yy, double **in_gfs, double gammabarUU[DIM][DIM])
{
  const int idx = GFIDX(ii, jj, kk);
  const double RM[DIM][DIM] = RESCALING_MATRIX; // Rescaling matrix macro in boundary_conditions/coord_*.c 
  const double one_over_detg = 1.0 / in_gfs[DETG][idx];
  const double h11 = in_gfs[H11][idx] + 1.0;
  const double h12 = in_gfs[H12][idx];
  const double h13 = in_gfs[H13][idx];
  const double h22 = in_gfs[H22][idx] + 1.0;
  const double h23 = in_gfs[H23][idx];
  const double h33 = in_gfs[H33][idx] + 1.0;

  gammabarUU[0][0] = (h22 * h33 - h23 * h23) / RM[0][0] * one_over_detg;
  gammabarUU[0][1] = (h23 * h13 - h12 * h33) / RM[0][1] * one_over_detg;
  gammabarUU[0][2] = (h12 * h23 - h22 * h13) / RM[0][2] * one_over_detg;
  gammabarUU[1][1] = (h11 * h33 - h13 * h13) / RM[1][1] * one_over_detg;
  gammabarUU[1][2] = (h12 * h13 - h11 * h23) / RM[1][2] * one_over_detg;
  gammabarUU[2][2] = (h11 * h22 - h12 * h12) / RM[2][2] * one_over_detg;

  return;
}

// Rescale gammabarDD to "unit" determinant
void Rescale_Metric_Det(double **yy, double **in_gfs, double **gfs_aux)
{
  // Get contaminated det(gammabarDD)
  BSSN_Det(in_gfs);

  LOOP_GZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);

      // Scale factor to reset determinant to initial value INITDETG
      const double fac = cbrt(gfs_aux[INITDETG][idx] / in_gfs[DETG][idx]);

      // Kronecker delta
      const double KD[DIM][DIM] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

      // Scale out metric determinant
      int which_gf = H11;
      F2s12(i,j) { in_gfs[which_gf][idx] = fac * (KD[i][j] + in_gfs[which_gf][idx]) - KD[i][j]; which_gf++; }
    }

  // Recalculate determinant with cleaned metric
  BSSN_Det(in_gfs);

  return;
}

double Compute_Trace(const int ii, const int jj, const int kk, double **yy, double **in_gfs)
{
  const int idx = GFIDX(ii, jj, kk);

  // Compute inverse conformal metric
  //double gammabarDD[DIM][DIM];
  //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);
  double gammabarUU[DIM][DIM];
  //const double oneoverdetgammabar = 1.0 / in_gfs[DETG][idx];
  //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

  Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

  // Compute conformal trace-free extrinsic curvature
  double AbarDD[DIM][DIM];
  int which_gf = A11;
  const double RM[DIM][DIM] = RESCALING_MATRIX; // Rescaling matrix macro in boundary_conditions/coord_*.c     
  F2s12(i,j) { AbarDD[i][j] = RM[i][j] * in_gfs[which_gf][idx]; which_gf++; }

  // Residual trace
  const double trace_A = gammabarUU[0][0] * AbarDD[0][0] + gammabarUU[1][1] * AbarDD[1][1] + gammabarUU[2][2] * AbarDD[2][2] + 2.0 * (gammabarUU[0][1] * AbarDD[0][1] + gammabarUU[0][2] * AbarDD[0][2] + gammabarUU[1][2] * AbarDD[1][2]);

  return trace_A;
}

void Remove_Trace(double **yy, double **in_gfs)
{
  LOOP_GZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);

      // Residual trace
      const double trace_A = Compute_Trace(ii, jj, kk, yy, in_gfs) / 3.0;

      // Kronecker delta
      const double KD[DIM][DIM] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

      // Subtract out residual trace
      int which_gf = A11;
      int which_gf_2 = H11;
      F2s12(i,j) { in_gfs[which_gf][idx] -= (KD[i][j] + in_gfs[which_gf_2][idx]) * trace_A; which_gf++; which_gf_2++; }
    }

  return;
}

#endif // METRIC_INVERSE_AND_DETERMINANT_H__
