#ifndef BSSNL1_C__
#define BSSNL1_C__

//#include "../loop_setup.h"
//#include "../BSSN__grid_and_gridfunction_setup.h"

void BSSN_L1_rhs(double **in_gfs, double **xx, double **yy, double **rhs_gfs) {
  /*
    At each iteration of our main loop (below), we must read in each 
    BSSN variable gridfunction at point ii,jj,kk AND MANY NEIGHBORING POINTS
    (so that we may evaluate the finite difference derivatives).
    Geometrically, the neighboring points includes the intersection of
    three planes centered at the point where the derivatives must be computed.
    
    Reading in the gridfunction data from main memory is a very expensive operation,
    as every cache miss can waste ~tens to hundreds of CPU cycles.
    (see http://igoro.com/archive/gallery-of-processor-cache-effects/ for beautiful
    illustrations and analysis.) So our top goal should be to NEVER REPEAT reading
    a gridfunction from main memory at a given ii,jj,kk point.

    Most BSSN gridfunctions need only read in data for first spatial derivatives, but
    the 3-metric, lapse, shift, & conformal factor need second derivatives. The finite 
    difference (FD) stencils for all DIM first derivatives look like DIM intersecting 
    line segments, intersecting at the point from where the deriv is computed.
    Meanwhile, the FD stencils for the six independent second derivatives look like 
    DIM-1 intersecting planes, again centered at the point where the deriv is computed.

    Thus for gridfunctions requiring only first derivatives' computation, we need to 
    read in (2*NGHOSTS)*NDIM + 1 points (the +1 might be needed for upwinded derivs)

    And for gridfunctions for which second derivatives need to be taken, we need 
    for 1D:
    (2*NGHOSTS+1)
    for 2D:
    (2*NGHOSTS+1)*(2*NGHOSTS+1)
    for 3D (there's a line: 
    (2*NGHOSTS+1)*(2*NGHOSTS+1) + (2*NGHOSTS)*(2*NGHOSTS)

    // Generate a string of gridfunction indices to prevent any gridpoint to be pulled from main memory more than once.
    */

  //Start the timer, for benchmarking
  //struct timeval start, end;
  //long mtime, seconds, useconds;
  //gettimeofday(&start, NULL);

  LOOP_NOGZFILL(ii,jj,kk) {
    const int idx = GFIDX(ii, jj, kk);

    const double x1 = xx[0][ii];
    const double x2 = xx[1][jj];
    const double x3 = xx[2][kk];

    /***************************************/
    /**** DECLARE LOCAL VARIABLE ARRAYS ****/
    /***************************************/
    // Declare array that stores the gridfunction data on the stencils
    double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.

    // Declare local variable arrays
    double hDD[DIM][DIM], hDDdD[DIM][DIM][DIM], hDDdupD[DIM][DIM][DIM], hDDddnD[DIM][DIM][DIM];
    double vetU[DIM], vetUdD[DIM][DIM], vetUdupD[DIM][DIM], vetUddnD[DIM][DIM];
    double aDD[DIM][DIM];
    double alpha, alphadD[DIM], alphadupD[DIM], alphaddnD[DIM];
    double phi, phidD[DIM], phidupD[DIM], phiddnD[DIM];
    double trK;
    double betU[DIM], betUdD[DIM][DIM], betUdupD[DIM][DIM];
    double detg, detgdD[DIM];

    /* Inputs for autogenFDcode/ code generation. 
       Note that UPDOWNWIND is contingent on UPWIND being
       #define'd, and betU/betaU's UPDOWNWIND are contingent
       on SHIFTADVECT & BIADVECT being #define'd, respectively.

       To generate all finite difference code for this file,
       simply go to the autogenFDcode directory and run
       ./gen_stencil [this filename]

       DVGENSTART BSSN_L1rhs__input_gfdata_and_compute_derivs.h
       vetU	        ZERO	FIRST   UPDOWNWIND
       hDD      	ZERO	FIRST   UPDOWNWIND
       phi		ZERO    FIRST   UPDOWNWIND
       alpha     	ZERO    FIRST   UPDOWNWIND
       aDD		ZERO
       trK		ZERO
       betU		ZERO    FIRST
       detg 	        ZERO    FIRST
       DVGENEND
    */

#include "../autogenFDcode/BSSN_L1rhs__input_gfdata_and_compute_derivs.h"

    //double gammabarDD[DIM][DIM];

    //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);

    double gammabarUU[DIM][DIM];
    //double oneoverdetgammabar = 1.0 / detgammabar;
    //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

    Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

#ifdef UPWIND
    double ReU[DIM] = RESCALING_VECTOR;

    double dnwind[DIM];
    double upwind[DIM];
    F1(j) { dnwind[j] = 1.0; upwind[j] = 0.0; }
    F1(j) { if(vetU[j]*ReU[j]>0.0) { dnwind[j] = 0.0; upwind[j] = 1.0; } }

#ifdef SHIFTADVECT
    F2(i,j)   {  vetUdupD[i][j]          = upwind[j]*vetUdupD[i][j]    + dnwind[j]*vetUddnD[i][j]; }
#endif // SHIFTADVECT
    F3(i,j,k) {  hDDdupD[i][j][k]        = upwind[k]*hDDdupD[i][j][k]  + dnwind[k]*hDDddnD[i][j][k]; }
    F1(i)     {  phidupD[i]              = upwind[i]*phidupD[i]        + dnwind[i]*phiddnD[i]; }
#ifdef LAPSEADVECT
    F1(i)     {  alphadupD[i]            = upwind[i]*alphadupD[i]      + dnwind[i]*alphaddnD[i]; }
#endif // LAPSEADVECT
#else // UPWIND
#ifdef SHIFTADVECT
    F2(i,j)   {  vetUdupD[i][j]          = vetUdD[i][j];   }
#endif // SHIFTADVECT
    F3(i,j,k) {  hDDdupD[i][j][k]        = hDDdD[i][j][k]; }
    F1(i)     {  phidupD[i]              = phidD[i];       }
#ifdef BIADVECT
    F2(i,j)   {  betUdupD[i][j]          = betUdD[i][j];   }
#endif // BIADVECT
#ifdef LAPSEADVECT
    F1(i)     {  alphadupD[i]            = alphadD[i];     }
#endif // LAPSEADVECT
#endif // UPWIND

    double BSSNL1Operator[NUM_GFS];

    {
#include "../autogenMathcode/BSSNL1Operator.txt-parsed.h"
    }

    int which_gf=0;
    for(int j=H11;j<=H33;j++)   { rhs_gfs[j][idx]    =BSSNL1Operator[which_gf]; which_gf++; }
    /**/                          rhs_gfs[PHI][idx]  =BSSNL1Operator[which_gf]; which_gf++;
    /**/                          rhs_gfs[ALPHA][idx]=BSSNL1Operator[which_gf]; which_gf++;
    for(int j=VET1;j<=VET3;j++) { rhs_gfs[j][idx]    =BSSNL1Operator[which_gf]; which_gf++; }


    
    /*if(ii == 17 && jj == 17 && kk == 17)
      {
	//printf("trAbar = %.15e\n", BSSNL1Operator[19]);
	//printf("Dbarbetacontraction = %.15e\n", BSSNL1Operator[18]);
	//printf("div beta = %.15e\n", BSSNL1Operator[12]);
	//printf("detg = %.15e\n", detg);
	//printf("alpha_a %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", in_gfs[ALPHA][idx], in_gfs[A11][idx], in_gfs[A12][idx], in_gfs[A13][idx], in_gfs[A22][idx], in_gfs[A23][idx], in_gfs[A33][idx]);
	//printf("alpha_K %.15e %.15e\n", in_gfs[ALPHA][idx], in_gfs[TRK][idx]);
	//printf("alpha_K %.15e %.15e\n", alpha, trK);
	//printf("LbetagammabarDD %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", BSSNL1Operator[11], BSSNL1Operator[12], BSSNL1Operator[13], BSSNL1Operator[14], BSSNL1Operator[15], BSSNL1Operator[16], BSSNL1Operator[17]);
	//printf("phi_rhs = %.15e\n", BSSNL1Operator[17] + (BSSNL1Operator[18] - alpha * trK) / 6.0);
	//printf("phirhs = %.15e\n", BSSNL1Operator[6]);
	//printf("gup %.15e %.15e %.15e %.15e %.15e %.15e\n", gammabarUU[0][0], gammabarUU[0][1], gammabarUU[0][2], gammabarUU[1][1], gammabarUU[1][2], gammabarUU[2][2]);
	printf("vetU %.15e %.15e %.15e\n", vetU[0], vetU[1], vetU[2]);
	printf("betU %.15e %.15e %.15e\n", betU[0], betU[1], betU[2]);
	printf("betadotbetad %.15e %.15e %.15e\n", BSSNL1Operator[11], BSSNL1Operator[12], BSSNL1Operator[13]);
	printf("vetUrhs %.15e %.15e %.15e\n", BSSNL1Operator[8], BSSNL1Operator[9], BSSNL1Operator[10]);
	}*/
    
    
  }
  //gettimeofday(&end, NULL);

  //seconds  = end.tv_sec  - start.tv_sec;
  //useconds = end.tv_usec - start.tv_usec;

  //mtime = ((seconds) * 1000 + useconds/1000.0) + 0.999;  // We add 0.999 since mtime is a long int; this rounds up the result before setting the value.  Here, rounding down is incorrect.
  //printf("Finished in %e seconds. %e gridpoints per second.\n",((double)mtime/1000.0),(Npts[2]-2*NGHOSTS)*(Npts[1]-2*NGHOSTS)*(Npts[0]-2*NGHOSTS) / ((double)mtime/1000.0));

}

#endif // BSSNL1_C__
