///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN BSSN PIRK2 STEPS
int gf;
const double tp1 = t + dt;

double *gfs_tmp[NUM_GFS];

//int i1 = 17, j1 = 17, k1 = 17;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 1: Evolve hDD, phi, alpha, and vetU explicitly to intermediate timestep (1)
BSSN_L1_rhs(gfs_n, xx, yy, gfs_L1_n); // Returns L1 operator for hDD, phi, alpha, and vetU

for(gf = H11; gf <= H33; gf++)   PIRK2_Eq1(gf, gfs_n, gfs_L1_n, gfs_1); // Evolve hDD to (1)
/**/gf = PHI;                    PIRK2_Eq1(gf, gfs_n, gfs_L1_n, gfs_1); // Evolve phi to (1)
/**/gf = ALPHA;                  PIRK2_Eq1(gf, gfs_n, gfs_L1_n, gfs_1); // Evolve alpha to (1)
for(gf = VET1; gf <= VET3; gf++) PIRK2_Eq1(gf, gfs_n, gfs_L1_n, gfs_1); // Evolve vetU to (1)

for(gf = H11; gf <= H33; gf++)   Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to hDD
/**/gf = PHI;                    Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to phi
/**/gf = ALPHA;                  Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to alpha
for(gf = VET1; gf <= VET3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to vetU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = H11; gf <= H33; gf++)   Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
/**/gf = PHI;                    Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
/**/gf = ALPHA;                  Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
for(gf = VET1; gf <= VET3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
#endif

BSSN_Det(gfs_1); // detg at timestep (1)

//printf("STEP 1 %d %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", n, gfs_1[H11][GFIDX(i1, j1, k1)], gfs_1[H12][GFIDX(i1, j1, k1)], gfs_1[H13][GFIDX(i1, j1, k1)], gfs_1[H22][GFIDX(i1, j1, k1)], gfs_1[H23][GFIDX(i1, j1, k1)], gfs_1[H33][GFIDX(i1, j1, k1)], gfs_1[PHI][GFIDX(i1, j1, k1)], gfs_1[ALPHA][GFIDX(i1, j1, k1)], gfs_1[VET1][GFIDX(i1, j1, k1)], gfs_1[VET2][GFIDX(i1, j1, k1)], gfs_1[VET3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 2: Evolve lambdaU explicitly to intermediate timestep (1)
//BSSN_ExplicitLambda_rhs(gfs_n, xx, yy, gfs_L1_n); // Returns L1 operator for lambdaU

for(gf = 0; gf < NUM_GFS; gf++)
  {
    gfs_tmp[gf] = gfs_n[gf];
  }

gfs_tmp[PHI] = gfs_1[PHI];
gfs_tmp[ALPHA] = gfs_1[ALPHA];
gfs_tmp[VET1] = gfs_1[VET1];
gfs_tmp[VET2] = gfs_1[VET2];
gfs_tmp[VET3] = gfs_1[VET3];

BSSN_L2part2_rhs(gfs_n, xx, yy, gfs_L2_n); // Returns L2 operator for lambdaU
BSSN_L2part2_rhs(gfs_tmp, xx, yy, gfs_L2_1); // Returns L2 operator for lambdaU
BSSN_L3part2_rhs(gfs_n, xx, yy, gfs_L3_n); // Returns L3 operator for lambdaU

//for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq1(gf, gfs_n, gfs_L1_n, gfs_1); // Evolve lambdaU to (1)

for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq2(gf, gfs_n, gfs_L2_n, gfs_L2_1, gfs_L3_n, gfs_1);

for(gf = LAMB1; gf <= LAMB3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to lambdaU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = LAMB1; gf <= LAMB3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
#endif

//printf("STEP 2 %d %.15e %.15e %.15e\n", n, gfs_1[LAMB1][GFIDX(i1, j1, k1)], gfs_1[LAMB2][GFIDX(i1, j1, k1)], gfs_1[LAMB3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 3: Evolve aDD and trK partially implicitly to intermediate timestep (1)
BSSN_L2part1_rhs(gfs_n, xx, yy, gfs_L2_n); // Returns L2 operator for aDD and trK
BSSN_L2part1_rhs(gfs_1, xx, yy, gfs_L2_1); // Returns L2 operator for aDD and trK
BSSN_L3part1_rhs(gfs_n, xx, yy, gfs_L3_n); // Returns L3 operator for aDD and trK

for(gf = A11; gf <= A33; gf++) PIRK2_Eq2(gf, gfs_n, gfs_L2_n, gfs_L2_1, gfs_L3_n, gfs_1); // Evolve aDD to (1)
/**/gf = TRK;                  PIRK2_Eq2(gf, gfs_n, gfs_L2_n, gfs_L2_1, gfs_L3_n, gfs_1); // Evolve trK to (1)

for(gf = A11; gf <= A33; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to aDD
/**/gf = TRK;                  Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to trK

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = A11; gf <= A33; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
/**/gf = TRK;                  Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
#endif

//printf("STEP 3 %d %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", n, gfs_1[A11][GFIDX(i1, j1, k1)], gfs_1[A12][GFIDX(i1, j1, k1)], gfs_1[A13][GFIDX(i1, j1, k1)], gfs_1[A22][GFIDX(i1, j1, k1)], gfs_1[A23][GFIDX(i1, j1, k1)], gfs_1[A33][GFIDX(i1, j1, k1)], gfs_1[TRK][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 4: Evolve lambdaU partially implicitly to intermediate timestep (1)
// OVERWRITE the previous values of lambdaU at (1)
BSSN_L2part2_rhs(gfs_n, xx, yy, gfs_L2_n); // Returns L2 operator for lambdaU
BSSN_L2part2_rhs(gfs_1, xx, yy, gfs_L2_1); // Returns L2 operator for lambdaU
BSSN_L3part2_rhs(gfs_n, xx, yy, gfs_L3_n); // Returns L3 operator for lambdaU

for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq2(gf, gfs_n, gfs_L2_n, gfs_L2_1, gfs_L3_n, gfs_1); // Evolve lambdaU to (1)

for(gf = LAMB1; gf <= LAMB3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to lambdaU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = LAMB1; gf <= LAMB3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
#endif

//printf("STEP 4 %d %.15e %.15e %.15e\n", n, gfs_1[LAMB1][GFIDX(i1, j1, k1)], gfs_1[LAMB2][GFIDX(i1, j1, k1)], gfs_1[LAMB3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 5: Evolve betU partially implicitly to intermediate timestep (1)
BSSN_L2part3_rhs(gfs_n, xx, yy, gfs_L2_n); // Returns L2 operator for betU
BSSN_L2part3_rhs(gfs_1, xx, yy, gfs_L2_1); // Returns L2 operator for betU
BSSN_L3part3_rhs(gfs_n, xx, yy, gfs_L3_n); // Returns L3 operator for betU

for(gf = BET1; gf <= BET3; gf++) PIRK2_Eq2(gf, gfs_n, gfs_L2_n, gfs_L2_1, gfs_L3_n, gfs_1); // Evolve betU to (1)

for(gf = BET1; gf <= BET3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_1); // Apply boundary conditions to betU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = BET1; gf <= BET3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_1);
#endif

//printf("STEP 5 %d %.15e %.15e %.15e\n", n, gfs_1[BET1][GFIDX(i1, j1, k1)], gfs_1[BET2][GFIDX(i1, j1, k1)], gfs_1[BET3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 6: Evolve hDD, phi, alpha, vetU explicitly to timestep n+1
BSSN_L1_rhs(gfs_1, xx, yy, gfs_L1_1); // Returns L1 operator for hDD, phi, alpha, and vetU

for(gf = H11; gf <= H33; gf++)   PIRK2_Eq3(gf, gfs_n, gfs_1, gfs_L1_1, gfs_np1); // Evolve hDD to n+1
/**/gf = PHI;                    PIRK2_Eq3(gf, gfs_n, gfs_1, gfs_L1_1, gfs_np1); // Evolve phi to n+1
/**/gf = ALPHA;                  PIRK2_Eq3(gf, gfs_n, gfs_1, gfs_L1_1, gfs_np1); // Evolve alpha to n+1
for(gf = VET1; gf <= VET3; gf++) PIRK2_Eq3(gf, gfs_n, gfs_1, gfs_L1_1, gfs_np1); // Evolve vetU to n+1    

for(gf = H11; gf <= H33; gf++)   Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to hDD
/**/gf = PHI;                    Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to phi
/**/gf = ALPHA;                  Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to alpha
for(gf = VET1; gf <= VET3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to vetU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = H11; gf <= H33; gf++)   Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
/**/gf = PHI;                    Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
/**/gf = ALPHA;                  Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
for(gf = VET1; gf <= VET3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
#endif

BSSN_Det(gfs_np1); // detg at timestep n+1

//printf("STEP 6 %d %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", n, gfs_np1[H11][GFIDX(i1, j1, k1)], gfs_np1[H12][GFIDX(i1, j1, k1)], gfs_np1[H13][GFIDX(i1, j1, k1)], gfs_np1[H22][GFIDX(i1, j1, k1)], gfs_np1[H23][GFIDX(i1, j1, k1)], gfs_np1[H33][GFIDX(i1, j1, k1)], gfs_np1[PHI][GFIDX(i1, j1, k1)], gfs_np1[ALPHA][GFIDX(i1, j1, k1)], gfs_np1[VET1][GFIDX(i1, j1, k1)], gfs_np1[VET2][GFIDX(i1, j1, k1)], gfs_np1[VET3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 7: Evolve lambdaU explicitly to timestep n+1
//BSSN_ExplicitLambda_rhs(gfs_1, xx, yy, gfs_L1_1); // Returns L1 operator for lambdaU    

for(gf = 0; gf < NUM_GFS; gf++)
  {
    gfs_tmp[gf] = gfs_1[gf];
  }

gfs_tmp[PHI] = gfs_np1[PHI];
gfs_tmp[ALPHA] = gfs_np1[ALPHA];
gfs_tmp[VET1] = gfs_np1[VET1];
gfs_tmp[VET2] = gfs_np1[VET2];
gfs_tmp[VET3] = gfs_np1[VET3];

BSSN_L2part2_rhs(gfs_n,   xx, yy, gfs_L2_n); // Returns L2 operator for lambdaU
BSSN_L2part2_rhs(gfs_tmp, xx, yy, gfs_L2_np1); // Returns L2 operator for lambdaU
BSSN_L3part2_rhs(gfs_n,   xx, yy, gfs_L3_n); // Returns L3 operator for lambdaU
BSSN_L3part2_rhs(gfs_tmp, xx, yy, gfs_L3_1); // Returns L3 operator for lambdaU

//for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq3(gf, gfs_n, gfs_1, gfs_L1_1, gfs_np1); // Evolve lambdaU to n+1

for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq4(gf, gfs_n, gfs_L2_n, gfs_L2_np1, gfs_L3_n, gfs_L3_1, gfs_np1); // Evolve lambdaU to n+1

for(gf = LAMB1; gf <= LAMB3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to lambdaU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = LAMB1; gf <= LAMB3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
#endif

//printf("STEP 7 %d %.15e %.15e %.15e\n", n, gfs_np1[LAMB1][GFIDX(i1, j1, k1)], gfs_np1[LAMB2][GFIDX(i1, j1, k1)], gfs_np1[LAMB3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 8: Evolve aDD and trK partially implicitly to timestep n+1
BSSN_L2part1_rhs(gfs_n,   xx, yy, gfs_L2_n); // Returns L2 operator for aDD and trK
BSSN_L2part1_rhs(gfs_np1, xx, yy, gfs_L2_np1); // Returns L2 operator for aDD and trK
BSSN_L3part1_rhs(gfs_n,   xx, yy, gfs_L3_n); // Returns L3 operator for aDD and trK
BSSN_L3part1_rhs(gfs_1,   xx, yy, gfs_L3_1); // Returns L3 operator for aDD and trK

for(gf = A11; gf <= A33; gf++) PIRK2_Eq4(gf, gfs_n, gfs_L2_n, gfs_L2_np1, gfs_L3_n, gfs_L3_1, gfs_np1); // Evolve aDD to n+1
/**/gf = TRK;                  PIRK2_Eq4(gf, gfs_n, gfs_L2_n, gfs_L2_np1, gfs_L3_n, gfs_L3_1, gfs_np1); // Evolve trK to n+1

for(gf = A11; gf <= A33; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to aDD
/**/gf = TRK;                  Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to trK

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = A11; gf <= A33; gf++)   Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
/**/gf = TRK;                    Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
#endif

//printf("STEP 8 %d %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", n, gfs_np1[A11][GFIDX(i1, j1, k1)], gfs_np1[A12][GFIDX(i1, j1, k1)], gfs_np1[A13][GFIDX(i1, j1, k1)], gfs_np1[A22][GFIDX(i1, j1, k1)], gfs_np1[A23][GFIDX(i1, j1, k1)], gfs_np1[A33][GFIDX(i1, j1, k1)], gfs_np1[TRK][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 9: Evolve lambdaU partially implicitly to timestep n+1
// OVERWRITE the previous values of lambdaU at n+1

for(gf = 0; gf < NUM_GFS; gf++)
  {
    gfs_tmp[gf] = gfs_np1[gf];
  }

gfs_tmp[LAMB1] = gfs_1[LAMB1];
gfs_tmp[LAMB2] = gfs_1[LAMB2];
gfs_tmp[LAMB3] = gfs_1[LAMB3];

BSSN_L2part2_rhs(gfs_n,   xx, yy, gfs_L2_n); // Returns L2 operator for lambdaU
BSSN_L2part2_rhs(gfs_np1, xx, yy, gfs_L2_np1); // Returns L2 operator for lambdaU
BSSN_L3part2_rhs(gfs_n,   xx, yy, gfs_L3_n); // Returns L3 operator for lambdaU
//BSSN_L3part2_rhs(gfs_1,   xx, yy, gfs_L3_1); // Returns L3 operator for lambdaU
BSSN_L3part2_rhs(gfs_tmp,   xx, yy, gfs_L3_1);

for(gf = LAMB1; gf <= LAMB3; gf++) PIRK2_Eq4(gf, gfs_n, gfs_L2_n, gfs_L2_np1, gfs_L3_n, gfs_L3_1, gfs_np1); // Evolve lambdaU to n+1

for(gf = LAMB1; gf <= LAMB3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to lambdaU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = LAMB1; gf <= LAMB3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
#endif

//printf("STEP 9 %d %.15e %.15e %.15e\n", n, gfs_np1[LAMB1][GFIDX(i1, j1, k1)], gfs_np1[LAMB2][GFIDX(i1, j1, k1)], gfs_np1[LAMB3][GFIDX(i1, j1, k1)]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSSN PIRK2 Notes Step 10: Evolve betU partially implicitly to timestep n+1
BSSN_L2part3_rhs(gfs_n,   xx, yy, gfs_L2_n); // Returns L2 operator for betU
BSSN_L2part3_rhs(gfs_np1, xx, yy, gfs_L2_np1); // Returns L2 operator for betU
BSSN_L3part3_rhs(gfs_n,   xx, yy, gfs_L3_n); // Returns L3 operator for betU
BSSN_L3part3_rhs(gfs_1,   xx, yy, gfs_L3_1); // Returns L3 operator for betU

for(gf = BET1; gf <= BET3; gf++) PIRK2_Eq4(gf, gfs_n, gfs_L2_n, gfs_L2_np1, gfs_L3_n, gfs_L3_1, gfs_np1); // Evolve betU to n+1

for(gf = BET1; gf <= BET3; gf++) Apply_out_sym_bcs(gf, tp1, xx, yy, gfs_np1); // Apply boundary conditions to betU

#if defined OUTER_BOUNDARY_SOMMERFELD
for(gf = BET1; gf <= BET3; gf++) Sommerfeld_Outer_BC(gf, xx, yy, gfs_n, gfs_np1);
#endif

//printf("STEP 10 %d %.15e %.15e %.15e\n", n, gfs_np1[BET1][GFIDX(i1, j1, k1)], gfs_np1[BET2][GFIDX(i1, j1, k1)], gfs_np1[BET3][GFIDX(i1, j1, k1)]);

// END BSSN PIRK2 STEPS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





Hamiltonian_Constraint(xx, yy, gfs_n, gfs_aux);
const double ham_L2 = L2_Norm_Over_Entire_Grid(HAM, gfs_aux);

FILE *outfile;
outfile = fopen("SENR_output.txt", "a");

/*for(int kk = NGHOSTS; kk < Npts[2] - NGHOSTS; kk++)
  {
    for(int jj = NGHOSTS; jj < Npts[1] - NGHOSTS; jj++)
      {
	for(int ii = NGHOSTS; ii < Npts[0] - NGHOSTS; ii++)
	  {
	    fprintf(outfile, "%d %d %d %d %.15e\n", n, ii, jj, kk, gfs_aux[HAM][GFIDX(ii, jj, kk)]);
	    //fprintf(outfile, "%d %d %d %d %.15e\n", n, ii, jj, kk, gfs_n[H11][GFIDX(ii, jj, kk)]);
	  }
      }
  }

  fprintf(outfile, "\n\n");*/

fprintf(outfile, "%d %.15e\n", n, ham_L2);

fclose(outfile);


// Rescale gammabarDD to "unit" determinant
Rescale_Metric_Det(yy, gfs_np1, gfs_aux);

// Enforce trace-free condition on AbarDD
Remove_Trace(yy, gfs_np1);
