/*
  The scalar function u satisfies the wave equation
  ==> \nabla^{mu} \nabla_{mu} u = 0
  
  Expand in terms of ordinary derivatives
  ==> g^{mu nu} d_{mu} d_{nu} u - g^{mu nu} \Gamma^{tau}_{mu nu} d_{tau} u = 0
  
  Define v = d_{0} u, assume g^{0 0} = -1 and \Gamma^{0} = 0, and expand the sums and contract the Christoffel symbols
  ==> -d_{0} v + g^{0 i} d_{0} d_{i} u + g^{i 0} d_{i} v + g^{i j} d_{i} d_{j} u - \Gamma^{i} d_{i} u = 0
  
  Collect terms, using metric and partial derivative symmetry
  ==> d_{0} v = 2 g^{0 i} d_{i} v + g^{i j} d_{i} d_{j} u - \Gamma^{i} d_{i} u
              = 2 * term1 + term2 - term3

  term1 is set in ScalarWave_L3()
  term2 is set in ScalarWave_L2()
  term3 is set in ScalarWave_L2()
*/

// PIRK2 L1 operator for the scalar wave
void ScalarWave_L1(double **in_gfs, double **rhs_gfs)
{
  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);
      const double v = in_gfs[V][idx];

      rhs_gfs[U][idx] = v; // PIRK L1 Operator
    }

  return;
}

// PIRK2 L2 operator for the scalar wave
void ScalarWave_L2(double **in_gfs, double **gfs_aux, double **gfs_rhs)
{
  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);

      // Read off contracted Christoffel symbols
      const double Gam[3] = {gfs_aux[GAM1][idx], gfs_aux[GAM2][idx], gfs_aux[GAM3][idx]};

      double udD[3]; // First spatial derivatives udD[i] = d_{i} u
      double udDD[3][3]; // Second spatial derivatives udDD[i][j] = d_{i} d_{j} u

      const int stencil_width = 2 * NGHOSTS + 1;
      double locGF[stencil_width][stencil_width][stencil_width];

      /*
	DVGENSTART WaveToy_L2_RHS_Derivs.h
	u FIRST SECOND
	DVGENEND
      */

#include "../autogenFDcode/WaveToy_L2_RHS_Derivs.h"
      
      const double term2 = gfs_aux[GINV11][idx] * udDD[0][0] + gfs_aux[GINV22][idx] * udDD[1][1] + gfs_aux[GINV33][idx] * udDD[2][2] + 2.0 * (gfs_aux[GINV12][idx] * udDD[0][1] + gfs_aux[GINV13][idx] * udDD[0][2] + gfs_aux[GINV23][idx] * udDD[1][2]); // g^{i j} d_{i} d_{j} u
      double term3 = 0.0;
      
      for(int i = 0; i < 3; i++)
	{
	  term3 += Gam[i] * udD[i]; // \Gamma^{i} d_{i} u
	}

      gfs_rhs[V][idx] = term2 - term3;  // PIRK L2 Operator
    } // END for(i, j, k)

  return;
}

// PIRK2 L3 operator for the scalar wave
void ScalarWave_L3(double **in_gfs, double **gfs_aux, double **gfs_rhs)
{
  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);

      // Read off inverse metric
      double gInv0U[3] = {gfs_aux[GINV01][idx], gfs_aux[GINV02][idx], gfs_aux[GINV03][idx]}; // gInv0U[i] = g^{0 i}
      
      double vdD[3]; // First spatial derivatives vdD[i] = d_{i} v
      
      const int stencil_width = 2 * NGHOSTS + 1;
      double locGF[stencil_width][stencil_width][stencil_width];

      /*
	DVGENSTART WaveToy_L3_RHS_Derivs.h
	v  FIRST
	DVGENEND
      */
      
#include "../autogenFDcode/WaveToy_L3_RHS_Derivs.h"
      
      double term1 = 0.0;
  
      for(int i = 0; i < 3; i++)
	{
	  term1 += gInv0U[i] * vdD[i]; // g^{0 i} d_{i} v
	}
      
      gfs_rhs[V][idx] = 2.0 * term1; // PIRK L3 Operator
    } // END for(i, j, k)
  
  return;
}
