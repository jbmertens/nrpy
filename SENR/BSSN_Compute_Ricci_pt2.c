#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "loop_setup.h" // Located in ../../common_functions/
#include "SIMD_header.h" // Must be included after loop_setup.h! Located in ../../common_functions/
#include "defines_for_BSSN_variables.h"
// Declare the struct datatype for precomputed hatted (reference-metric-related) quantities.
#include "reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h"

// Compute Ricci tensor.
void Compute_Ricci_pt2(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs,REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params)
{

#if defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)
  SIMD_WARNING_MESSAGE;

#include "parameters_readin_SIMD-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs_SIMD.h" // Located in ../../common_functions/

#include "evolution_equations/NRPy_codegen/NRPy_BSSN_Ricci_pt2.h-SIMDconstants.h"

  START_LOOP_NOGZFILL_SIMD(ii,jj,kk) {
#include "evolution_equations/NRPy_codegen/NRPy_BSSN_Ricci_pt2.h"
  } END_LOOP_NOGZFILL_SIMD;

#else

#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/
#include "read_FD_dxs.h" // Located in ../common_functions/

  START_LOOP_NOGZFILL(ii,jj,kk) {
#include "evolution_equations/NRPy_codegen/NRPy_BSSN_Ricci_pt2.h"
  } END_LOOP_NOGZFILL;

#endif

  return;
}
