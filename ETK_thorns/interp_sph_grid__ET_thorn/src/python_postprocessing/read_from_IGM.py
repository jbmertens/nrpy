from math import sin, cos, sqrt
import numpy as np
import struct
import matplotlib.pyplot as plt
import sys
import numpy.linalg as linalg

def input_from_file(filename):
    
    with open(filename,"rb") as f: 
        Nr=struct.unpack('i',f.read(4))[0]
        Nth=struct.unpack('i',f.read(4))[0]
        Nph=struct.unpack('i',f.read(4))[0]
        print "Nr,Nth,Nph:",Nr,Nth,Nph
        r_min=struct.unpack('d',f.read(8))[0]
        r_max=struct.unpack('d',f.read(8))[0]
        print r_min,"<= r <=",r_max
        th_min=struct.unpack('d',f.read(8))[0]
        th_max=struct.unpack('d',f.read(8))[0]
        print th_min,"<= th <=",th_max
        ph_min=struct.unpack('d',f.read(8))[0]
        ph_max=struct.unpack('d',f.read(8))[0]
        print ph_min,"<= ph <=",ph_max

        dr=(r_max-r_min)/(Nr-1.0);
        rs=np.arange(r_min,r_max+dr,dr)
        dth=(th_max-th_min)/(Nth-1.0);
        ths=np.arange(th_min,th_max+dth,dth)
        dph=(ph_max-ph_min)/(Nph-1.0);
        phs=np.arange(ph_min,ph_max+dph,dph)
        magicnum=0
        data=[]
        iters=[]
        times=[]
        
        bytechunk=f.read(8)
        
        print "len bytechunk=",len(bytechunk)
        while (bytechunk ):
            num=struct.unpack('d',bytechunk)[0]
            if(magicnum==0): 
                magicnum=num  
                print "magicnum=",magicnum
            elif(not magicnum==num):
                print"magicnum mismatch"
                print "got num=",num
                return data
            iters.append(struct.unpack('i',f.read(4))[0])
            times.append(struct.unpack('d',f.read(8))[0])
            print "reading slab it=",iters[-1]," time=",times[-1]
            Nvar=8
            bytechunk=f.read(Nr*Nth*Nph*Nvar*8)            
            data.append(np.frombuffer(bytechunk).reshape(Nvar,Nph,Nth,Nr))
            bytechunk=f.read(8)

    return rs,ths,phs,times,data


def compute_KS_metric(r,th,ph,M=1,a=0.9375):
    # Adopt the Kerr-Schild metric for Fishbone-Moncrief disks
    # http://gravity.psu.edu/numrel/jclub/jc/Cook___LivRev_2000-5.pdf
    # Alternatively, Appendix of https://arxiv.org/pdf/1704.00599.pdf
    # Variables needed for initial data given in spherical basis
    rhoKS2  = r**2 + a**2*cos(th)**2 # Eq 79 of Cook's Living Review article
    DeltaKS = r**2 - 2*M*r + a**2    # Eq 79 of Cook's Living Review article
    alphaKS = 1/sqrt(1 + 2*M*r/rhoKS2)
    betaKSU = np.zeros(3)
    betaKSU[0] = alphaKS**2*2*M*r/rhoKS2
    gammaKSDD = np.zeros(3,3)
    gammaKSDD[0][0] = 1 + 2*M*r/rhoKS2
    gammaKSDD[0][2] = gammaKSDD[2][0] = -(1 + 2*M*r/rhoKS2)*a*sin(th)**2
    gammaKSDD[1][1] = rhoKS2
    gammaKSDD[2][2] = (r**2 + a**2 + 2*M*r/rhoKS2 * a**2*sin(th)**2) * sin(th)**2

    if(0):
        AA = a**2 * cos(2*th) + a**2 + 2*r**2
        BB = AA + 4*M*r
        DD = sqrt(2*M*r / (a**2 * cos(th)**2 + r**2) + 1)
        KDD=np.zeros(3,3)
        KDD[0][0] = DD*(AA + 2*M*r)/(AA**2*BB) * (4*M*(a**2 * cos(2*th) + a**2 - 2*r**2))
        KDD[0][1] = KDD[1][0] = DD/(AA*BB) * 8*a**2*M*r*sin(th)*cos(th)
        KDD[0][2] = KDD[2][0] = DD/AA**2 * (-2*a*M*sin(th)**2 * (a**2 * cos(2*th) + a**2 - 2*r**2))
        KDD[1][1] = DD/BB * 4*M*r**2
        KDD[1][2] = KDD[2][1] = DD/(AA*BB) * (-8*a**3*M*r*sin(th)**3*cos(th))
        KDD[2][2] = DD/(AA**2*BB) * \
                    (2*M*r*sin(th)**2 * (a**4*(r-M)*cos(4*th) + a**4*(M+3*r) +
                    4*a**2*r**2*(2*r-M) + 4*a**2*r*cos(2*th)*(a**2 + r*(M+2*r)) + 8*r**5))

        # For compatibility, we must compute gPhys4UU
        gammaKSUU = np.zeros(3,3)
        gammaKSDD = linalg.inv(gammaKSUU)
        gammaKSDET = linalg.det(gammaKSUU)
    
        # See, e.g., Eq. 4.49 of https://arxiv.org/pdf/gr-qc/0703035.pdf , where N = alpha
        gphys4UU=np.zeros(4,4)
        gPhys4UU[0][0] = -1 / alphaKS**2
        for i in range(1,4):
            if i>0:
                # if the quantity does not have a "4", then it is assumed to be a 3D quantity.
                #  E.g., betaKSU[] is a spatial vector, with indices ranging from 0 to 2:
                gPhys4UU[0][i] = gPhys4UU[i][0] = betaKSU[i-1]/alphaKS**2
        for i in range(1,4):
            for j in range(1,4):
                # if the quantity does not have a "4", then it is assumed to be a 3D quantity.
                #  E.g., betaKSU[] is a spatial vector, with indices ranging from 0 to 2,
                #    and gammaKSUU[][] is a spatial tensor, with indices again ranging from 0 to 2.
                gPhys4UU[i][j] = gPhys4UU[j][i] = gammaKSUU[i-1][j-1] - betaKSU[i-1]*betaKSU[j-1]/alphaKS**2

    betaD = np.zero(3)
    for i in range(3):
        for j in range(3):
            betaD[i] += gammaDD[i][j]*betaU[j]

    beta2 = 0
    for i in range(3):
        beta2 += betaU[i]*betaD[i]

    gPhys4DD = np.zeros(4,4)
    gPhys4DD[0][0] = -alpha**2 + beta2
    for i in range(3):
        gPhys4DD[0][i+1] = gPhys4DD[i+1][0] = betaD[i]
        for j in range(3):
            gPhys4DD[i+1][j+1] = gammaDD[i][j]

    return gammaKSDD,gammaKS4DD

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.
    

filename=sys.argv[1]

#rs,ths,phs,times,data=input_from_file("interp_sph_grids_test_data_dec15.dat")
print "reading from",filename
rs,ths,phs,times,data=input_from_file(filename)


print "rho[th=ph=0]=",data[0][0,0,0,:]
print "rho[th=0,ph=N]=",data[0][0,0,-1,:]

#plt.plot(rs,np.log10(data[0][0,0,0,:]))
#plt.plot(rs,np.log10(data[0][0,:,0,0]))
#plt.show()

plt.plot(rs,np.log10(data[0][1,0,0,:]),rs,np.log10(data[0][0,0,0,:])*4/3.-3)
plt.show()


