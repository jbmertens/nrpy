# Parameter definitions for thorn interp_sph_grid__ET_thorn
# $Header:$

#############################################################################
### import HydroBase & ADMBase parameters

shares: HydroBase
USES CCTK_INT timelevels

shares: ADMBase
USES CCTK_INT lapse_timelevels
USES CCTK_INT shift_timelevels
USES CCTK_INT metric_timelevels

shares: IO
USES STRING out_dir
#############################################################################

private:

CCTK_INT interp_out_every "how often to output" STEERABLE=ALWAYS
{
  0:1500 :: "Any number"
} 8

########################################
CCTK_REAL interp_r_min "minimum radius" STEERABLE=ALWAYS
{
  0:* :: "Any number"
} 1.347981006

CCTK_REAL interp_r_max "maximum radius > minimum radius" STEERABLE=ALWAYS
{
  0:* :: "Any number"
} 50.0

CCTK_INT interp_Nr "Number of points in radial direction" STEERABLE=ALWAYS
{
  0:1500 :: "Any number"
} 150

########################################
CCTK_REAL interp_th_min "minimum theta" STEERABLE=ALWAYS
{
  0:3.14159265358979 :: "0 to pi"
} 1.0471975511966 # pi/3

CCTK_REAL interp_th_max "maximum theta > minimum theta" STEERABLE=ALWAYS
{
  0:3.14159265358979 :: "0 to pi"
} 2.0943951023932 # 2*pi/3

CCTK_INT interp_Nth "Number of points in theta direction" STEERABLE=ALWAYS
{
  0:1500 :: "Any number"
} 75

########################################
CCTK_REAL interp_ph_min "minimum phi" STEERABLE=ALWAYS
{
  0:6.28318530717959 :: "0 to 2 pi"
} 0

CCTK_REAL interp_ph_max "maximum phi > minimum phi" STEERABLE=ALWAYS
{
  0:6.28318530717959 :: "0 to 2 pi"
} 6.28318530717959 # 2*pi

CCTK_INT interp_Nph "Number of points in phi direction" STEERABLE=ALWAYS
{
  0:1500 :: "Any number"
} 150
########################################

## Interpolator information
CCTK_STRING interpolator_name "Which interpolator should I use"
{
  ".+" :: "Any nonempty string"
} "Lagrange polynomial interpolation"

CCTK_STRING interpolator_pars "Parameters for the interpolator"
{
  ".*" :: "Any string that Util_TableSetFromString() will take"
} "order=1"


########################################
CCTK_INT verbose "Set verbosity level: 1=useful info; 2=moderately annoying (though useful for debugging)" STEERABLE=ALWAYS
{
  0:2 :: "0 = no output; 1=useful info; 2=moderately annoying (though useful for debugging)"
} 1
