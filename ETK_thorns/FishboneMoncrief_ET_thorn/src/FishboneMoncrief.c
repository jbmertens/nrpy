#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// Use booleans
#include <stdbool.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#define REAL CCTK_REAL

// Alias for "vel" vector gridfunction:
#define velx (&vel[0*cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2]])
#define vely (&vel[1*cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2]])
#define velz (&vel[2*cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2]])


void FishboneMoncrief_ET_GRHD_initial(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  printf("Fishbone-Moncrief Disk Initial data.\n");
  printf("Using input parameters of\n a = %e,\n M = %e,\nr_in = %e,\nr_at_max_density = %e\nkappa = %e\ngamma = %e\n",a,M,r_in,r_at_max_density,kappa,gamma);

  // First compute maximum density
  CCTK_REAL rho_max;
  {
    CCTK_REAL hm1;
    CCTK_REAL x1 = r_at_max_density;
    CCTK_REAL x2 = 0.0;
    CCTK_REAL x3 = 0.0;
    {
#include "NRPy_codegen/FMdisk_GRHD_hm1.h"
    }
    rho_max = pow( hm1 * (gamma-1.0) / (kappa*gamma), 1.0/(gamma-1.0) );
  }

#pragma omp parallel for
  for(int k=0;k<cctk_lsh[2];k++) for(int j=0;j<cctk_lsh[1];j++) for(int i=0;i<cctk_lsh[0];i++) {
        int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);

        CCTK_REAL x1 = x[idx];
        CCTK_REAL x2 = y[idx];
        CCTK_REAL x3 = z[idx];
        CCTK_REAL rr = r[idx];

        {
#include "NRPy_codegen/KerrSchild.h"
        }
        
        CCTK_REAL hm1;
        bool set_to_atmosphere=false;
        if(rr > r_in) {
          {
#include "NRPy_codegen/FMdisk_GRHD_hm1.h"
          }
          if(hm1 > 0) {
            rho[idx] = pow( hm1 * (gamma-1.0) / (kappa*gamma), 1.0/(gamma-1.0) ) / rho_max;
            press[idx] = kappa*pow(rho[idx], gamma);
            // P = (\Gamma - 1) rho epsilon
            eps[idx] = press[idx] / (rho[idx] * (gamma - 1.0));
#include "NRPy_codegen/FMdisk_GRHD_velocities.h"
          } else {
            set_to_atmosphere=true;
          }
        } else {
          set_to_atmosphere=true;
        }
        // Outside the disk? Set to atmosphere all hydrodynamic variables!
        if(set_to_atmosphere) {
          // Choose an atmosphere such that 
          //   rho =       1e-5 *x r^(-3/2), and
          //   P   = (1/3)*1e-7 * r^(-5/2)
          // Add 1e-100 or 1e-300 to rr or rho to avoid divisions by zero.
          rho[idx] = 1e-5 * pow(rr + 1e-100,-3.0/2.0);
          press[idx] = (1.0/3.0)*1e-7 * pow(rr + 1e-100,-5.0/2.0);
          eps[idx] = press[idx] / ((rho[idx] + 1e-300) * (gamma - 1.0));
          w_lorentz[idx] = 1.0;
          velx[idx] = 0.0;
          vely[idx] = 0.0;
          velz[idx] = 0.0;
        }
      }

  int final_idx = CCTK_GFINDEX3D(cctkGH,cctk_lsh[0]-1,cctk_lsh[1]-1,cctk_lsh[2]-1);
  printf("=====   OUTPUTS   =====\n");
  printf("betai: %e %e %e \ngij: %e %e %e %e %e %e \nKij: %e %e %e %e %e %e\nalp: %e\n\n",betax[final_idx],betay[final_idx],betaz[final_idx],gxx[final_idx],gxy[final_idx],gxz[final_idx],gyy[final_idx],gyz[final_idx],gzz[final_idx],kxx[final_idx],kxy[final_idx],kxz[final_idx],kyy[final_idx],kyz[final_idx],kzz[final_idx],alp[final_idx]);
  printf("rho: %.15e\nPressure: %.15e\nvx: %.15e\nvy: %.15e\nvz: %.15e\n",rho[final_idx],press[final_idx],velx[final_idx],vely[final_idx],velz[final_idx]);
}

void FishboneMoncrief_ET_GRHD_initial__perturb_pressure(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for
  for(int k=0;k<cctk_lsh[2];k++) for(int j=0;j<cctk_lsh[1];j++) for(int i=0;i<cctk_lsh[0];i++) {
        int idx = CCTK_GFINDEX3D(cctkGH,i,j,k);
        CCTK_REAL random_number_between_min_and_max = random_min + (random_max - random_min)*drand48();
        press[idx] = press[idx]*(1.0 + random_number_between_min_and_max);
        // Add 1e-300 to rho to avoid division by zero when density is zero.
        eps[idx] = press[idx] / ((rho[idx] + 1e-300) * (gamma - 1.0));
      }
}

