// Check gridfunctions for NaNs
void NaN_Checker(const int RK4_step, const int n, const REAL t, REAL *x1G, REAL *x2G, REAL *x3G, REAL *gfs_n, const char **gf_name, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/

  int nanflag = 0;

  for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
    LOOP_NOGZFILL(i, j, k) {
      const int idx = IDX3(i, j, k);

      if(!isfinite(gfs_n[IDX4pt(gf,idx)])) {
        nanflag = 1;
        printf("NaN in RK4 Step = %d n = %d, t = %e, (x1,x2,x3)=(%e, %e, %e) gfs_n[%s][(%d,%d,%d)] = %e\n", RK4_step, n, t, x1G[i], x2G[j], x3G[k],  gf_name[gf] , i,j,k, gfs_n[IDX4pt(gf,idx)]);
      }
    }
  }

  if(nanflag) {
    printf("NaN detected BEFORE RK4 Step %d\n", RK4_step);
    exit(1);
  }

  return;
}
