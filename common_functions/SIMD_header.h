#if defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)

#include <immintrin.h>

// ENABLE_SIMD_512_INTRINSICS
#ifdef ENABLE_SIMD_256_INTRINSICS

#define ConstSIMD(a) _mm256_set1_pd(a)
#define AddSIMD(a,b) _mm256_add_pd((a),(b))
#define SubSIMD(a,b) _mm256_sub_pd((a),(b))
#define MulSIMD(a,b) _mm256_mul_pd((a),(b))
#define DivSIMD(a,b) _mm256_div_pd((a),(b))
#ifdef ENABLE_SIMD_256_FMA_INTRINSICS
#define FusedMulAddSIMD(a,b,c) _mm256_fmadd_pd((a),(b),(c))
#define FusedMulSubSIMD(a,b,c) _mm256_fmsub_pd((a),(b),(c))
#else
#define FusedMulAddSIMD(a,b,c) _mm256_add_pd(_mm256_mul_pd(a,b),c)
#define FusedMulSubSIMD(a,b,c) _mm256_sub_pd(_mm256_mul_pd(a,b),c)
#endif // ENABLE_SIMD_256_FMA_INTRINSICS

#ifdef __INTEL_COMPILER
// Use Intel SVML:
#define PowSIMD(a,b) _mm256_pow_pd((a),(b))
#define SqrtSIMD(a) _mm256_sqrt_pd((a))
#define CbrtSIMD(a) _mm256_cbrt_pd((a))
#define ExpSIMD(a) _mm256_exp_pd((a))
#define SinSIMD(a) _mm256_sin_pd((a))
#define CosSIMD(a) _mm256_cos_pd((a))
#else // GCC
#include "../sleef/build/include/sleef.h"
// Use Sleef:
#define PowSIMD(a,b) Sleef_powd4_u10((a),(b))
#define SqrtSIMD(a) Sleef_sqrtd4_u35((a))
#define CbrtSIMD(a) Sleef_cbrtd4_u35((a))
#define ExpSIMD(a) Sleef_expd4_u10((a))
#define SinSIMD(a) Sleef_sind4_u35((a))
#define CosSIMD(a) Sleef_cosd4_u35((a))
#endif // __INTEL_COMPILER / GCC

// ENABLE_SIMD_512_INTRINSICS
#else 

#define ConstSIMD(a) _mm512_set1_pd(a)
#define AddSIMD(a,b) _mm512_add_pd((a),(b))
#define SubSIMD(a,b) _mm512_sub_pd((a),(b))
#define MulSIMD(a,b) _mm512_mul_pd((a),(b))
#define DivSIMD(a,b) _mm512_div_pd((a),(b))
#define FusedMulAddSIMD(a,b,c) _mm512_fmadd_pd((a),(b),(c))
#define FusedMulSubSIMD(a,b,c) _mm512_fmsub_pd((a),(b),(c))

#ifdef __INTEL_COMPILER
// Use Intel SVML:
#define PowSIMD(a,b) _mm512_pow_pd((a),(b))
#define SqrtSIMD(a) _mm512_sqrt_pd((a))
#define CbrtSIMD(a) _mm512_cbrt_pd((a))
#define ExpSIMD(a) _mm512_exp_pd((a))
#define SinSIMD(a) _mm512_sin_pd((a))
#define CosSIMD(a) _mm512_cos_pd((a))
#else // GCC
#include "../sleef/build/include/sleef.h"
// Use Sleef:
#define PowSIMD(a,b) Sleef_powd4_u10((a),(b))
#define SqrtSIMD(a) Sleef_sqrtd4_u35((a))
#define CbrtSIMD(a) Sleef_cbrtd4_u35((a))
#define ExpSIMD(a) Sleef_expd4_u10((a))
#define SinSIMD(a) Sleef_sind4_u35((a))
#define CosSIMD(a) Sleef_cosd4_u35((a))
#endif // __INTEL_COMPILER / GCC

#endif // ENABLE_SIMD_256_INTRINSICS / ENABLE_SIMD_512_INTRINSICS

#endif // defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)
