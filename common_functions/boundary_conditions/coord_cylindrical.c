// Clyindrical coordinates (rho, ph, z)

// A mod function that handles negative numbers as expected
// e.g., (-1) % 8 == -1, but MOD(-1, 8) == 7
#define MOD(a, b) ( ( ( (a) % (b) ) + (b) ) % (b) )
#define J_PLUS_PI(j) (NGHOSTS + MOD((j) + Npts2 / 2 - 2 * NGHOSTS, Npts2 - 2 * NGHOSTS)) // ph -> pi + ph MOD 2 pi

void Apply_out_sym_bcs_Cylindrical(const int gf, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  // Outer rho boundary conditions
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
#if defined OUTER_BOUNDARY_EXACT
        REAL soln[NUM_GFS];
        Exact_soln(t, x1G[i], x2G[j], x3G[k], soln);
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
        // Linear extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
        // Quadratic extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
#endif
      }
    }
  }

  // Vector and tensor parity conditions.
  int parity_axis = 1;

#ifdef BSSN_EVOLUTION
  if(gf == VET1 || gf == VET2 || gf == LAMB1 || gf == LAMB2 || gf == BET1 || gf == BET2 || gf == H13 || gf == A13 || gf == H23 || gf == A23) {
    // Vector components rho or ph or tensor components rho-z or ph-z
    parity_axis = -1;
  }
#endif

  // Apply rho => -rho parity condition, near rho = 0
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      const int j_plus_pi = J_PLUS_PI(j);

      for(int i = 0; i < NGHOSTS; i++) {
        const int i_lower_ghost = NGHOSTS - 1 - i;
        const int i_lower_phys = NGHOSTS + i;

        // f(-eps, ph, z) = f(+eps, ph + pi, z)
        in_gfs[IDX4(gf, i_lower_ghost, j, k)] = parity_axis * in_gfs[IDX4(gf, i_lower_phys, j_plus_pi, k)];
      }
    }
  }

  // Apply periodic boundary condition in phi direction
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_plus_2pi = J_PLUS_PI(J_PLUS_PI(j));

      for(int i = 0; i < Npts1; i++) {
        // f(rho, -ph, z) = f(rho, 2 * pi - ph, z)
        in_gfs[IDX4(gf, i, j, k)] = in_gfs[IDX4(gf, i, j_plus_2pi, k)];
      }
    }
  }

  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
      const int j_plus_2pi = J_PLUS_PI(J_PLUS_PI(j));

      for(int i = 0; i < Npts1; i++) {
        // f(rho, 2 * pi + ph, z) = f(rho, ph, z)
        in_gfs[IDX4(gf, i, j, k)] = in_gfs[IDX4(gf, i, j_plus_2pi, k)];
      }
    }
  }

  // Outer -z boundary conditions
  for(int k = NGHOSTS - 1; k >= 0; k--) {
    for(int j = 0; j < Npts2; j++) {
      for(int i = 0; i < Npts1; i++) {
#if defined OUTER_BOUNDARY_EXACT
        REAL soln[NUM_GFS];
        Exact_soln(t, x1G[i], x2G[j], x3G[k], soln);
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
        // Linear extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j, k + 1)] - in_gfs[IDX4(gf, i, j, k + 2)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
        // Quadratic extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j, k + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k + 2)] + in_gfs[IDX4(gf, i, j, k + 3)];
#endif
      }
    }
  }

  // Outer +z boundary conditions
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    for(int j = 0; j < Npts2; j++) {
      for(int i = 0; i < Npts1; i++) {
#if defined OUTER_BOUNDARY_EXACT
        REAL soln[NUM_GFS];
        Exact_soln(t, x1G[i], x2G[j], x3G[k], soln);
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
        // Linear extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j, k - 1)] - in_gfs[IDX4(gf, i, j, k - 2)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
        // Quadratic extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];
#endif
      }
    }
  }

  return;
}

#undef MOD
#undef J_PLUS_PI
