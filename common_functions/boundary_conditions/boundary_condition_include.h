#include "coord_spherical.c"
#include "coord_cartesian.c"
#include "coord_cylindrical.c"
#include "coord_symtp.c"

// Include coordinate-specific functions
// Set boundary conditions
void Apply_out_sym_bcs(const int gf, const REAL t, REAL *x1G, REAL *x2G, REAL *x3G, REAL *in_gfs, paramstruct params) {
  if(strncmp(params.CoordSystem,"Spherical",100) == 0) {
    Apply_out_sym_bcs_Spherical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SinhSpherical",100) == 0) {
    Apply_out_sym_bcs_Spherical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SinhSphericalv2",100) == 0) {
    Apply_out_sym_bcs_Spherical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"Cartesian",100) == 0) {
    Apply_out_sym_bcs_Cartesian(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"Cylindrical",100) == 0) {
    Apply_out_sym_bcs_Cylindrical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SinhCylindrical",100) == 0) {
    Apply_out_sym_bcs_Cylindrical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SinhCylindricalv2",100) == 0) {
    Apply_out_sym_bcs_Cylindrical(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SymTP",100) == 0) {
    Apply_out_sym_bcs_SymTP(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else if(strncmp(params.CoordSystem,"SinhSymTP",100) == 0) {
    Apply_out_sym_bcs_SymTP(gf, t, x1G,x2G,x3G, in_gfs, params);
  }
  else {
    printf("ERROR: Could not find appropriate boundary condition for CoordSystem = %s!\n",params.CoordSystem);
    exit(1);
  }

  return;
}

/*
 * Important notes:
 * 1) Below is an old version of the boundary condition function calls.
 * 2) We typically use the hand-coded boundary conditions in Apply_out_sym_bcs() above.
 * 3) The Sommerfeld condition is implemented only in the case of spherical-like coordinates.
 * 4) The automatic boundary condition routine is currently slower than the hand-coded versions,
 *    and is generally used only in debugging the hand-coded versions.
 */
/*
void Fill_Ghostzone_Points(const REAL t, REAL *x1G, REAL *x2G, REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_1, paramstruct params) {
// Boundary (parity) conditions at (1)
  for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
#if defined AUTO_BOUNDARY
    Fill_Outer_Boundary(gf, SymmetryMap, gfs_1, params);
    Fill_Symmetry_Boundary(gf, SymmetryMap, ParityMap, gfs_1, params);
#else
    Apply_out_sym_bcs(gf, tph, x1G,x2G,x3G, yy, gfs_1, params);
#endif
  }

// Sommerfeld outer boundary condition at (1)
#if defined OUTER_BOUNDARY_SOMMERFELD
  for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
    Sommerfeld_Outer_BC(gf, x1G,x2G,x3G, yy, gfs_n, gfs_1, params);
  }
#endif

  return;
}
*/
