const int IDX_INIT = -999;
const int IDX_OUTER = -1;

void Unit_Vectors(const REAL x1, const REAL x2, const REAL x3, REAL *x1hat, REAL *x2hat, REAL *x3hat, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"
#include "../reference_metric/NRPy_codegen/xxhat.h"
  return;
}

// Map a ghost zone point to a physical point (possibly itself in the ghost zone)
void Map_Ghost_To_Available_Point(const int idx_g, REAL *Cartxyz, int *ListOfSetPoints, int *ListOfSetPointsLength, int *SymmetryMap) {
  //printf("Filling idx_g = %d\n", idx_g);

  // Length of SymmetryOrderedList
  const int NumberOfSetPoints = *ListOfSetPointsLength;

  // Suppose this is an outer boundary point
  // Next, check if it matches any point already set on our list
  SymmetryMap[0][idx_g] = IDX_OUTER;

  // The Cartesian coordinates for this ghost point
  const REAL x_g = Cartxyz[0][idx_g];
  const REAL y_g = Cartxyz[1][idx_g];
  const REAL z_g = Cartxyz[2][idx_g];

  // Search over all of the grid point locations already set
  for(int ind = 0; ind < NumberOfSetPoints; ind++) {
    // Get the index for this point on the list
    const int idx_p = ListOfSetPoints[0][ind];

    // Physical point location
    const REAL x_p = Cartxyz[0][idx_p];
    const REAL y_p = Cartxyz[1][idx_p];
    const REAL z_p = Cartxyz[2][idx_p];

    // Distance between points
    const REAL dx = x_g - x_p;
    const REAL dy = y_g - y_p;
    const REAL dz = z_g - z_p;
    const REAL dr = sqrt(dx * dx + dy * dy + dz * dz);

    // If the points are 'close enough', call them the same
    // Analytically, they would agree exactly
    if(dr < 1.0e-12) {
      // Set the map
      SymmetryMap[0][idx_g] = idx_p;

      // Break from the search
      break;
    }
  }

  // This ghost point has now been mapped
  // Add it to the end of the list
  ListOfSetPoints[0][NumberOfSetPoints] = idx_g;

  // Increment the list count
  (*ListOfSetPointsLength)++;

  return;
}

// Fill SymmetryMap and ParityMap
void Map_Ghosts(REAL *x1G,REAL *x2G,REAL *x3G, int *SymmetryMap, REAL *ParityMap, paramstruct params) {
  printf("Constructing SymmetryMap and ParityMap...\n");

#include "../parameters_readin-NRPyGEN.h"

  const int Ntot = Npts1 * Npts2 * Npts3;

  ALLOCATE_2D_GENERIC(int, ListOfSetPoints, 1, Ntot);
  ALLOCATE_2D_GENERIC(int, Cartijk, 3, Ntot);
  ALLOCATE_2D_GENERIC(REAL, Cartxyz, 3, Ntot);

  int NumberOfSetPoints = 0;

  // Find Cartesian coordinates of each grid point, Cartxyz
  LOOP_GZFILL(i, j, k) {
    const int idx = IDX3(i, j, k);
    const REAL x1 = x1G[i];
    const REAL x2 = x2G[j];
    const REAL x3 = x3G[k];

    // Fill Cartxyz
#include "../reference_metric/NRPy_codegen/xyz.h"

    Cartijk[0][idx] = i;
    Cartijk[1][idx] = j;
    Cartijk[2][idx] = k;

    // Initialize
    SymmetryMap[0][idx] = IDX_INIT;

    for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
      ParityMap[IDX4pt(gf,idx)] = 1.0;
    }
  }

  printf("Filling grid zone (0, 0, 0)\n");

  // The physical grid maps to itself
  // Grid zone (i, j, k) ~ (0, 0, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      const int idx = IDX3(i, j, k);

      SymmetryMap[0][idx] = idx;
      ListOfSetPoints[0][NumberOfSetPoints] = idx;
      NumberOfSetPoints++;
      }
    }
  }

  printf("NumberOfSetPoints = %d\n", NumberOfSetPoints);

  ///////////////////////////////////////////////////////////////////
  // First, map the 6 faces to the physical grid or outer boundary
  ///////////////////////////////////////////////////////////////////

  printf("Filling grid zone (-, 0, 0)\n");

  // Ghost zone (i, j, k) ~ (-, 0, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, 0, 0)\n");

  // Ghost zone (i, j, k) ~ (+, 0, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, -, 0)\n");

  // Ghost zone (i, j, k) ~ (0, -, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_lower = NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_lower, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, +, 0)\n");

  // Ghost zone (i, j, k) ~ (0, +, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_upper, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, 0, -)\n");

  // Ghost zone (i, j, k) ~ (0, 0, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, 0, +)\n");

  // Ghost zone (i, j, k) ~ (0, 0, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  ///////////////////////////////////////////////////////////////////
  // Second, map the 12 edges to the physical grid or outer boundary
  ///////////////////////////////////////////////////////////////////

  printf("Filling grid zone (-, -, 0)\n");

  // Ghost zone (i, j, k) ~ (-, -, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j_lower, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, +, 0)\n");

  // Ghost zone (i, j, k) ~ (-, +, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j_upper, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, -, 0)\n");

  // Ghost zone (i, j, k) ~ (+, -, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_lower, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, +, 0)\n");

  // Ghost zone (i, j, k) ~ (+, +, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_upper, k), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, 0, -)\n");

  // Ghost zone (i, j, k) ~ (-, 0, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, 0, +)\n");

  // Ghost zone (i, j, k) ~ (-, 0, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, 0, -)\n");

  // Ghost zone (i, j, k) ~ (+, 0, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, 0, +)\n");

  // Ghost zone (i, j, k) ~ (+, 0, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, -, -)\n");

  // Ghost zone (i, j, k) ~ (0, -, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
      const int j_lower = NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_lower, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, -, +)\n");

  // Ghost zone (i, j, k) ~ (0, -, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
      const int j_lower = NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_lower, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, +, -)\n");

  // Ghost zone (i, j, k) ~ (0, +, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
      const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_upper, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (0, +, +)\n");

  // Ghost zone (i, j, k) ~ (0, +, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
      const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	Map_Ghost_To_Available_Point(IDX3(i, j_upper, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  ///////////////////////////////////////////////////////////////////
  // Third, map the 8 corners to the physical grid or outer boundary
  ///////////////////////////////////////////////////////////////////

  printf("Filling grid zone (-, -, -)\n");

  // Ghost zone (i, j, k) ~ (-, -, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j_lower, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, -, +)\n");

  // Ghost zone (i, j, k) ~ (-, -, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	// Ghost zone (i, j, k) ~ (-, -, +)
	Map_Ghost_To_Available_Point(IDX3(i_lower, j_lower, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, +, -)\n");

  // Ghost zone (i, j, k) ~ (-, +, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j_upper, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (-, +, +)\n");

  // Ghost zone (i, j, k) ~ (-, +, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_lower = NGHOSTS - 1 - i;

	Map_Ghost_To_Available_Point(IDX3(i_lower, j_upper, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, -, -)\n");

  // Ghost zone (i, j, k) ~ (+, -, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_lower, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, -, +)\n");

  // Ghost zone (i, j, k) ~ (+, -, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_lower = NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_lower, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, +, -)\n");

  // Ghost zone (i, j, k) ~ (+, +, -)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_lower = NGHOSTS - 1 - k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_upper, k_lower), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  printf("Filling grid zone (+, +, +)\n");

  // Ghost zone (i, j, k) ~ (+, +, +)
  for(int k = 0; k < NGHOSTS; k++) {
    const int k_upper = Npts3 - NGHOSTS + k;

    for(int j = 0; j < NGHOSTS; j++) {
	const int j_upper = Npts2 - NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
	const int i_upper = Npts1 - NGHOSTS + i;

	Map_Ghost_To_Available_Point(IDX3(i_upper, j_upper, k_upper), Cartxyz, ListOfSetPoints, &NumberOfSetPoints, SymmetryMap);
      }
    }
  }

  // Check that every point has been mapped
  LOOP_GZFILL(i, j, k) {
    const int idx = IDX3(i, j, k);

    if(SymmetryMap[0][idx] == IDX_INIT) {
      printf("Error: point with no map (%d, %d, %d)\n", i, j, k);
      exit(1);
    }
  }

  // Fill ParityMap
  LOOP_GZFILL(ig, jg, kg) {
    const int idx_g = IDX3(ig, jg, kg);
    const int idx_p = SymmetryMap[0][idx_g];

    if(idx_p != IDX_OUTER) {
      // Index of the physical point
      const int ip = Cartijk[0][idx_p];
      const int jp = Cartijk[1][idx_p];
      const int kp = Cartijk[2][idx_p];

      REAL x1hat_g[3], x2hat_g[3], x3hat_g[3];
      REAL x1hat_p[3], x2hat_p[3], x3hat_p[3];

      // Evaluate the unit vectors at the ghost point and the physical point
      Unit_Vectors(x1G[ig], x2G[jg], x3G[kg], x1hat_g, x2hat_g, x3hat_g, params);
      Unit_Vectors(x1G[ip], x2G[jp], x3G[kp], x1hat_p, x2hat_p, x3hat_p, params);

      REAL parity_x1 = 0.0, parity_x2 = 0.0, parity_x3 = 0.0;

      // Calculate the dot product between the ghost point and physical point unit vectors
      // This determines the parity condition on vectors and tensors
      // For symmetrical coordinate systems, these should always equal +/- 1
      for(int ii = 0; ii < DIM; ii++) {
	parity_x1 += x1hat_g[ii] * x1hat_p[ii];
	parity_x2 += x2hat_g[ii] * x2hat_p[ii];
	parity_x3 += x3hat_g[ii] * x3hat_p[ii];
      }

      // Parity conditions on one-component vectors
      ParityMap[VET1][idx_g] = ParityMap[LAMB1][idx_g] = ParityMap[BET1][idx_g] = parity_x1;
      ParityMap[VET2][idx_g] = ParityMap[LAMB2][idx_g] = ParityMap[BET2][idx_g] = parity_x2;
      ParityMap[VET3][idx_g] = ParityMap[LAMB3][idx_g] = ParityMap[BET3][idx_g] = parity_x3;

      // Parity conditions on two-component tensors
      ParityMap[H12][idx_g] = ParityMap[A12][idx_g] = parity_x1 * parity_x2;
      ParityMap[H13][idx_g] = ParityMap[A13][idx_g] = parity_x1 * parity_x3;
      ParityMap[H23][idx_g] = ParityMap[A23][idx_g] = parity_x2 * parity_x3;
    }
  }

  FREE_2D_GENERIC(int, ListOfSetPoints, 1, Ntot);
  FREE_2D_GENERIC(int, Cartijk, 3, Ntot);
  FREE_2D_GENERIC(REAL, Cartxyz, 3, Ntot);

  printf("Ntot = %d, NumberOfSetPoints = %d\n", Ntot, NumberOfSetPoints);

  return;
}

// Check each ghost zone region to see if it is an outer boundary
// If so, fill the region using quadratic extrapolation
void Fill_Outer_Boundary(const int gf, int *SymmetryMap, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // First, set the 6 faces
  // Perform extrapolation outward from the physical grid (i, j, k) ~ (0, 0, 0)
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, 0, 0)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
          in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
          in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, 0)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;

        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
          in_gfs[IDX4(gf, i, jj, k)] = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, 0)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 - 1, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
          in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 / 2, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
          in_gfs[IDX4(gf, i, j, kk)] = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 / 2, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
          in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Second, set the 12 edges
  // Use average of the extrapolation from the two adjoining faces
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, -, 0)
  if(SymmetryMap[IDX4(0,0, 0, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];

	  in_gfs[IDX4(gf, ii, jj, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, jj, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, jj, k)] + in_gfs[IDX4(gf, i - 3, jj, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];

	  in_gfs[IDX4(gf, i, jj, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, 0)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, ii, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, ii, j - 2, k)] + in_gfs[IDX4(gf, ii, j - 3, k)];

	  in_gfs[IDX4(gf, ii, j, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, Npts3 / 2)] == IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];

	  in_gfs[IDX4(gf, i, j, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, -)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, ii, j, kk)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, j, kk)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, kk)] + in_gfs[IDX4(gf, i - 3, j, kk)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, i, j, kk)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, +)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, ii, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, ii, j, k - 2)] + in_gfs[IDX4(gf, ii, j, k - 3)];

	  in_gfs[IDX4(gf, ii, j, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];

	  in_gfs[IDX4(gf, i, j, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, i, jj, kk)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 - 1, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i, j - 1, kk)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, kk)] + in_gfs[IDX4(gf, i, j - 3, kk)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, i, j, kk)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, jj, k - 2)] + in_gfs[IDX4(gf, i, jj, k - 3)];

	  in_gfs[IDX4(gf, i, jj, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 - 1, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];

	  in_gfs[IDX4(gf, i, j, k)] = 0.5 * (extrap1 + extrap2);
	}
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Third, set the 8 corners
  // Use average of the extrapolation from the three adjoining edges
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, -, -)
  if(SymmetryMap[IDX4(0,0, 0, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, ii, jj, kk)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, jj, kk)] - 3.0 * in_gfs[IDX4(gf, i - 2, jj, kk)] + in_gfs[IDX4(gf, i - 3, jj, kk)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, kk)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, kk)] + in_gfs[IDX4(gf, i, jj + 3, kk)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, i, jj, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, jj, kk + 2)] + in_gfs[IDX4(gf, i, jj, kk + 3)];

	  in_gfs[IDX4(gf, i, jj, kk)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, -)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, kk)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, kk)] + in_gfs[IDX4(gf, ii + 3, j, kk)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, ii, j - 1, kk)] - 3.0 * in_gfs[IDX4(gf, ii, j - 2, kk)] + in_gfs[IDX4(gf, ii, j - 3, kk)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, ii, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, ii, j, kk + 2)] + in_gfs[IDX4(gf, ii, j, kk + 3)];

	  in_gfs[IDX4(gf, ii, j, kk)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, 0)] == IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, j, kk)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, kk)] + in_gfs[IDX4(gf, i - 3, j, kk)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j - 1, kk)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, kk)] + in_gfs[IDX4(gf, i, j - 3, kk)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];

	  in_gfs[IDX4(gf, i, j, kk)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, -, +)
  if(SymmetryMap[IDX4(0,0, 0, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, jj, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, jj, k)] + in_gfs[IDX4(gf, ii + 3, jj, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, ii, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, ii, jj + 2, k)] + in_gfs[IDX4(gf, ii, jj + 3, k)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, ii, jj, k - 1)] - 3.0 * in_gfs[IDX4(gf, ii, jj, k - 2)] + in_gfs[IDX4(gf, ii, jj, k - 3)];

	  in_gfs[IDX4(gf, ii, jj, k)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, jj, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, jj, k)] + in_gfs[IDX4(gf, i - 3, jj, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, i, jj, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, jj, k - 2)] + in_gfs[IDX4(gf, i, jj, k - 3)];

	  in_gfs[IDX4(gf, i, jj, k)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, +)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = 0; i < NGHOSTS; i++) {
	  const int ii = NGHOSTS - 1 - i;

	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, ii, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, ii, j - 2, k)] + in_gfs[IDX4(gf, ii, j - 3, k)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, ii, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, ii, j, k - 2)] + in_gfs[IDX4(gf, ii, j, k - 3)];

	  in_gfs[IDX4(gf, ii, j, k)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, Npts3 - 1)] == IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
	for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const REAL extrap1 = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
	  const REAL extrap2 = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
	  const REAL extrap3 = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];

	  in_gfs[IDX4(gf, i, j, k)] = (extrap1 + extrap2 + extrap3) / 3.0;
	}
      }
    }
  }

  return;
}

// Check each ghost zone region to see if it is an inner symmetry boundary
// If so, copy data from the physical grid with proper parity
void Fill_Symmetry_Boundary(const int gf, int *SymmetryMap, REAL *ParityMap, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  //////////////////////////////////////
  // First, set the 6 faces
  //////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, 0, 0)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, 0)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, 0)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 / 2, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 / 2, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  //////////////////////////////////////
  // Second, set the 12 edges
  //////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, -, 0)
  if(SymmetryMap[IDX4(0,0, 0, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, 0)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, 0)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, Npts3 / 2)] != IDX_OUTER) {
    for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, -)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, +)
  if(SymmetryMap[IDX4(0,0, Npts2 / 2, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 / 2, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, jj, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, 0, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, -)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 - 1, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, +)
  if(SymmetryMap[IDX4(0,Npts1 / 2, Npts2 - 1, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  //////////////////////////////////////
  // Third, set the 8 corners
  //////////////////////////////////////

  // Ghost zone (i, j, k) ~ (-, -, -)
  if(SymmetryMap[IDX4(0,0, 0, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, jj, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, -, +)
  if(SymmetryMap[IDX4(0,0, 0, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, -)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, +)
  if(SymmetryMap[IDX4(0,0, Npts2 - 1, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = 0; i < NGHOSTS; i++) {
          const int ii = NGHOSTS - 1 - i;
	  const int idx_g = IDX3(ii, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, jj, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, 0, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = 0; j < NGHOSTS; j++) {
	const int jj = NGHOSTS - 1 - j;

        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, jj, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, -)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, 0)] != IDX_OUTER) {
    for(int k = 0; k < NGHOSTS; k++) {
      const int kk = NGHOSTS - 1 - k;

      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, kk);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, +)
  if(SymmetryMap[IDX4(0,Npts1 - 1, Npts2 - 1, Npts3 - 1)] != IDX_OUTER) {
    for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
	  const int idx_g = IDX3(i, j, k);
	  const int idx_p = SymmetryMap[0][idx_g];

          in_gfs[IDX4pt(gf,idx_g)] = ParityMap[IDX4pt(gf,idx_g)] * in_gfs[IDX4pt(gf,idx_p)];
        }
      }
    }
  }

  return;
}
