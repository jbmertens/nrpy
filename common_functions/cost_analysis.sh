#!/bin/bash
cat $1 | sed "s/\((...L\\/...L)\)/555/g" > /tmp/costanalyze`whoami`.txt
FILENAME="/tmp/costanalyze`whoami`.txt"

SUBS=`cat $FILENAME |sed "s/\-/\nddddd\n/g"|grep dddd|wc -l`
ADDS=`cat $FILENAME |sed "s/\+/\nddddd\n/g"|grep dddd|wc -l`
MLTS=`cat $FILENAME |sed "s/\*/\nddddd\n/g"|grep dddd|wc -l`
DIVS=`cat $FILENAME |sed "s/\//\nddddd\n/g"|grep dddd|wc -l`
ASNS=`cat $FILENAME |sed  "s/=/\nddddd\n/g"|grep dddd|wc -l`
SQTS=`cat $FILENAME |grep -v \# | sed "s/[Ss]qrt(/\nddddd\n/g"|grep dddd|wc -l`
LOGS=`cat $FILENAME |grep -v \# | sed "s/[Ll]og(/\nddddd\n/g"|grep dddd|wc -l`
SINS=`cat $FILENAME |grep -v \# | sed "s/[Ss]in(/\nddddd\n/g"|grep dddd|wc -l`
COSS=`cat $FILENAME |grep -v \# | sed "s/[Cc]os(/\nddddd\n/g"|grep dddd|wc -l`
TANS=`cat $FILENAME |grep -v \# | sed "s/[Tt]an(/\nddddd\n/g"|grep dddd|wc -l`
POWS=`cat $FILENAME |grep -v \# | sed "s/pow(/\nddddd\n/g;s/Power(/\nddddd\n/g"|grep dddd|wc -l`

# http://nicolas.limare.net/pro/notes/2014/12/16_math_speed/
SUBSFP=`echo $SUBS*1| bc`  # 178 (guess)
ADDSFP=`echo $ADDS*1 | bc` # 178
MLTSFP=`echo $MLTS*1 | bc` # 178
DIVSFP=`echo $DIVS*2.2 | bc` # 80
ASNSFP=`echo $ASNS*1 | bc` # guess
SQTSFP=`echo $SQTS*2.2 | bc` # 178 / 81 ~ 2.2
LOGSFP=`echo $LOGS*16.3| bc` # 178 / 10.9 ~ 16.3
SINSFP=`echo $SINS*19.3| bc` # 178 / 9.235 ~ 19.3
COSSFP=`echo $COSS*19.3| bc` # 178 / 9.235 ~ 19.3
TANSFP=`echo $TANS*25.0| bc` # 178 / 7.118 ~ 25.0
POWSFP=`echo $POWS*16.6| bc` # 178 / 10.7 ~ 16.6

echo "  OPERATION  | NUM   | Estimated FLOPs"
echo "Subtractions :" $SUBS "	" $SUBSFP 
echo "Additions    :" $ADDS "	" $ADDSFP 
echo "Mults        :" $MLTS "	" $MLTSFP 
echo "Divisions    :" $DIVS "	" $DIVSFP 
echo "Assignments  :" $ASNS "	" $ASNSFP 
echo "Sqrts        :" $SQTS "	" $SQTSFP 
echo "Logs         :" $LOGS "	" $LOGSFP 
echo "Sines        :" $SINS "	" $SINSFP 
echo "Cosines      :" $COSS "	" $COSSFP 
echo "Tangents     :" $TANS "	" $TANSFP 
echo "Pows         :" $POWS "	" $POWSFP
echo "============================="
echo "Total FLOPs (est): 	" `echo $SUBSFP+$ADDSFP+$MLTSFP+$DIVSFP+$ASNSFP+$SQTSFP+$LOGSFP+$SINSFP+$COSSFP+$TANSFP+$POWSFP | bc`
