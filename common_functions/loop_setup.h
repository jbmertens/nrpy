#ifndef LOOP_SETUP_H__
#define LOOP_SETUP_H__

//#define ENABLE_SIMD_256_INTRINSICS
//#define ENABLE_SIMD_256_FMA_INTRINSICS
//#define ENABLE_SIMD_512_INTRINSICS

//#define USE_LONG_DOUBLE
#ifdef USE_LONG_DOUBLE
#define REAL long double
#else
#define REAL double
#endif


/* Aliases for computing index on arrays. Note that this must be 
   consistent with LOOP_GZFILL and LOOP_NOGZFILL below, to ensure 
   we are not cache-missing ourselves to death.
*/

/* The following aliases assume (Ni,Nj,Nk) = (Npts1,Npts2,Npts3). 
   WARNING: attempting to use these aliases with arrays that
   * are not allocated with these dimensions will result in
   * IMPROPER ARRAY ACCESSES. */
#define IDX3(i,j,k)     ( (i) + Npts1 * ( (j) + Npts2 * (k) ) )
#define IDX4(g,i,j,k)   ( (i) + Npts1 * ( (j) + Npts2 * ( (k) + Npts3 * (g) ) ) )
#define IDX4pt(g,idx)   ( (idx) + (Npts1*Npts2*Npts3) * (g) ) // Assuming idx = IDX3(i,j,k). Much faster if idx can be reused over and over.

/* Main Loops */
#define LOOP_GZFILL(i,j,k) _Pragma("omp parallel for")		\
  for(int k=0;k<Npts3;k++) for(int j=0;j<Npts2;j++) for(int i=0;i<Npts1;i++)
#define LOOP_NOGZFILL(i,j,k) _Pragma("omp parallel for")		\
  for(int k=NGHOSTS;k<Npts3-NGHOSTS;k++) for(int j=NGHOSTS;j<Npts2-NGHOSTS;j++) for(int i=NGHOSTS;i<Npts1-NGHOSTS;i++)

#define SIMD_WARNING_MESSAGE                                            \
  if(strncmp(params.SIMD,"256bit",100)!=0 && strncmp(params.SIMD,"512bit",100)!=0) { \
    printf("Error: #define'd ENABLE_SIMD_[256|512]_INTRINSICS, but did not enable SIMD=256bit or SIMD=512bit inside NRPy+!\n"); \
    exit(1);                                                            \
  }                                                                     \
  if((strncmp(params.SIMD,"256bit",100)==0 && params.Nx1%4 != 0) || (strncmp(params.SIMD,"512bit",100)==0 && params.Nx1%8 != 0)) { \
  printf("Error: params.SIMD is set to 256bit or 512bit, but Nx1 was not set to a multiple of 4 or 8, respectively.\n"); \
  exit(1);                                                              \
  }

#define START_LOOP_NOGZFILL(ii,jj,kk)           \
  for(int kk=NGHOSTS;kk<Npts3-NGHOSTS;kk++) {   \
  const REAL x3 = x3G[kk];                      \
  _Pragma("omp parallel for")                   \
  for(int jj=NGHOSTS;jj<Npts2-NGHOSTS;jj++) {   \
  const REAL x2 = x2G[jj];                      \
  _Pragma("ivdep")                              \
  _Pragma("vector always")                      \
  for(int ii=NGHOSTS;ii<Npts1-NGHOSTS;ii++) {   \
  const int idx = IDX3(ii, jj, kk);             \
  const REAL x1 = x1G[ii];

#define END_LOOP_NOGZFILL } } }

#if defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)

#ifdef ENABLE_SIMD_256_INTRINSICS

#define START_LOOP_NOGZFILL_SIMD(ii,jj,kk)                              \
  for(int (kk)=NGHOSTS;(kk)<Npts3-NGHOSTS;(kk)++) {                     \
  const __m256d x3 = _mm256_set1_pd(x3G[(kk)]);                         \
  _Pragma("omp parallel for")                                           \
  for(int (jj)=NGHOSTS;(jj)<Npts2-NGHOSTS;(jj)++) {                     \
  const __m256d x2 = _mm256_set1_pd(x2G[(jj)]);                         \
  for(int (ii)=NGHOSTS;(ii)<Npts1-NGHOSTS;(ii)+=4) {                    \
  const int idx = IDX3((ii), (jj), (kk));                               \
  const __m256d x1 = _mm256_loadu_pd(&x1G[(ii)]);

#else // ENABLE_SIMD_512_INTRINSICS

#define START_LOOP_NOGZFILL_SIMD(ii,jj,kk)                              \
  for(int (kk)=NGHOSTS;(kk)<Npts3-NGHOSTS;(kk)++) {                     \
  const __m512d x3 = _mm512_set1_pd(x3G[(kk)]);                         \
  _Pragma("omp parallel for")                                           \
  for(int (jj)=NGHOSTS;(jj)<Npts2-NGHOSTS;(jj)++) {                     \
  const __m512d x2 = _mm512_set1_pd(x2G[(jj)]);                         \
  for(int (ii)=NGHOSTS;(ii)<Npts1-NGHOSTS;(ii)+=8) {                    \
  const int idx = IDX3((ii), (jj), (kk));                               \
  const __m512d x1 = _mm512_loadu_pd(&x1G[(ii)]);

#endif // ENABLE_SIMD_256_INTRINSICS

#endif // defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)

#define END_LOOP_NOGZFILL_SIMD } } }

/* Basic loops for implied sums, etc. Impose indexed-quantity symmetry. */
#define F2s12(i,j) for(int i=0;i<DIM;i++) for(int j=i;j<DIM;j++) 

#endif // LOOP_SETUP_H__
