void get_w(const int N,REAL w[N],REAL x[N]) {
  for(int i=0;i<N;i++) {
    REAL w_tmp_inv = 1.0;
    for(int j=0;j<i;j++)   w_tmp_inv *= (x[i] - x[j]);
    for(int j=i+1;j<N;j++) w_tmp_inv *= (x[i] - x[j]);
    w[i] = 1.0 / w_tmp_inv;
  }
}

void get_w_double(const int N,double w[N],double x[N]) {
  for(int i=0;i<N;i++) {
    double w_tmp_inv = 1.0;
    for(int j=0;j<i;j++)   w_tmp_inv *= (x[i] - x[j]);
    for(int j=i+1;j<N;j++) w_tmp_inv *= (x[i] - x[j]);
    w[i] = 1.0 / w_tmp_inv;
  }
}

/*
  interpolate_3D(): Function documentation:
  
  -={ Inputs }=-
  paramstruct params: Contains information about grid and coord.
  *            system parameters
  const REAL *x1G,*x2G,*x3G: Points at which input gridfunctions are stored
  const REAL *in_gfs: Array of input gridfunctions

  const int numgfs: Number of gridfunctions to interpolate
  const int gfs_interp_list[numgfs]: List of gridfunctions (by number) 
  *            on which to perform interpolation

  const int N: the degree of the fitting polynomial, plus one.
  *            I.e., N=2 -> P_1(x) = a x + b
  *            I.e., N=3 -> P_2(x) = a x^2 + b x + c

  const REAL *out_pts: Output points
  const int numpoints: The number of interpolation (output) points

  -={ Outputs }=-
  REAL *out_interp_gfs: Output gridfunctions. Only numgfs 
  *             gridfunctions are output, in order.
 
  */
void interpolate_3D(paramstruct params,
                    REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, 
                    const int numgfs, const int gfs_interp_list[numgfs],
                    const int N,
                    REAL *output_pts, const int numpoints, REAL *out_interp_gfs) {
#include "../parameters_readin-NRPyGEN.h"
#include "../read_FD_dxs.h"

  REAL x[N];
  REAL y[N];
  REAL z[N];
  for(int i=0;i<N;i++) {
    x[i] = i*del[0];
    y[i] = i*del[1];
    z[i] = i*del[2];
  }

  REAL w_x[N];
  REAL w_y[N];
  REAL w_z[N];
  get_w(N,w_x,x);
  get_w(N,w_y,y);
  get_w(N,w_z,z);

  REAL xmin = x1G[0];
  REAL ymin = x2G[0];
  REAL zmin = x3G[0];

  for(int which_gf_i=0;which_gf_i<numgfs;which_gf_i++) {
    int which_gf = gfs_interp_list[which_gf_i];
    
    //#pragma omp parallel for
    for(int ll=0;ll<numpoints;ll++) {
      int ijk_idx[3][N];

      out_interp_gfs[IDX4pt(which_gf_i,ll)]=0.0;

      REAL l_of_x = 1.0;
      REAL l_of_y = 1.0;
      REAL l_of_z = 1.0;

      REAL w_x_i_times_xdiff_inv[N];
      REAL w_y_i_times_ydiff_inv[N];
      REAL w_z_i_times_zdiff_inv[N];

      for(int i=0;i<N;i++) {
        REAL x_out = output_pts[IDX4pt(0,ll)];
        REAL y_out = output_pts[IDX4pt(1,ll)];
        REAL z_out = output_pts[IDX4pt(2,ll)];

        // x_i = x_0 + i * dx
        // -> i = (x_i - x_0) / dx
        // --> nearest point i to x_out = i_out = (x_out - x_0) / dx
        // Our interpolation stencil should be centered at i,j,k.
        // Thus, leftmost point of the stencil in i-direction
        //       should be at i - N/2
        // Note that we add 0.5 to i before the integer typecast to
        //       ensure proper rounding!
        ijk_idx[0][i] = i + (int)((x_out - xmin) * invdx0 + 0.5) - N/2;
        ijk_idx[1][i] = i + (int)((y_out - ymin) * invdx1 + 0.5) - N/2;
        ijk_idx[2][i] = i + (int)((z_out - zmin) * invdx2 + 0.5) - N/2;

        if(ijk_idx[0][i] > Npts1-NGHOSTS) { printf("BAD I %d %e\n",ijk_idx[0][i],(double)x_out); }
        if(ijk_idx[1][i] > Npts2-NGHOSTS) { printf("BAD J\n"); }
        if(ijk_idx[2][i] > Npts3-NGHOSTS) { printf("BAD K\n"); }

        if(ijk_idx[0][i] < 0) { printf("minBAD I %d %e\n",ijk_idx[0][i],(double)x_out); }
        if(ijk_idx[1][i] < 0) { printf("minBAD J\n"); }
        if(ijk_idx[2][i] < 0) { printf("minBAD K\n"); }

        REAL xdiff = x_out - x1G[ijk_idx[0][i]];
        REAL ydiff = y_out - x2G[ijk_idx[1][i]];
        REAL zdiff = z_out - x3G[ijk_idx[2][i]];
        w_x_i_times_xdiff_inv[i] = w_x[i] / xdiff;
        w_y_i_times_ydiff_inv[i] = w_y[i] / ydiff;
        w_z_i_times_zdiff_inv[i] = w_z[i] / zdiff;
        l_of_x *= xdiff;
        l_of_y *= ydiff;
        l_of_z *= zdiff;
      }
      // First fill the stencil with all needed points.
      //for(int i=0;i<N;i++) for(int j=0;j<N;j++) for(int k=0;k<N;k++) {
      for(int k=0;k<N;k++) for(int j=0;j<N;j++) for(int i=0;i<N;i++) {
            const REAL f_ijk  = in_gfs[IDX4(which_gf,ijk_idx[0][i],ijk_idx[1][j],ijk_idx[2][k])];
            out_interp_gfs[IDX4pt(which_gf_i,ll)] += f_ijk*w_x_i_times_xdiff_inv[i]*w_y_i_times_ydiff_inv[j]*w_z_i_times_zdiff_inv[k];
          }
      out_interp_gfs[IDX4pt(which_gf_i,ll)] *= l_of_x*l_of_y*l_of_z;
    }
  }
}
