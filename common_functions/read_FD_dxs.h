const REAL invdx0 = 1.0 / (x1G[1] - x1G[0]);
const REAL invdx1 = 1.0 / (x2G[1] - x2G[0]);
const REAL invdx2 = 1.0 / (x3G[1] - x3G[0]);

const REAL invdx0invdx0 = invdx0*invdx0;
const REAL invdx0invdx1 = invdx0*invdx1;
const REAL invdx0invdx2 = invdx0*invdx2;

const REAL invdx1invdx1 = invdx1*invdx1;
const REAL invdx1invdx2 = invdx1*invdx2;
const REAL invdx2invdx2 = invdx2*invdx2;
